BondIT Ltd Fixed-Assets Management Project in glance.
=======================================

There is an algorithm main directory of BondIT fix-assets management project.

The project is built on the three groups of logical functionality:
    - Projects
    - Business Logic
    - Shared functions

There are two projects:
    - simulations
    - pilot
There are including independent functionality. There are not any relations between them at logic or at functionality levels.

Business Logic group includes Business Logic Layer (BLL) modules and Tools module.
The scheme of it can been seen in Code Guidelines presentation.
    - BLL includes phisical packages like single index model, markovitz model, etc.
    - Tools includes phisical packages like pricing, inflation model, term structure and so on.

Shared functions group based on Data Access Layer (DAL), Data Model Layer (DML) and Utility package.
    - DAL provides interaction with database server.
    - DML offers the portfolio structure and functionality for simulation project
    - Utility includes all possible shared functions are used in the functions from any of levels above.
       All utilized functions are grouped by it sence into some packages.

Copyright 2013, BondIT Ltd.
