load clusteringPractice.mat


alphas = round(ds4.alpha(1:end)*10000)/10000;
totalValues = ds4.total_value;
nans = find(isnan(alphas)==1);
endport = nans - 1;

if endport(1)==0
    endport = endport(2:end);
end
[a, i1, i2] = unique(alphas(endport)); 
i3 = [];
for j =1:length(i2)-1
    if i2(j) ~= i2(j+1)
        i3 = [i3 i2(j)];
    end
end
i3 = [i3 i2(end)];
alpha_end = a(i3); 
totalValue_end = totalValues(endport);
totalValue_end = totalValue_end(i3);
volatilitys = ds4.volatility(endport);
volatility_end = volatilitys(i3);
skewnesses = ds4.skewness(endport);
skewness_end = skewnesses(i3);
success_end = totalValue_end > 1.04*1.04*10^7;
x_end = [alpha_end volatility_end skewness_end];
t_end= classregtree(x_end, success_end, 'names', {'alpha','volatility', 'skewness'}, 'minparent', 1);%, 'skew', 'kurt'})
%  RegressionTree.fit(x, results)%, 'names', {'alpha','beta', 'skew'})
view(t_end)

begport = nans+1;
begport = begport(1:end-1);
[a, k1, k2] = unique(alphas(begport)); 
i3 = [];
for j =1:length(k2)-1
    if k2(j) ~= k2(j+1)
        i3 = [i3 k2(j)];
    end
end
i3 = [i3 k2(end)];
alpha_beg = a(i3);
volatilityb = ds4.volatility(begport);
volatility_beg = volatilityb(i3);
skewnessesb = ds4.skewness(begport);
skewness_beg = skewnessesb(i3);
totalValueb = ds4.total_value(begport);
success_beg = totalValueb(i3)>1.04*10^7;

x_beg = [alpha_beg volatility_beg skewness_beg];
t_beg= classregtree(x_beg, success_beg, 'names', {'alpha_beg','vol_beg', 'skew_beg'}, 'minparent', 1);%, 'skew', 'kurt'})
%  RegressionTree.fit(x, results)%, 'names', {'alpha','beta', 'skew'})
view(t_beg)





