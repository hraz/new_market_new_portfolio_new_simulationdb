function simulation_script(inString)


 
global DBSOURCE     %TO COPY
global NBUSINESSDAYS
global PUBLIC_FLAG_DEFAULT
global VIRTUAL_FLAG_DEFAULT
global BASED_ON_FLAG_DEFAULT
global EXP_RETURN_CALC_METHOD;
global EXP_VOL_CALC_METHOD;
global FTSALLPRICE;
global GLOBALFTSAllBONDYTM;
global GLOBALCELLYTM603;
global GLOBALCELLYTM602;
global FTSPAYEXDATEMAPPING;
global CELLYTM707;
global CELLYTM708;
global CELLYTM709;
global GLOBALDSLINKAGEINDEX;
 
NBUSINESSDAYS = 252;
PUBLIC_FLAG_DEFAULT = 0;
VIRTUAL_FLAG_DEFAULT = 0;
BASED_ON_FLAG_DEFAULT = 0;
EXP_RETURN_CALC_METHOD=1;
EXP_VOL_CALC_METHOD=2;
ftsPrices = load(fullfile(pwd, '+bll', '+cumulative_return', 'ftsDirtyPriceAllBonds.mat'));
FTSALLPRICE = ftsPrices.ftsPrice0;
FTSPAYEXDATEMAPPING = load(fullfile(pwd, '+bll', '+cumulative_return', 'fts_pay_ex_date_mapping.mat'));
load(fullfile(pwd, '+bll', '+cumulative_return', 'cellYtmUpdated.mat'));
CELLYTM707 = cellYTM707;
CELLYTM708 = cellYTM708;
CELLYTM709 = cellYTM709;
GLOBALFTSAllBONDYTM = cellYTM601;
GLOBALCELLYTM602 = cellYTM602;
GLOBALCELLYTM603 = cellYTM603;
load(fullfile(pwd, '+bll', '+cumulative_return', 'ds_linkage_index.mat'));
GLOBALDSLINKAGEINDEX = dsLinkageIndex;



import dml.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;
import dal.simulation.set.*;
import dal.simulation.get.*;
import bll.solver_calculations.*;
import dal.portfolio.data_access.*;

% dbstop if error

% tic;
min_num = 1; max_num = 894;
% [ sim_id ] = get_free_sim_id(min_num, max_num);% sims = [400];
%% If going in order
sim_num=  [209 160 322 77 377 138 38 115 391 153 284 66 170 313 213 234 132 348 339 293 246 346 105 89 158 354 406 11 194 323 155 129 336 54 233 229 244 50 171 227 69 334 328 435 261 37 204 376 253 301 167 303 370 114 425 332 62 17 19 81 57 5 8 56 123 223 238 131 75 169 228 117 315 231 349 428 404 240 53 203 168 127 110 217 150 88 288 326 222 45 296 183 419 148 76 86 9 330 125 329 218 365 147 437 287 299 439 432 357 382 39 24 184 29 379 347 369 205 237 4 97 40 268 71 280 272 331 28 181 239 340 259 199 134 333 294 22 310 174 197 108 285 32 274 319 372 305 119 395 399 128 214 18 143 136 113 417 254 442 95 245 10 265 73 338 15 13 327 407 436 175 187 202 276 74 413 94 116 31 35 36 248 362 85 104 392 311 427 295 149 30 201 418 44 52 363 371 220 411 251 308 230 277 443 247 307 367 101 241 193 60 394 70 186 146 25 163 154 64 358 343 72 388 271 191 269 235 3 423 33 378 364 122 447 145 390 84 353 190 192 317 304 291 400 27 385 424 188 112 446 172 144 258 273 320 16 6 221 290 166 137 243 107 102 130 93 393 263 368 325 111 41 79 405 292 135 182 286 434 58 408 316 403 375 91 219 433 318 178 65 185 51 161 12 23 344 208 278 444 61 215 2 289 257 297 92 360 438 195 350 351 133 118 206 389 216 262 300 121 341 282 48 34 49 141 68 366 266 189 87 275 412 20 47 430 46 180 225 90 212 236 374 207 162 420 373 99 83 342 356 140 383 156 151 306 67 410 321 250 142 98 96 55 441 397 224 26 264 445 267 302 281 361 120 210 398 100 298 416 106 21 401 309 352 431 421 386 335 164 200 345 59 252 232 384 440 165 429 426 242 139 422 1 256 82 211 355 78 126 255 337 80 402 409 226 43 260 396 159 380 173 387 177 103 63 283 42 124 312 198 414 279 314 324 270 381 14 176 359 152 7 415 179 157 196 109 249];
% numOfSimulations = length(sim_num);
% [outStatus, outException] = set_simulation_range( numOfSimulations );
% if outStatus == 1
%     error(outException)
% end
tic;
ts=0;
iSimId=1;
while iSimId <= length(sim_num) && ts < 24 * 3600
    %     for iSimNum = 1:length(sims)
    import simulations.scripts.*;
    
    
    DBSOURCE = 'mysql';
    %     t2 = tic;
    simulation_function(sim_num(iSimId));
    iSimId=iSimId+1;
    
    % [ sim_num ] = get_free_sim_id(min_num, max_num);% sims = [400];
    ts = toc
    
end

toc

%  save(fullfile(pwd, 'NoSQLTiming.mat'),'TIME');