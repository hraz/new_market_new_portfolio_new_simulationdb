function [solution solver_props] = prep_save(constraints, solution, nsin_data, solver_props)
% PREP_SAVE prepares the saving of the solution
%
% [solution solver_props] = prep_save(constraints, solution, nsin_data, solver_props)- functions prepares the saving of the solution and the arranges some of the needed data
%
%     Input: constraints - data structure - required fields: benchmark_id - Integer - referring to benchmark against which portfolio was built
%                                                            wgt_per_bond_choice - Double - the max weight per bond allowed in portfolio
%
%            solution - data structure - required fields - nsin_sol - N X 1 Integer - list of nsins making up solution
%                                                          PortWts - N X 1 Double - list of weights making up solution, must add up to 1
%                                                          PortDuration - Double - time of actual portfolio duration
%                                                          selected_indx - M X 1 integer - indices of nsins chosen from original filtered list
%                                                          ReinvestmentStrategy - string - what type of reinvestment strategy to calculate, possibilities:
%                                                          1. 'Specific-Bond-Reinvestment' - we invest in the same bond from which we got a coupon.
%                                                          2. 'risk-free': we invest in a risk free asset (government bond).
%                                                          3. 'portfolio': we invest in the entire portfolio.
%                                                          4. 'benchmark': we invest in the appropriate benchmark.
%                                                          5. 'bank': we put the money in the bank to earn interest.
%                                                          6. 'none':
%                                                          PortRet - Double - expected portfolio return
%                                                          PortRisk - Double - expected portfolio risk
%            solver_props - data structure - required fields - risk_free_interests - Years X 1 Double - yield of risk free bonds for differet periods
%                                                              risk_free_bond_ttm - Years X 1 Double - ttm of risk free bonds for differet periods
%            nsin_data - data structure - required fields - Duration - M X 1 double - duration of bonds filtered
%                                                           YTM - M X 1 double - YTM of bonds filtered
%
%     Output:     solution - data structure
%                 solver_props - data structure - fields related to solution cleared
%
%   See also:
%       simulation_function
%
% Hillel Raz, June 11th, BABY DAY!!!
% Copyright 2013, Bond IT Ltd

import bll.performance_calculations.*;
import bll.data_manipulations.*;

if isempty(constraints.benchmark_id)
    constraints.benchmark_id = 601;
end

%% check and save solution
solution.PortRetExpectedWReinvestment = solution.PortReturn; %Initialize
if strcmp(solution.ReinvestmentStrategy, 'risk-free'); %Since Specific-Bond-Reinvestment does not require recalculation of the expected return
    %     reinvestmentMethod = 2; %risk-free
    if ~isnan(solution.PortReturn)||( sum(solution.PortWts) ~= 0)
        allocation.nsin = solution.nsin_sol; % Needed for calc_expected_return
        allocation.weight = solution.PortWts;
        returnMethod = 1; % Expected return calculated using YTM.
        [solution.PortRetExpectedWReinvestment] = calc_expected_return(returnMethod, solution.ReinvestmentStrategy, allocation, solution.PortDuration, constraints.benchmark_id, nsin_data.YTM(solution.selected_indx), nsin_data.Duration(solution.selected_indx), solver_props.risk_free_interests, solver_props.risk_free_bond_ttm);
    end
elseif strcmp(solution.ReinvestmentStrategy, 'benchmark');
    %     reinvestmentMethod = 4;
    if ~isnan(solution.PortReturn)
        allocation.nsin = solution.nsin_sol; % Needed for calc_expected_return
        allocation.weight = solution.PortWts;
        returnMethod = 1; % Expected return calculated using YTM.
        [solution.PortRetExpectedWReinvestment] = calc_expected_return(returnMethod, solution.ReinvestmentStrategy, allocation, solution.PortDuration, constraints.benchmark_id, nsin_data.YTM(solution.selected_indx), nsin_data.Duration(solution.selected_indx),solution.DateNum);
    end
    if isempty(solution.PortRetExpectedWReinvestment)||isempty(solution.nsin_sol)
        solution.PortRetExpectedWReinvestment = NaN;
    end
    if isempty(solution.PortRisk)||isempty(solution.nsin_sol)
        solution.PortRisk = NaN;
    end
end
%Refresh relevant solver_props fields.
[solver_props] = clear_relevant_solver_props_fields(solver_props, constraints.wgt_per_bond_choice);

end