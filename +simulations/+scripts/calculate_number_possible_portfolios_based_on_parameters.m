function [totalPortfolios] = calculate_number_possible_portfolios_based_on_parameters(numMonths, indices, possiblePortDurations, numPointsFrontier, varargin)
% CALCULATE_NUMBER_POSSIBLE_PORTFOLIOS_BASED_ON_PARAMETERS - function calculates total possible number of portfolios for a given set of parameters
%     
%  [totalPortfolios] = calculate_number_possible_portfolios_based_on_parameters(numMonths, indices, possiblePortDurations, numPointsFrontier, varargin) - 
%         function calculates number of permutations of parameters to get the total number of possible portfolios that can be built.  
%             
%             
%     Input:    numMonths - INT - number of months from which simulation will build portfoios
%               
%               indices - N X 1 logic - 1 represents if the index is being simulated, 0 not.  The order of the indices
%                       is 601, 602, 603, 707 (20), 708 (40), 709 (60)
% 
%               possiblePortDurations - INT - what is the total number of
%                   portfolios durations
%               
%               numPointsFrontier - INT - number of points on frontier
% 
%               varargin - INT - parameters that are multipliers of the
%                   total number of portfolios - for instance, 3 different
%                   types of ratings would be entered as a value of 3. 
% 
%    Output:    totalPortfolios - INT - total number of portfolios to be
%                   simulated
% 
% Hillel Raz, July 18th, Jonathan's Pidayon/Pikadon day.
% Copyright 2013, Bond IT Ltd


TelBond20 = 13; % Number of months after beginning of data that index was born
TelBond40and60 = 25; % Also means that we must go that many months into data before simulating

if nargin > 4
    numParams = nargin - 4;
    params = zeros(numParams, 1);
    for j = 1:numParams
        params(j) = varargin{j};
    end
end

totalPortfolios = 0;
numIndices = length(indices);
for indexCount = 1:numIndices
    numMonthsCurrent = numMonths; %for 601, 602, 603
    
    if indexCount == 4  % TelBond 20
        numMonthsCurrent = numMonths -  TelBond20;
    end
    if indexCount == 5 || indexCount == 6
        numMonthsCurrent = numMonths -  TelBond40and60;
    end
    if indexCount  == 7 % Room for other indices
    end
    
    if indices(indexCount) % Is index being simulated?
        for numDur = 1:possiblePortDurations
            
            totalPortfolios = totalPortfolios + numMonths-(numDur-1)*12;
            
        end
        
    end
end

totalPortfolios = totalPortfolios*numPointsFrontier;
if nargin>4
    extraParams = prod(params);
    totalPortfolios = totalPortfolios*extraParams;
end
