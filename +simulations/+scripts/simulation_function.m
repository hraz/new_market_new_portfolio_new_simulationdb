function  simulation_function(sim_num)

global NBUSINESSDAYS

import bll.data_manipulations.*;
import dal.market.filter.market_indx.*;
import dal.market.get.dynamic.*;
import dal.portfolio.data_access.*;
% import dal.portfolio.crud.*; %replaced by new simulations function in dal.simulation.set
import dal.simulation.set.*; 
import dml.*;
import simulations.choose_functions.*;
import dal.market.get.base_data.*;
import bll.filtering_functions.*;
import bll.solver_calculations.*;
import bll.cumulative_return.*;
import utility.*;
import simulations.scripts.*;
import bll.performance_calculations.*;
import simulations.simulation_utility_functions.*;
import dml.portfolio.*;

account_name=['sim_num'];
load(fullfile(pwd, 'simulations', 'scripts', 'riskFreeInfo.mat'));

try
    dbstop if error
    
    errorDS  = dataset({}, 'VarNames', 'err_msg');
    
    %% Set time
    t_beg = 732679; %02/01/2006
    t_fin= 735353-10; %As of 19/03/2013, -10 days just to be safe
    
    nSimMonthsTotal = floor((t_fin-t_beg)/30) - 3;
    [ nMonthsBeginSim benchmark_choice model_choice indexChoice] = simulation_parameters( sim_num, nSimMonthsTotal );
    datesvector=floor(linspace(t_beg,t_fin-30, nSimMonthsTotal)); %Enough time to simulate for at least a month
    DateNum = datesvector(nMonthsBeginSim); % amit:same as:  DateNum = datesvector(nMonths);
    
    time_left = (t_fin - DateNum)/365;
    port_dur_yrs = min(ceil(time_left), 6); %Any fracion of a year is counted as a year for simulation's sake
    errorDS  = dataset({}, 'VarNames', 'err_msg');
    
    [solver_props] = create_solver_props();
    [constraints] = create_constraints();
    constraints.model_choice = model_choice;
    filtered =[];
    
    %% Filter choices
    %%model and point on frontier
    %constraints.point_choice = 'Tar'; %'TarOS'; %'MV''OS''Tar' 'HighMom' 'Om'
    %% Default values for simulation
    solver_props.benchmarkSolutionFlag = 1; %Solution compared to and solved against benchmark universe.
    if  solver_props.benchmarkSolutionFlag
        numPoints = 7;
    else
        numPoints = 10;
    end
    
    constraints.BondDuration_choice = 'Unlimited';%'Unlimited' 'UpperLimitOnBondsDuration' 'LowerLimitOnBondsDuration' 'UpperAndLowerLimitOnBondsDuration'
    constraints.YTM_choice = 'YTM100'; %'YTM20' 'YTM40' 'YTM60''YTM100''YTM300'
    constraints.MinTurnover_choice = 'MinTurnOver0'; %% 'MinTurnOver0' 'MinTurnOver0.1''MinTurnOver1'
    constraints.rating_choice = 'AAA-NVR'; % 'AAA''AAA-BBB''AAA-CCC' 'AAA-NVR''BBB-NVR'
    constraints.sector_choice = 'Unlimited'; % 'Fin' '~I&H' '~Bio'  'Ind' 'Risky'
    constraints.type_choice = 'G&C'; % 'Gov''Cor' 'G&C'  'G-Link''C-Link'
    constraints.wgt_per_bond_choice = 20/100; %10/100 15/100
    reinvestmentMethod = 'risk-free'; %'Specific-Bond-Reinvestment' 'risk-free''benchmark''real-specific-bond-reinvestment'
    %  benchmark_choice = 'TelBond60'; % 'AllBonds' 'None' 'GovBonds''NonGovBonds''TelBond20''TelBond40''TelBond60'
    
    %%special instructions
    constraints.capital_TC = ''; %'B/S.15''B.15/S.25''B/S.25'
    constraints.special_case_choice = ''; %'Z' 'Clean'
    
    %% Setting filter parameters
    
    %% relevant benchmark
    if benchmark_choice ~= 0
        [constraints.benchmark_id] = choose_relevant_benchmark(benchmark_choice, DateNum);
    end
    
    %% YTM range
    if constraints.YTM_choice ~= 0
        [constraints] = choose_max_YTM(constraints.YTM_choice, constraints);
    end
    
    %% Turnover range
    if constraints.MinTurnover_choice ~= 0
        [constraints] = choose_min_Turnover(constraints.MinTurnover_choice, constraints);
    end
    
    %% rating choice
    if constraints.rating_choice ~= 0
        [constraints] = choose_rating(constraints.rating_choice, constraints);
    end
    
    %%  sector choice
    if constraints.sector_choice ~= 0
        [constraints] = choose_sector(constraints.sector_choice, constraints);
    end
    
    %% choose type
    if constraints.type_choice ~= 0
        [constraints] = choose_type(constraints.type_choice, constraints);
    end
    
    %% choose weight per bond
    if constraints.wgt_per_bond_choice ~= 0
        [solver_props] = choose_wgt_per_bond(constraints.wgt_per_bond_choice, solver_props);
    end
    
    %% choose transaction costs
    if ~isempty(constraints.capital_TC)
        [solver_props] = choose_TC(constraints.capital_TC, solver_props);
    end
    
    %% choose special cases
    if ~isempty(constraints.special_case_choice)
        [solver_props] = choose_special_case(constraints.special_case_choice, solver_props);
    end
    
    if ~isempty(reinvestmentMethod)
        [constraints reinvChoice] = choose_reinvestment(reinvestmentMethod, constraints);
    end
    
    %% Portfolio creation
    
    %% get risk less rate for each of the portfolio durations calculated according to its investment horizon
    %% Without caching
    % interest_period = (1:6);
    % nominal_yield = get_nominal_yield_with_param(interest_period, DateNum, DateNum);
    
    %% With caching
    interest_period = 6; % (1:6); % Number represents number of periods wanted, possibilities are (1:12)/12, 1:6
    riskFreeDateIndex = find(riskFreeInfo.yield.dates <= DateNum, 1, 'last');
    
    dataYield = fts2mat(riskFreeInfo.yield(riskFreeDateIndex));
    solver_props.risk_free_interests = dataYield(end-interest_period+1:end);
    dataID = fts2mat(riskFreeInfo.id(riskFreeDateIndex));
    solver_props.risk_free_bond = dataID(end-interest_period+1:end);
    dataTTM = fts2mat(riskFreeInfo.ttm(riskFreeDateIndex));
    solver_props.risk_free_bond_ttm = dataTTM(end-interest_period+1:end);
    
    %% Filter
    TelBond20 = 13; % Number of months after beginning of data that index was born
    TelBond40and60 = 25; % Also means that we must go that many months into data before simulating
    
    if sim_num < nSimMonthsTotal*6 -  TelBond20 - 2*TelBond40and60;
        constraints.point_choice = 'MV';% For Omega change to 'Om'  Just for filterization purposes.
    else
        constraints.point_choice = 'Om';
    end
    
    if ~isempty(constraints.model_choice)
        [solver_props constraints constraints.point_choice] = choose_model(constraints.model_choice, constraints.point_choice, solver_props, constraints);
        constraints.is_markowitz = strcmp(constraints.model_choice, 'Marko'); %Will be used to check number of bonds versus days of history ***************
    end
    constraints.dynamic.port_dur = 8; % Random number, just to fill it in and be able to filter before setting port_duration
    [constraints] = choose_Bond_Duration_Range(constraints.BondDuration_choice, constraints);
    
    if isempty(filtered) %only filter once bond duration is unlimited
        [filtered ] = filter_function(constraints, DateNum);
    elseif ~strcmp(constraints.BondDuration_choice, 'Unlimited') %if bond duration is not portfolio duration dependent, filter again every iteration of loop
        [filtered ] = filter_function(constraints, DateNum);
    end
    
    filtered.NumBonds = length(filtered.nsin_list);
    %% Solver
    
    for port_duration = 1:port_dur_yrs %constraints.dynamic.port_dur  = 1:port_dur_yrs
        for pointFrontier = 1:numPoints
            try
                if pointFrontier == 1 && isfield(solver_props, 'WantedPortReturns')
                    solver_props = rmfield(solver_props, 'WantedPortReturns');
                end
                
                constraints.dynamic.port_dur = port_duration; % Set it to the correct value now.
                solver_props.port_duration_sought = port_duration;
                if strcmp(constraints.model_choice, 'Marko')
                    if isfield(solver_props, 'WantedPortReturns')
                        [constraints.point_choice constraints.target_choice sim_num_save] = set_model(constraints.model_choice, pointFrontier, solver_props.benchmarkSolutionFlag, solver_props.WantedPortReturns);
                    else
                        [constraints.point_choice constraints.target_choice sim_num_save] = set_model(constraints.model_choice, pointFrontier, solver_props.benchmarkSolutionFlag);
                    end
                    sim_num_save = 100000000 + 100000*sim_num_save + 10000*indexChoice + 1000*reinvChoice + 10*nMonthsBeginSim +solver_props.port_duration_sought;
                elseif strcmp(constraints.model_choice, 'Omeg')
                    
                    if isfield(solver_props, 'WantedPortReturns')
                        [constraints.point_choice constraints.target_choice sim_num_save] = set_model(constraints.model_choice, pointFrontier, solver_props.benchmarkSolutionFlag, solver_props.WantedPortReturns);
                    else
                        [constraints.point_choice constraints.target_choice sim_num_save] = set_model(constraints.model_choice, pointFrontier, solver_props.benchmarkSolutionFlag);
                    end
                    sim_num_save = 300000000 + 100000*sim_num_save + 10000*indexChoice + 1000*reinvChoice + 10*nMonthsBeginSim + solver_props.port_duration_sought;
                    
                end
                %marking the start time of the portfolio
                [ outStatus, outException ] = set_portfolio_time( sim_num, num2str(sim_num_save), true );
                if outStatus==0
                    error(outException);
                end
                %% frontier point
                if constraints.point_choice ~= 0
                    [solver_props] = choose_frontier_point(constraints.point_choice, constraints.target_choice, solver_props);
                end
                
                if filtered.NumBonds >= 1/solver_props.MaxBondWeight %Checks if there are enough bonds to solve
                    
                    account_name = ['PrePortCreation' num2str(sim_num_save)]; %account name to be saved ***************
                    solution = [];
                    
                    %% Solve
                    [solution nsin_data solver_props] = solver_ultimate_box(constraints, filtered, solver_props, DateNum, account_name);
                    
                    solution.ReinvestmentStrategy = constraints.ReinvestmentStrategy;
                    
                    %% Prepare for saving
                    [solution solver_props] = prep_save(constraints, solution, nsin_data, solver_props);
                    
                    %% Save solver log
                    if strcmp(constraints.model_choice, 'Marko')                        
                        solution.solverOutputDs.sim_num_save = cellstr(num2str(sim_num_save*ones(size(solution.solverOutputDs.sim_num_save))));
                        solution.solverOutputDs.second_excess_duration = cellstr(num2str(solution.solverOutputDs.second_excess_duration));
                        dsTemp=[dataset(ones(size(solution.solverOutputDs,1),1)*sim_num) solution.solverOutputDs];
                        dsTemp=set(dsTemp,'VarNames',{'simId','portfolioDesc','callNum','errFlag',...
                            'iterations','constrViolation','algo','cgIterations','msg',...
                            'firstOrderOpt','firstExcessDuration','secondExcessDuration',...
                            'infeasibleNonDurationConstStart','infeasibleDurationConstStart',...
                            'infeasibleNonDurationConstFinish','infeasibleDurationConstFinish'});
                        [outStatus, outException] = set_optimization_details( dsTemp );
                    end
                    
                    %% Save solution
                    [solution account_name ftsPrice nsin_non0 account_id owner_id manager_id investment_amount filter_id initial_allocation port_model] = save_solution(sim_num_save, solution, solver_props, constraints, DateNum, reinvChoice);
                    if ~isnan(solution.PortRetExpectedWReinvestment)
                        
                        nsin_data.riskFreeInfo = riskFreeInfo;
                        [ftsTotalHoldingValue ftsHoldingValuePerBond mtxTransactionsTotal] = calc_total_value_envelope(solution, nsin_data, DateNum, t_fin, constraints, ftsPrice);
                        holding = fts2mat(ftsHoldingValuePerBond(1))';
                        nanSpots = isnan(holding);
                        holding(nanSpots) = 0;
                        idsNsins = fieldnames(ftsHoldingValuePerBond);
                        nsin = strid2numid(idsNsins(4:end));
                        nNsin = length(nsin);
                        nSpot = 1;
                        count = zeros(nNsin, 1);
                        %get initial allocation according to final allocation gotten
                        %from cumulative_returns
                        n = 1;
                        while nSpot <= solution.NumBondsSol
                            if nsin(n) == nsin_non0(nSpot);
                                count(n) = solution.count(nSpot);
                                nSpot = nSpot + 1;
                            end
                            n = n+1;
                        end
                        
                        initial_allocation = dataset(nsin, count, holding); %create initial allocation
                        
                        %% Creation of the portfolio object:
                        saveStageIndex = 0; % Index describing the stage of saving - 0 = beginning, 1 = middle, 2 = end                        
                        port = portfolio(account_id, account_name, owner_id, manager_id, DateNum, DateNum, investment_amount, constraints.benchmark_id, filter_id, initial_allocation, port_model, [], filtered.dyn_param, constraints.static, constraints.sectors, constraints.rating, constraints.ReinvestmentStrategy, saveStageIndex);
                        
                        %% Save current portfolio into database:                        
                        err_msg =[];
                        save(port); % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previous version was [bool, err_msg] = port.save();
                        
%                         errorDS = [dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                        
                        %% Simulation:
                        yearsTotal = ceil(solution.PortDuration);
                        t = zeros(1,min(yearsTotal, 5));%Can't simulate more than five years - change in future!!!
                        
                        for j = 1:yearsTotal%%%%ADD _ WHAT HAPPENS WHEN PASSES t_fin
                            if t <  t_fin;
                                t(j) = DateNum + j*365;
                            else
                                t(j) = t_fin;
                            end
                        end
                                                
                        maxDate = min(t_fin, ftsTotalHoldingValue.dates(end));
                        t = min(t, maxDate);
                        
                        numT_fin = sum(t == max(t));
                        if numT_fin > 1 % Catch for the case that more than one date exceeded t_fin - so was made = t_fin
                           t = t(1:end - numT_fin +1); %Cut the extra t == t_fin, leave just one.
                        end
                        
                        numSavePoints = min(length(t), 5);
                        for  save_points = 1:numSavePoints
                            
                            if t(save_points)
                                
                                timeSpot = find(ftsTotalHoldingValue.dates >= t(save_points), 1, 'first');
                                if (isempty(timeSpot) && t(save_points) >= t_fin) || (save_points == numSavePoints)% In case actual date checked is beyond last
                                    timeSpot = length(ftsTotalHoldingValue.dates); % date of data, but not 6 take last data-date.
                                    t(save_points) = ftsTotalHoldingValue.dates(end);
                                end
                                
                                if timeSpot ==  length(ftsTotalHoldingValue.dates) %To ensure different date than below so no duplicate saving
                                    timeSpot =  length(ftsTotalHoldingValue.dates) - 1;
                                    t(save_points) = ftsTotalHoldingValue.dates(end) - 1;
                                end
                                if t(save_points) ~= ftsTotalHoldingValue.dates(timeSpot) %If don't fall on business day, set equal to date on which here exists cumulative
                                    t(save_points) = ftsTotalHoldingValue.dates(timeSpot);
                                end
                                holding = fts2mat(ftsTotalHoldingValue(timeSpot));
                                mtxHoldingValuePerBond= fts2mat(ftsHoldingValuePerBond);
                                holdingPerBond = mtxHoldingValuePerBond(timeSpot,:)';
                                holdingPerBond(isnan(holdingPerBond)) = 0;
                                
                                %% Update allocation
                                
                                % allocation = dataset(nsin, count, holding); % CHECK THIS!!
                                allocation = dataset(nsin, count, holdingPerBond, 'VarNames', {'nsin', 'count', 'holding'}); % CHECK THIS!!
                                if save_points ~= 1
                                    previous_update = t(save_points-1);
                                else
                                    previous_update = DateNum; % Initial time portfolio was saved
                                end
                                
                                saveStageIndex = 1; % Middle stage of saving (meaning - do not save cumulative return)
                                [riskFreeVec] = risk_free_for_period(riskFreeInfo.yield, constraints.dynamic.hist_len, DateNum);
                                mtxTransactionsPartial=cut_relevant_transaction(mtxTransactionsTotal,previous_update,t(save_points));
                                port = portfolio(port.account_id, account_name, owner_id, manager_id, DateNum, t(save_points), investment_amount, constraints.benchmark_id, filter_id, allocation, port_model, previous_update, filtered.dyn_param, constraints.static, constraints.sectors, constraints.rating, constraints.ReinvestmentStrategy, saveStageIndex, ftsTotalHoldingValue(1:timeSpot), ftsHoldingValuePerBond(1:timeSpot), riskFreeVec, mtxTransactionsPartial);
                                
%                                 err_msg =[];
                                 save(port); %bool returns yes no if saved or not. % Yigal: If the first output variable is not used, it is possible replace it with tilda. Previouse version was [bool, err_msg] = port.save();
%                                 if ~isempty(err_msg)
%                                     err_msg = [err_msg];
%                                 end
%                                 errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                                
                            end
%                             if ~bool
%                                 set_sim_err(t(save_points), sim_num_save , errorDS); % expected errors
%                             end
                        end
                        
                        saveStageIndex = 2; % Final stage of saving - meaning: save cumulative return
                        port_model.reinv_low_bnd = length(ftsTotalHoldingValue);
                        % port = portfolio(port.account_id, account_name, owner_id, manager_id, DateNum, FINAL_SAVE, investment_amount, benchmark_choice, filter_id, allocation, port_model, previous_update, ftsTotalHoldingValue, ftsHoldingValuePerBond, solver_props.risk_free_interests(port_dur_yrs));
                        [riskFreeVec] = risk_free_for_period(riskFreeInfo.yield, port_model.reinv_low_bnd, DateNum);
                        
                        mtxTransactionsPartial=cut_relevant_transaction(mtxTransactionsTotal,t(save_points),ftsTotalHoldingValue.dates(end));
                        port = portfolio(port.account_id, account_name, owner_id, manager_id, DateNum, ftsTotalHoldingValue.dates(end), investment_amount, constraints.benchmark_id, filter_id, allocation, port_model, previous_update, filtered.dyn_param, constraints.static, constraints.sectors, constraints.rating, constraints.ReinvestmentStrategy, saveStageIndex, ftsTotalHoldingValue, ftsHoldingValuePerBond, riskFreeVec, mtxTransactionsPartial);
%                         err_msg =[];
                        port.flagFinalSave = true;
                        save(port); %bool returns yes no if saved or not.
%                         err_msg = [err_msg];
%                         errorDS = [errorDS; dataset(cellstr(err_msg), 'VarNames', 'err_msg')];
                        ftsTotalHoldingValue(end)
                    end
                else
                    error('Not enough bonds through filter, no solution');
                end
                
            catch ME %inside for loop
                sizeME = size(ME.stack, 1);
                errorString = cell(1, sizeME);
                errorLineString = cell(1, sizeME);
                andString = ' and in ';
                inLineString = ' in line ';
                errorDescription = 'Error in ';
                for a = 1:sizeME
                    errorString{a} = ME.stack(a).file;
                    errorLineString{a} = num2str(ME.stack(a).line);
                end
                for a = 1:sizeME
                    loopErrorString = errorString{a};
                    loopErrorLineString = errorLineString{a};
                    errorDescription = [errorDescription, loopErrorString, inLineString, loopErrorLineString,  andString];
                end
                err_msg = [ME.message];
                [outStatus, outException] =set_error(sim_num, num2str(sim_num_save), datestr(now(), 'yyyy-mm-dd HH:MM:SS'), ...
                    err_msg, errorDescription);
                account_name =['PointDurationChoice'];
            end
            %setting the portfolio finish time
            [ outStatus, outException ] = set_portfolio_time( sim_num, num2str(sim_num_save), false );
            if outStatus==0
                error(outException);
            end
        end
    end
catch ME %general errors
    sizeME = size(ME.stack, 1);
    errorString = cell(1, sizeME);
    errorLineString = cell(1, sizeME);
    andString = ' and in ';
    inLineString = ' in line ';
    errorDescription = 'Error in ';
    for a = 1:sizeME
        errorString{a} = ME.stack(a).file;
        errorLineString{a} = num2str(ME.stack(a).line);
    end
    for a = 1:sizeME
        loopErrorString = errorString{a};
        loopErrorLineString = errorLineString{a};
        errorDescription = [errorDescription, loopErrorString, inLineString, loopErrorLineString,  andString];
    end
    account_name =['Filter' datestr(now)];
    err_msg = [ME.message];
    [outStatus, outException] =set_error(sim_num, num2str(sim_num_save), datestr(now(), 'yyyy-mm-dd HH:MM:SS'), ...
        err_msg, errorDescription);
    
end
end
