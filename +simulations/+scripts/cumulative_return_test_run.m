import dal.market.get.dynamic.*; % Because of: get_multi_dyn_between_dates
import dal.market.get.static.*; % Because of: get_bond_stat
import bll.cumulative_return.*;

%% Must be entered
% solution.nsin_sol = [                       1940048
%      1940147
%      1940188
%      2180024
%      2260040
%      2260081
%      6000012
%      6020010
%      6040075
%      6980098
% ];
 % [ 1094051 1097443	1106905	1107325	1110618	1110642	1112887	1117720	1460070	1610138];%; 1940147; 9266735];
% solution.nsin_sol =    5280045  ;4110110 % %multi redemption
% solution.count = [ 
%        1086
%        15898
%         3382
%        19062
%         8995
%          820
%         1215
%         1015
%         4390
%        10771
% ];
%solution.PortWts = [.333];%; .333; .333];
% solution.PortDuration = 1.5;
% DateNum =733303; %733303; 
% benchmarkID='AllBonds';
% dateFinalData = 735302;

%%Creation of solution

%solution.PortWts = solution.PortWts/sum(solution.PortWts);
% solution.portfolioMaturityDate = 733851; %DateNum + ceil(solution.PortDuration*365);
% solution.NumBondsSol = length(solution.nsin_sol);
% 
% [ftsPrice] = get_multi_dyn_between_dates(solution.nsin_sol, {'price_dirty'}, 'bond', DateNum, solution.portfolioMaturityDate);
% solution.initial_prices = fts2mat(ftsPrice.price_dirty(1))';
% %count_rough = 10000000 * solution.PortWts ./ solution.initial_prices; %100,000 Shekels
% 
% %solution.count = floor(count_rough);
% 
% 
% dataList = {'redemption_type'};
% nsinData = get_bond_stat(solution.nsin_sol, DateNum, dataList);
% x = strcmp(nsinData.redemption_type, 'multi');
% if isrow(x)
%     x = x';
% end
% 
% solution.flagMultiRedemption = x;
solution.count(end-1) =0;
[ftsTotalHoldingValue ftsHoldingValuePerBond] = calc_total_value_envelope(solution, DateNum, dateFinalData, benchmarkID, ftsPrice);
