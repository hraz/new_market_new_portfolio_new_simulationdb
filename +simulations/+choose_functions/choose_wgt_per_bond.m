function [solver_props] = choose_wgt_per_bond(wgt_per_bond_choice, solver_props)
%CHOOSE_WGT_PER_BOND function set maximum weight per bond allowed in
%solution.
%
%   [solver_props] = choose_wgt_per_bond(wgt_per_bond_choice, solver_props)
%   function receives point_choice chooses the
%   model.
%
%   input:
%        wgt_per_bond_choice - VALUE - maximum allowed weight per bond
%
%        solver_props
%
% output: handles with frontier point chosen.
%
    solver_props.MaxBondWeight = wgt_per_bond_choice;
    solver_props.MinNumOfBonds = ceil(1/solver_props.MaxBondWeight);

end
