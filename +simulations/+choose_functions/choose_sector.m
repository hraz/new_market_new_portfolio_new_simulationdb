function [constraints ] = choose_sector(sector_flag, constraints)
%
% function receives sector_flag and constraints and sets the sectors to be included in the filter according to the input
%
%
%   input:
%           sector_flag - string, one of the following choices: 'Unlimited'
%           'Fin' '~I&H' '~Bio'  'Ind' 'Risky'  with:
%               Unlimited - all sectors
%               Fin -    'BANKS'
%                        'COMMERCIAL BANKS'
%                        'FINANCIAL SERVICES'
%                        'GOVERNMENT'
%                        'INSURANCE'
%                        'INSURANCE COMPANIES'
%                        'INVESTMENT & HOLDINGS'
%                        'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
%                        'REAL-ESTATE AND CONSTRUCTION'
%               Ind -    'FASHION & CLOTHING'
%                        'FOREIGN SHARES AND OPTIONS'
%                        'INDEX PRODUCTS'
%                        'BIOMED'
%                        'CHEMICAL RUBBER & PLASTIC '
%                        'COMMERCE'
%                        'COMMUNICATIONS & MEDIA '
%                        'COMPUTERS'
%                        'ELECTRONICS'
%                        'FOOD'
%                        'GOVERNMENT'
%                        'HOTELS & TOURISM '
%                        'ISSUANCES'
%                        'METAL'
%                        'MISCELLANEOUS INDUSTRY'
%                        'OIL & GAS EXPLORATION'
%                        'SERVICES'
%                        'UNKNOWN'
%                        'WOOD & PAPER'
%              Risky -   'BIOMED'
%                        'COMMUNICATIONS & MEDIA '
%                        'INVESTMENT & HOLDINGS'
%                        'REAL-ESTATE AND CONSTRUCTION'
%
%       constraints - data structure containing constraints needs for filtering.
%
%   output: constraints with updated fields.
%
import dal.market.get.base_data.*;
%
 list = get_bond_base_static( 'sector'  );
 sectors = list.sector;

% sectors={  'FASHION & CLOTHING'
%     'FOREIGN SHARES AND OPTIONS'
%     'INDEX PRODUCTS';
%     'BIOMED'
%     'CHEMICAL RUBBER & PLASTIC '
%     'COMMERCE'
%     'COMMERCIAL BANKS'
%     'COMMUNICATIONS & MEDIA '
%     'COMPUTERS'
%     'ELECTRONICS'
%     'FINANCIAL SERVICES'
%     'FOOD'
%     'GOVERNMENT'
%     'HOTELS & TOURISM '
%     'INSURANCE COMPANIES'
%     'INVESTMENT & HOLDINGS'
%     'ISSUANCES'
%     'METAL'
%     'MISCELLANEOUS INDUSTRY'
%     'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
%     'OIL & GAS EXPLORATION'
%     'REAL-ESTATE AND CONSTRUCTION'
%     'SERVICES'
%     'STRUCTURED BONDS '
%     'UNKNOWN'
%     'WOOD & PAPER'};
switch sector_flag
    
    case  'Fin'
       % constraints.sectors.sectors_to_include = sectors([7 11 13 15 16 20
       % 22]); % Old list of sectors based on only 26 sectors
        constraints.sectors.sectors_to_include = sectors([1 5 11 14 17 18 19 24 26]);
    case 'Ind'
        % constraints.sectors.sectors_to_include = sectors([1:6 8:10 12:14 17:19 21 23 25 26]);
        constraints.sectors.sectors_to_include = sectors([2 4 6:11 13:16 21 22 23 25 27 29 30]);
    case 'Risky'
        % constraints.sectors.sectors_to_include = sectors([4 8 16 22]);
        constraints.sectors.sectors_to_include = sectors([2 6 19 26]);
    case 'Unlimited'
        % constraints.sectors.sectors_to_include= {};% sectors([1:26]);
        constraints.sectors.sectors_to_include = sectors([1:length(list)]);
    otherwise % Default
        constraints.sectors.sectors_to_include = {};
end


