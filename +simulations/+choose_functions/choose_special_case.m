function [solver_props] = choose_special_case(special_case_flag, solver_props)

% function receives point_flag chooses the
% model.
%
% input:
%        point_flag - string.  'MV' = mean variance
%                              'OS' = optimal sharpe
%                              'Tar#' = target return of # percent, # can
%                              be from 2-20
%                              'TarOS#' = target + optimal sharpe point of
%                              # percent, # can be from 2-20
%                              'HighMom' = Skewness Kurtosis

%
% output: handles with frontier point chosen.
%

switch special_case_flag
    case 'Z'
        solver_props.Zspread = 1;
    case 'Clean'
         solver_props.CleanCovarianceMatrixFlag = 1; %If we want to "clean up" the historical conariance matrix, we set this flag to 1  
   
end
