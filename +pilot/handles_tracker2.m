handles.nsin=nsin_list;
handles.DateNum=req_date;
handles.ExpInflation=0.03;
handles.UseTermStructure=0;
handles.MaxPortDuration=port_duration_max;
handles.MinPortDuration=portfolio_duration-ceil(0.25*portfolio_duration);
handles.HistLen=hist_len;
handles.RisklessRate=0.025;
handles.Sigma_eiMethod=1;
handles.NumBonds=length(handles.nsin);
handles.TypeFilterParams=cell(7,1);
handles.UserBondTypes=ones(7,1);
handles.BondsPerType0 =  zeros(7,1);%handles.UserBondTypes';
handles.BondsPerType =  zeros(7, 1);%  handles.UserBondTypes';
handles.CorpGovTypes=[0 0];
handles.SpecBonHistLenError=0;
handles.SpecBondLoc=[];
handles.MaxWeight=[];
handles.SpecBondCon=[];
handles.MinWeight=[];
handles.MaxTypeWeight=ones(7,1);
handles.MinTypeWeight=zeros(7,1);
handles.TargetBetaFlag=0;
handles.MinBondWeight=0;
handles.MaxBondWeight=0.1;

handles.AllSectors={  'FASHION & CLOTHING'
    'FOREIGN SHARES AND OPTIONS'
    'INDEX PRODUCTS';
    'BIOMED'
    'CHEMICAL RUBBER & PLASTIC '
    'COMMERCE'
    'COMMERCIAL BANKS'
    'COMMUNICATIONS & MEDIA '
    'COMPUTERS'
    'ELECTRONICS'
    'FINANCIAL SERVICES'
    'FOOD'
    'GOVERNMENT'
    'HOTELS & TOURISM '
    'INSURANCE COMPANIES'
    'INVESTMENT & HOLDINGS'
    'ISSUANCES'
    'METAL'
    'MISCELLANEOUS INDUSTRY'
    'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS'
    'OIL & GAS EXPLORATION'
    'REAL-ESTATE AND CONSTRUCTION'
    'SERVICES'
    'STRUCTURED BONDS '
    'UNKNOWN'
    'WOOD & PAPER'};
handles.MaxSectorWeight=ones(size(handles.AllSectors));
handles.MinSectorWeight=zeros(size(handles.AllSectors));
% handles.MarkowitzG=0;
% handles.SingleIndexG=1;
% handles.MultiIndexG=0;
handles.ConMinNumOfBonds=0;
handles.ConMaxNumOfBonds=0;
handles.ConMinWeight2AlloBonds=0;
handles.ConMaxWeight2NonAlloBonds=0;
handles.FminConFlag=0;
handles.MinNumOfBonds=10;
handles.MinNumOfBonds0=10;
% handles.MinVar=0;
% handles.TarRetType=1;
% handles.Opt=0;
% handles.OptTarRet=0;
% handles.NumPorts=1;
% handles.TarRet=12;
handles.MyBonds=[];
handles.MyWeights=[];
handles.MaxNumOfBonds=15;
handles.MinWeight2AllocBonds=0.05;
handles.MaxWeight2NonAllocBonds=1e-4;
% in SolverPilot
handles.BondsHistLenRatioError=0; % instead of MiscSolverCallbackSettingsNoGui
handles.ResMat = []; % instead of MiscSolverCallbackSettingsNoGui