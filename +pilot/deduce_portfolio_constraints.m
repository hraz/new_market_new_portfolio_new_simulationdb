function deduce_portfolio_constraints (in_string)
%% intializing
import dal.market.get.risk.*;
import bll.inflation_mdl.*;
import dal.market.get.dynamic.*;
import dal.market.get.static.*;
import utility.pilot_utility.*;

error_id=0;
out_string=[];

%%  parsing

in_string_split=regexp(in_string, '\','split');
field_name=cell(1,length(in_string_split)-1);
field_data=cell(1,length(in_string_split)-1);
for m=2:length(in_string_split)
    input_temp=regexp(in_string_split{m}, ':','split');
    field_name{m-1}=input_temp{1};
    field_data{m-1}=input_temp{2};
end
% \security_list:[security1,unit1;security2,unit2;�;security,unitN]
if length(field_data)>=2
    nsins_string=field_data{find(strcmp('security_list',field_name))};
    eval(['A=' nsins_string ';'])
    B=sortrows(A,1); %this is needed since the data functions output the data with sorted nsins, so in order to be able to match the the nsins with their corresponding data, it is better to sort the units and nsin according to nsins
    nsins = B(:,1);
    units = B(:,2);
    req_date=field_data{find(strcmp('current_date',field_name))};
else
    %error in number of inputs
    error_id=999;
end

%% Get my bonds data in the current date
static_data_names = {'nsin','bond_type','linkage_index','coupon_type','sector'};
Dataset = get_bond_stat( nsins, req_date, static_data_names);                     %extract all static data fields at once
dynamic_data_name = {'ytm','ttm', 'price_dirty','volume'};
tbl_name = 'bond';
%replace the following with the true history length of all of the user bonds
MyMinHistLen = 60;
MyMaxHistLen = 200;
ExpInflation = 0.02; %take expected inflation expectations from the user/banks
start_date = datenum(req_date) -  MyMinHistLen*2; %TODO: if we want HistLen observations, we need to set end_date to approximatelly DateNum + HistLen/(5/7) (since there are 5 working days, and 2 days off every week...).
end_date = req_date;
MultiBondsFTS = get_multi_dyn_between_dates( nsins , dynamic_data_name, tbl_name, start_date, end_date);      %extract all dynamic data fields at once
RankDataSet = get_bond_risk(nsins, req_date);
YTM=fts2mat(MultiBondsFTS.ytm(end)).';
YTM=AddExpInflation(YTM,Dataset.linkage_index,ExpInflation);  %add expected inflation to YTM.
Duration=fts2mat(MultiBondsFTS.ttm(end)).';
price_dirty = fts2mat(MultiBondsFTS.price_dirty(end)).';
money=units.*price_dirty;
weights=money/sum(money);
weighted_YTM=weights'*YTM;
out_string=strcat(out_string,'\error_message:',num2str(error_id)); %print error. If there could be other error in the sequal, this line shoule be moved to the end of the string to allow good cover edge before outping the error message

%% Types
out_string=strcat(out_string,'\static_constraints:'); %starting the static constraints list, right now only 'basic_type'

MyTypes=Dataset.bond_type;
MyLinkage=Dataset.linkage_index;
MyCouponType=Dataset.coupon_type;

NewTypeID = {'1000','1001','1002','1003','1004'}; %these are the 5 groups of types: Corp NIS,CPI,USD/EURO/UKP and Gov NIS,CPI
MyUserBondTypes = zeros(1,5);
MyCorpNIS_ind = (strcmp(MyTypes,'Corporate Bond')==1 & strcmp(MyLinkage,'NON LINKED')==1);
MyCorpNIS=nsins(MyCorpNIS_ind);
MyUserBondTypes(1)=1 - isempty(MyCorpNIS);

MyCorpCPI_ind = (strcmp(MyTypes,'Corporate Bond')==1 & strcmp(MyLinkage,'CPI')==1 );
MyCorpCPI=nsins(MyCorpCPI_ind);
MyUserBondTypes(2)=1-isempty(MyCorpCPI);

MyCorpAbroad_ind = (strcmp(MyTypes,'Corporate Bond')==1 & ( strcmp(MyLinkage,'USD')==1 | strcmp(MyLinkage,'EURO')==1 | strcmp(MyLinkage,'UKP')==1));
MyCorpAbroad=nsins(MyCorpAbroad_ind);
MyUserBondTypes(3)=1-isempty(MyCorpAbroad);

MyGovNIS_ind = (strcmp(MyTypes,'Government Bond Shachar')==1 | strcmp(MyTypes,'Government Bond New Gilon')==1 | strcmp(MyTypes,'Government Bond Gilon')==1);
MyGovNIS=nsins(MyGovNIS_ind);
MyUserBondTypes(4)=1-isempty(MyGovNIS);

MyGovCPI_ind =(strcmp(MyTypes,'Government Bond Galil')==1 | strcmp(MyTypes,'Government Bond Improved Galil')==1);
MyGovCPI=nsins(MyGovCPI_ind);
MyUserBondTypes(5)=1-isempty(MyGovCPI);

% find min and max weight for each type
tmp_check=MyUserBondTypes;
MyMinTypeWeight = zeros(5,1); %one for each type
MyTypeTol = 0.1; %user should enter this number - tolerance for type allocation. sinc the use has a weight for each type, we need find the efficient with the weights as close as possible to the user weights. these weights are given  by the user
if tmp_check(1) MyMinTypeWeight(1) = max( sum(weights(MyCorpNIS_ind)) - MyTypeTol , 0); end
if tmp_check(2) MyMinTypeWeight(2) =  max( sum(weights(MyCorpCPI_ind)) - MyTypeTol , 0); end
if tmp_check(3) MyMinTypeWeight(3) =  max( sum(weights(MyCorpAbroad_ind)) - MyTypeTol , 0);end
if tmp_check(4) MyMinTypeWeight(4) =  max( sum(weights(MyGovNIS_ind)) - MyTypeTol , 0);end
if tmp_check(5) MyMinTypeWeight(5) =  max( sum(weights(MyGovCPI_ind)) - MyTypeTol , 0);end


MyMaxTypeWeight=ones(5,1);
if tmp_check(1) MyMaxTypeWeight(1) = min( sum(weights(MyCorpNIS_ind)) + MyTypeTol , 1); end
if tmp_check(2) MyMaxTypeWeight(2) = min( sum(weights(MyCorpCPI_ind)) + MyTypeTol , 1);end
if tmp_check(3) MyMaxTypeWeight(3) = min( sum(weights(MyCorpAbroad_ind)) + MyTypeTol , 1);end
if tmp_check(4) MyMaxTypeWeight(4) = min( sum(weights(MyGovNIS_ind)) + MyTypeTol , 1);end
if tmp_check(5) MyMaxTypeWeight(5) = min( sum(weights(MyGovCPI_ind)) + MyTypeTol , 1);end

flag=0;
for j=1:length(MyMaxTypeWeight)
    if MyUserBondTypes(j)
        if flag==0
            out_string=strcat(out_string,'15,',NewTypeID{j});
        else
            out_string=strcat(out_string,';15,',NewTypeID{j});
        end
        out_string=strcat(out_string,',',sprintf('%1.3f',MyMinTypeWeight(j)));
        out_string=strcat(out_string,',',sprintf('%1.3f',MyMaxTypeWeight(j)));
        flag=1; %mark that this is the first time, so ne ',' is needed before the number
    end
end

%% Sectors

%TODO: replace this with an automatic data retrival fromthe DB
AllSectors= {'BIOMED','CHEMICAL RUBBER & PLASTIC','COMMERCE','COMMERCIAL BANKS','COMMUNICATIONS & MEDIA' ,'COMPUTERS',...
'ELECTRONICS','FASHION & CLOTHING','FINANCIAL SERVICES','FOOD','FOREIGN SHARES AND OPTIONS','HOTELS & TOURISM' ,'INDEX PRODUCTS',...
'INSURANCE COMPANIES','INVESTMENT & HOLDINGS','ISSUANCES','METAL','MISCELLANEOUS INDUSTRY','MORTGAGE BANKS AND FINANCIAL INSTITUTIONS',...
'OIL & GAS EXPLORATION','REAL-ESTATE AND CONSTRUCTION','SERVICES','STRUCTURED BONDS' ,'WOOD & PAPER','GOVERNMENT','UNKNOWN'};
SectorID=[625,616,604,601,802,607,614,612,608,611,622,606,801,603,619,800,613,618,602,620,609,605,623,617,1,0];


% sector_ds = get_bond_base_static('sector');
% AllSectors = sector_ds.sector;
MySectorTol = 0.1; %we allow to allocate +-10% for each sector that the user is exposed to.
MyUserSectors = zeros(length(AllSectors),1);
MyMinSectorWeight = zeros(length(AllSectors),1);
MyMaxSectorWeight = zeros(length(AllSectors),1);
MySectorID= zeros(length(SectorID),1);
counter=0;
for i=1:length(AllSectors)
    CurrSecInds = strcmp(Dataset.sector,AllSectors(i));
    MyUserSectors(i)= (sum(CurrSecInds) > 0);
    if  MyUserSectors(i)
        if strcmp(AllSectors{i},'GOVERNMENT') continue; end %if the current sector is government, don't add it to the sector list
        counter=counter+1;
        MySectorID(counter)= SectorID(i);
        MyMinSectorWeight(counter) = max(sum(weights(CurrSecInds)) - MySectorTol,0);%cannot be lower than 0
        MyMaxSectorWeight(counter) = min(sum(weights(CurrSecInds)) + MySectorTol,1);%cannot be higher than 100
    end
end

flag=0;
out_string=strcat(out_string,'\sector_constraint:');
for j=1:counter
    if flag==0
        out_string=strcat(out_string,num2str(MySectorID(j)));
    else
        out_string=strcat(out_string,';',num2str(MySectorID(j)));
    end
    out_string=strcat(out_string,',',sprintf('%1.3f',MyMinSectorWeight(j)));
    out_string=strcat(out_string,',',sprintf('%1.3f',MyMaxSectorWeight(j)));
    flag=1;
end

%% Ranks
Rankmaalot={'AAA' ; 'AA+' ;'AA'; 'AA-'; 'A+'; 'A'; 'A-';...
    'BBB+' ;'BBB'; 'BBB-' ;'BB+'; 'BB'; 'BB-'; 'B+' ;'B'; 'B-';...
    'CCC+'; 'CCC'; 'CCC-'; 'CC'; 'C';'SD'; 'D'; 'NR'; 'NVR'};
Rankmidroog={'Aaa'; 'Aa1' ; 'Aa2'; 'Aa3' ;'A1'; 'A2'; 'A3';...
    'Baa1' ;'Baa2'; 'Baa3' ;'Ba1'; 'Ba2'; 'Ba3'; 'B1' ;'B2'; 'B3';...
    'Caa1'; 'Caa2'; 'Caa3'; 'Ca'; 'C'; 'NR';'NVR'};

if ~error_id
    maalots=cellfun(@(x) find(strcmp(x,Rankmaalot)),RankDataSet.rating_maalot(~strcmp(RankDataSet.rating_maalot,'RF'))); %'RF' ranking corresponds to Government. Thus, this rank should be ignored when comparing to the ranks latter
    midroogs=cellfun(@(x) find(strcmp(x,Rankmidroog)),RankDataSet.rating_midroog(~strcmp(RankDataSet.rating_midroog,'RF')));
    maalot_rating_min_id=length(Rankmaalot) - max(maalots);
    maalot_rating_max_id=length(Rankmaalot) - min(maalots); %note that the min number corresponds to the max rank (the best rank)
    midroog_rating_min_id=length(Rankmidroog) - max(midroogs);
    midroog_rating_max_id=length(Rankmidroog) - min(midroogs);
    out_string=strcat(out_string,'\rating_constraints:',num2str(maalot_rating_min_id));
    out_string=strcat(out_string,',',num2str(maalot_rating_max_id));
    out_string=strcat(out_string,',',num2str(midroog_rating_min_id));
    out_string=strcat(out_string,',',num2str(midroog_rating_max_id));
end

%% History Length
out_string=strcat(out_string,'\dynamic_constraints:'); %starting the dynamic constraints list

out_string=strcat(out_string,'history_length_constraint,');
out_string=strcat(out_string,sprintf('%d',MyMinHistLen));
out_string=strcat(out_string,',',sprintf('%d',MyMaxHistLen),';');

%% Durations
MyMinDuration=min(Duration);
MyMaxDuration=max(Duration);
out_string=strcat(out_string,'6,');
out_string=strcat(out_string,sprintf('%1.2f',MyMinDuration));
out_string=strcat(out_string,',',sprintf('%1.2f',MyMaxDuration),';');

%% Volume
Volume=fts2mat(MultiBondsFTS.volume);
MyMinMonetary=min(mean(Volume));
MyMaxMonetary=max(mean(Volume));
out_string=strcat(out_string,'7,');
out_string=strcat(out_string,sprintf('%1.2f',MyMinMonetary/1e6)); %only two digitis after the decimal point
out_string=strcat(out_string,',',sprintf('%1.2f',MyMaxMonetary/1e6),';');

%% YTM allowed
MyMinYTM=0;
MyMaxYTM=max(YTM);
out_string=strcat(out_string,'9,');
out_string=strcat(out_string,sprintf('%1.2f',MyMinYTM)); %only two digitis after the decimal point
out_string=strcat(out_string,',',sprintf('%1.2f',MyMaxYTM));

%% Weight per bond
MyMinBondWeight = 0;
MyMaxBondWeight = max(weights);
out_string=strcat(out_string,'\bond_weight_constraints:');
out_string=strcat(out_string,sprintf('%1.2f',MyMinBondWeight)); %only two digitis after the decimal point
out_string=strcat(out_string,',',sprintf('%1.2f',MyMaxBondWeight));

%% Invested_amount
out_string=strcat(out_string,'\basic_constraints:'); %starting the basic constraints list

out_string=strcat(out_string,'1,',num2str(sum(money)),';');

%% Portfolio duration
MyPortDuration = weights.'*Duration; %weighted average of durations

% NEW VERSION - returns port_dur_id [integer]
[MyPortDurationId notification_id]=port_dur_years2id(MyPortDuration);
out_string=strcat(out_string,'2,',num2str(MyPortDurationId));
out_string=strcat(out_string,'\notification_id:',num2str(notification_id));

% % OLD VERSION - returns min& max values of port_dur [years]
% MyDurationTol = 0.2; %allow a duation of +-20% with respect to the original user average duration
% MyMinPortDuration = MyPortDuration*(1 - MyDurationTol);
% MyMaxPortDuration = MyPortDuration*(1 + MyDurationTol);
% out_string=strcat(out_string,'\portfolio_duration_constraint:',num2str(MyPortDurationId));
% out_string=strcat(out_string,sprintf('%1.2f',MyMinPortDuration)); %only two digitis after the decimal point
% out_string=strcat(out_string,',',sprintf('%1.2f',MyMaxPortDuration));

%% Portfolio duration - -CORRECT
[portfolio_duration port_duration_min_JUNK port_duration_max_JUNK] = port_dur_id2years(MyPortDurationId);
maturity_date=floor(portfolio_duration*365 + datenum(req_date));
out_string=strcat(out_string,'\portfolio_maturity_date:',datestr(maturity_date,'yyyy-mm-dd'));


%% target return
out_string=strcat(out_string,'\target_constraint:'); %starting the basic constraints list

 [exp_return_JUNK exp_return_annually]=calculate_expected_return(nsins,weights,datenum(req_date),MyMinHistLen*2,maturity_date,1,ExpInflation,Dataset);
% CURRENT VERSION
target_return_id=target_return2id(exp_return_annually,YTM);
out_string=strcat(out_string,'3,',num2str(target_return_id));

% % SUGGESTED VERSION
% out_string=strcat(out_string,'3,',sprintf('%1.4f',exp_return_annually));

fprintf(1,'%s\n',out_string)  %if doesn't work: might need to change to: out_string{1}
