function YTM=AddExpInflation(YTM,Linkage,ExpInflation)
%Add the expected inflation to the bonds who are linked to the CPI ,in order to obtain the true YTM.
CPIindices=strcmp(Linkage,'CPI'); %find indices of bonds (Gov and Corp) that are linked to CPI
YTM(CPIindices)=YTM(CPIindices)+ExpInflation; %add the inflation only to those indics that are linked to CPI

%% old code, which calculated the days until the end of the year...etc. Check if has some good ideas behind it.
% Today=datenum(MosetRecentDate,'dd/mm/yyyy'); %find the serial number which represents today's date
% Tmp=MosetRecentDate; %change the month and the day of the current date, to match the last day of the year
% Tmp(4:5)='12'; %put december as the last month
% Tmp(1:2)='30'; %put the last day of the month
% LastDayOfCurrentYear=Tmp; %Now the last day is 31/12/YEAR
% LastDay=datenum(LastDayOfCurrentYear,'dd/mm/yyyy'); %find the serial number of the last day of the year
% DaysUntilEndOfCurrentYear=LastDay-Today; %number of days until the end of the year
% power=DaysUntilEndOfCurrentYear/365; %This the the power of (1+r). if we are on Jan 1st, DaysUntilEndOfCurrentYear=365, and the inflation is r.
% ExpInflationUntilEndOfYear=(1+ExpInflation)^power-1; %after raising to the power, substruct 1 to get the inflation
