function [ ucg ] = ar2( cg, alpha1, alpha2 )
%AR2 second order autoregressive reverse filter approach to unsmooth non-liquid assets' return.
%
%   [ ucg ] = ar2( cg, alpha1, alpha2 )
%   In the AR2 model, the second order means that we include the first two lags to unsmooth the
%   data. Each lagged capital growth return is multiplied by a selected unsmoothing parameter. The
%   approach assumes the following:
%       1) The mean is the same for the adjusted (unsmoothed) and unadjusted (smoothed) return
%       series.
%       2) The model parameters are the same over time.
%       3) There are no residuals (no random errors).
%       4) The unsmoothed capital growth rate, UCG, is calculated as:
%                   UCG = [CG(t) - alpha1 * CG(t-1) - alpha2 * CG(t-2)] / (1- alpha1 - alpha2)
%           where:
%           CG(t)	- capital growth for time t
%           alpha1	- first order autoregressive unsmoothing parameter
%           alpha2  - second order autoregressive unsmoothing parameter
%
%   Input:
%       cg - is an NOBSERVATIONSxMASSETS financial time series specifying some smoothing process.
%
%       alpha1 - is a scalar value specifying the first order autoregressive unsmoothing parameter (0.4 < alpha < 0.6).
%
%       alpha2 - is a scalar value specifying the second order autoregressive unsmoothing parameter (0.4 < alpha < 0.6).
%
%   Output:
%       ucg - is (N-1)OBSERVATIONSxMASSETS financial timeseries specifying the unsmoothed process.

% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import utility.fts.*;
    
    %%  Input validation:
    error(nargchk(3,3,nargin));
    
    if isempty(cg)
        error('unsmoothing:ar2:mismatchInput', 'An empty value of the smoothed data.');
    elseif ~isa(cg, 'fints')
        error('unsmoothing:ar2:wrongInput', 'Wrong type of the smoothed data.');
    end
    
    if isempty(alpha1)
        error('unsmoothing:ar2:mismatchInput', 'An empty value of the first unsmoothing parameter.');
    elseif ~isnumeric(alpha1)
        error('unsmoothing:ar2:wrongInput', 'Wrong type of the first unsmoothing parameter.');
    end
    
    if isempty(alpha2)
        error('unsmoothing:ar2:mismatchInput', 'An empty value of the second unsmoothing argument.');
    elseif ~isnumeric(alpha2)
        error('unsmoothing:ar2:wrongInput', 'Wrong type of the second unsmoothing argument.');
    end
    
    %% Step #1 (Get data from the cg): 
    names = tsnames(cg);
    dates = cg.dates(3:end);
    
    cg = fts2mat(cg);
    cgt = cg(3:end, :);
    cgt_1 = cg(2:end-1, :);
    cgt_2 = cg(1:end-2, :);
    
    %% Step #2 (Unsmooth the given data):
    unsmoothed_data = (cgt - alpha1 .* cgt_1 - alpha2 .* cgt_2) ./ (1- alpha1 - alpha2);
    
    %% Step #3 (Create the return fints):
    ucg = fints(dates, unsmoothed_data, names);

end
