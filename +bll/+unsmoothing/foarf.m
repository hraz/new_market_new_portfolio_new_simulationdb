function [ ucg ] = foarf( cg, alpha )
%FOARF first order autoregressive reverse filter approach to unsmooth non-liquid assets' return.
%
%   [ ucg ] = foarf( cg, alpha )
%   In the FOARF model, the first order simply means that we include a 1-period lag (i.e. the
%   capital growth return from the prior period) and multiply the lagged return by a constant
%   unsmoothing parameter. The approach assumes the following:
%       1) The mean is the same for the adjusted (unsmoothed) and unadjusted (smoothed) return
%       series.
%       2) The model parameters are the same over time.
%       3) There are no residuals (no random errors).
%       4) The unsmoothed capital growth rate, UCG, is calculated as:
%                   UCG = [CG(t) - alpha * CG(t-1)] / (1- alpha)
%           where:
%           CG(t)	- capital growth for time t
%           alpha	 - unsmoothing parameter
%
%   Input:
%       cg - is an NOBSERVATIONSxMASSETS financial time series specifying some smoothing process.
%
%       alpha - is a scalar value specifying the unsmoothing parameter (0.4 < alpha < 0.6).
%
%   Output:
%       ucg - is (N-1)OBSERVATIONSxMASSETS financial timeseries specifying the unsmoothed process.
%
%   Note: Based on FOARF approach for real estate returns.

% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import utility.fts.*;
    
    %%  Input validation:
    error(nargchk(2,2,nargin));
    if isempty(cg)
        error('unsmoothing:foarf:mismatchInput', 'An empty value of the first argument.');
    elseif ~isa(cg, 'fints')
        error('unsmoothing:foarf:wrongInput', 'Wrong type of the first argument.');
    end
    if isempty(alpha)
        error('unsmoothing:foarf:mismatchInput', 'An empty value of the second argument.');
    elseif ~isnumeric(alpha)
        error('unsmoothing:foarf:wrongInput', 'Wrong type of the second argument.');
    end
    
    %% Step #1 (Get data from the cg): 
    names = tsnames(cg);
    dates = cg.dates(2:end);
    cgt = fts2mat(cg(2:end));
    cgt_1 = fts2mat(cg(1:end-1));
    
    %% Step #2 (Unsmooth the given data):
    unsmoothed_data = (cgt - alpha .* cgt_1) ./ (1- alpha);
    
    %% Step #3 (Create the return fints):
    ucg = fints(dates, unsmoothed_data, names);

end