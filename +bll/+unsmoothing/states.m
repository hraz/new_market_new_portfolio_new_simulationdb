function [ ucg ] = states( cg, alpha )
%STATES first order autoregressive reverse filter approach to unsmooth non-liquid assets' return.
%
%   [ ucg ] = states( cg, alpha )
%   In the STATES approach, the first order simply means that we include a 1-period lag (i.e. the
%   capital growth return from the prior period) and multiply the lagged return by a constant
%   unsmoothing parameter. But, the unsmoothing parameter alpha is allowed to change depending on
%   the state of the market. The approach assumes the following: 
%       1) The mean is the same for the adjusted (unsmoothed) and unadjusted (smoothed) return
%       series.
%       2) The model parameters are the same over time.
%       3) There are no residuals (no random errors).
%       4) The unsmoothed capital growth rate, UCG, is calculated as:
%                   UCG = [CG(t) - alpha * CG(t-1)] / (1- alpha)
%           where:
%           CG(t)	- capital growth for time t
%           alpha	 - unsmoothing parameter
%
%   Unsmoothing parameter adjustments for the STATES approach:
%   -----------------------------------------------------------------------------------------------------------------------------------------
%                           Ranges below the mean                           |    Mean return    |                   Ranges above the mean
%   -----------------------------------------------------------------------------------------------------------------------------------------
%   Ri<Avg-2std | Avg-2std<Ri<Avg-std | Avg-std<Ri<Avg |          Avg          | Avg<Ri<Avg+std | Avg+std<Ri<Avg+2std | Avg+2std<Ri
%   -----------------------------------------------------------------------------------------------------------------------------------------
%   alpha+0.25   |          alpha + 0.15       |     alpha + 0.05    |          alpha         |             alpha        |         alpha + 0.10         |  alpha + 0.20
%   -----------------------------------------------------------------------------------------------------------------------------------------
%
%   Input:
%       cg - is an NOBSERVATIONSxMASSETS financial time series specifying some smoothing process.
%
%       alpha - is a scalar value specifying the unsmoothing parameter (0.4 < alpha < 0.6).
%
%   Output:
%       ucg - is (N-1)OBSERVATIONSxMASSETS financial timeseries specifying the unsmoothed process.
%

% Yigal Ben Tal
% Copyright 2012

    %% Import external packages:
    import utility.fts.*;
    
    %%  Input validation:
    error(nargchk(2,2,nargin));
    if isempty(cg)
        error('unsmoothing:states:mismatchInput',  'An empty value of the smoothed data.');
    elseif ~isa(cg, 'fints')
        error('unsmoothing:states:wrongInput',  'Wrong type of the smoothed data.');
    end
    if isempty(alpha)
        error('unsmoothing:states:mismatchInput', 'An empty value of the unsmoothing parameter.');
    elseif ~isnumeric(alpha)
        error('unsmoothing:states:wrongInput', 'Wrong type of the unsmoothing parameter.');
    end
    
    %% Step #1 (Get data from the cg): 
    names = tsnames(cg);
    dates = cg.dates(2:end);
    cgt = fts2mat(cg(2:end));
    cgt_1 = fts2mat(cg(1:end-1));
    
    %% Step #2 (Build statistical momets of smoothed data):
    R = nanmean(cg);
    R = R(ones(size(cgt,1),1),:);
    s = nanstd(cg);
    s = s(ones(size(cgt,1),1),:);
    
    %% Step #3 (Build alpha according to market state):
    alpha = alpha(ones(size(cgt)));
    alpha(cgt < R) = alpha(cgt < R) + 0.25;
    alpha(cgt >= R-2*s & cgt < R-s) = alpha(cgt >= R-2*s & cgt < R-s) + 0.15;
    alpha(cgt >= R-s & cgt < R) = alpha(cgt >= R-s & cgt < R) + 0.05;
    alpha(cgt >= R+s & cgt < R+2*s) = alpha(cgt >= R+s & cgt < R+2*s) + 0.10;
    alpha(cgt >= R+2*s) = alpha(cgt >= R+2*s) + 0.20;
    
    %% Step #4 (Unsmooth the given data):
    unsmoothed_data = (cgt - alpha .* cgt_1) ./ (1- alpha);
    
    %% Step #5 (Create the return fints):
    ucg = fints(dates, unsmoothed_data, names);

end