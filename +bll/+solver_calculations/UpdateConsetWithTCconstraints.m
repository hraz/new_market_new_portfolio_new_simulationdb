function [Daily_YTM2 ExpCovMat2 ConSet2 InitWeight2] = UpdateConsetWithTCconstraints(buy_cost, sell_cost, InitWeight, Daily_YTM, ExpCovMat, ConSet, MaxBondWeight)
%update the relevant metrics in order to take the transactio costs (TC)
%constraitns. "Based on the paper Rebalancing an Investment Portfolio in the
%presence of TC"
num_nsin = length(Daily_YTM);                                   %number of nsin
if isempty(InitWeight)                                          %if there are no MyWeights used as InitWeights, set the initial weights to 1/N
    InitWeight = 1/num_nsin * ones(num_nsin, 1);
end
added_zeros = zeros(2*num_nsin +1, 1); %for each weight to be found, there are two new variables - buy and sell weight. 
                                       % In addition, there is the "t" variable, which is also needed to be found
InitWeight2 = [InitWeight; added_zeros]; %update the initial weights with zeros that corresponds to the buy,sell and t variables. 
                                         % This is just in order to increase the size of the initial weights vector, 
                                         % to match the new size of the problem: 3*num_nsin+1
Daily_YTM2 = [Daily_YTM; added_zeros];  %same for the YTM vector. There is no meaning for YTM of the new variables 
                                        % (since they represent the buy and sell weight, so we just add zeros

%% construct the new expected covariance matrix, which is based on the original ExpCovMat, zero padded to 3*num_nsin+1 X 3*num_nsin+1 matrix
RightTop = zeros(num_nsin, 2*num_nsin +1); %the new covariance matrix is of size 3*num_nsin+1 X 3*num_nsin+1, so we need to add zeros
                                           %  to fill it up (again, no meaning for correlation between the weights of the buy and sell - so we add zeros)
LeftBot= zeros(2*num_nsin + 1, num_nsin);
RightBot = zeros(2*num_nsin+1);
ExpCovMat2 = [ExpCovMat RightTop; LeftBot RightBot];            %This will be the new covariance matrix which will be entered to the solver

num = size(ConSet);
num_var = num(2) - 1; %number of variables (last column corresponds to the constraints, and not to the assets)
num_const = num(1);
A = ConSet(:, 1:num_var);   %constraints equations/groups/sectors/ etc. ConSet =[ A b] so the first num_var columns corresponds to A, 
                            % while the last column correspond to b
b = ConSet(:, num_var+1); %ranges

%% const 1: x^ - u^ +v^ - x_bar*t = 0. New weight should be equal to the original weight, minus what was sold, plus what was bought.
%  t expressed the reduction in the the sum of weight of the portfolio due to TC (it isn't 100% anymore, but 99.8%, for example)  
%  x^: new weight, u^: weight to buy, v^: weight to sell, x_bar: original weights
id_num = eye(num_nsin);
first_const = [id_num -id_num id_num -InitWeight];          %remember to add num_nsin 0's to new_b
const1 = [first_const; -first_const];                       %the constraint required is an equality, so we also do the negative constraint
b_1 = zeros(1,2*num_var)'; %add zeros for each of above constraints in b vector (since the constraint (right hand side of the equation) equals zero)


%% const 2: 0*x + (buy_cost +e)'u^ +(sell_cost - e)'v^ + 0*t = 0, e is a unit vector, this is a scalar constraint. Expresses two combined constraints:
% The sum of all new weights should add up to almost 1 (due to TC that were paind in the transaction) and the first constraint
e_vec = ones(1, num_var);                               %unit vector
buy_vec = buy_cost + e_vec; sell_vec = sell_cost-e_vec;
trans_vec = [0*e_vec buy_vec sell_vec 0];               %first num_var 0's corresponds to 0's of x's, last 0 is for t.
const2 = [trans_vec; -trans_vec];                       %constraint is an equality, so both positive and negative constraints are required
b_2 = [0 0]';

%% const 3: -buy_cost*e'*u^ - cost_sell*e'*v^ + t = 1. Defenition of t= 1-(cost of transaction)
x_zeros_vector = zeros(1, num_var);
uv_ones_vector = ones(1, num_var);
const3_initial = [x_zeros_vector -buy_cost*uv_ones_vector  -sell_cost*uv_ones_vector 1];
const3 = [const3_initial; -const3_initial];             %equality constraints
b_3 = [1; -1];
%% const 4 - new version: Ax^ - b*t <= 0, note that the constraint in the paper is Ax^ <= 0. 
% In our case (before adding TC constraints) there is also a constraint vector, b,  Ax <= b. 
% Since we want to solve for the new weight, which are x^=x*t, we multiply the previous equation by t and obtain Ax^ <= bt. 
% Since t itself is a variable, the bt term moves to the lef hand side of the equation, such that the new constraint vector is zero, 
% which the A matrix now contains also a term that corresponds to the variable t
zero_rect = zeros(size(A(3:end, :))); %note that first two rows of A (sum x_i = 1) are obsolete as they are accounted for by 2nd constraint, 
                                      %the sum is no longer 1 but must be re-adjsted using t
b_no_top_two = b(3:end);
const4 = [A(3:end, :) zero_rect zero_rect -b_no_top_two]; 
b_4 = zeros(size(b(3:end)));

%% const 5: x^, u^, v^, t >= 0, x^ > 0 already taken account of in A
x_zeros = zeros(num_var);
uv_ones = eye(num_var); %used to set the constraints for all u weights and all v weights
zero_t = zeros(1, num_var)';
pos_u = [x_zeros -uv_ones x_zeros zero_t];  %minus sign is present since the constraint are always smaller than or equal, 
                                            % and since we need greater than, we need to negate the sign
pos_v = [x_zeros x_zeros -uv_ones zero_t];
pos_t = [zeros(1, 3*num_var) -1];
const5 = [pos_u; pos_v; pos_t];
b_5 = zeros(1, 2*num_var +1)';

%% const 6: limit u^, v^ < =10%, i.e., since weight for each bond is limited to MaxBondWeight, then it isn't possible to sell or buy more than MaxBondWeight...
max_wt = MaxBondWeight;
t_wt_lim = max_wt*ones(num_var, 1);
u_wt_lim = [x_zeros uv_ones x_zeros -t_wt_lim]; %again, the conatraint value moved to the left hand side, since the constraint is now on u^ and v^,
                                                % which are just u and v multiplied with t. so by multiplyin the equation u<=max_wt  
                                                % by t we obtain u^<=max_wt*t, and then we move the right term to the left u^-max_wt*t<=0
v_wt_lim = [x_zeros x_zeros uv_ones -t_wt_lim];
const6  = [u_wt_lim; v_wt_lim];
b_6 = [zeros(2*num_var, 1)];


%% const 7: limit t< 2, doesn't appear in the paper, but is required by the solver, Otherwise t is unlimited and unreasonable results are obtained
zeros_for_all_but_t = zeros(1, 3*num_var);
const7 = [zeros_for_all_but_t 1];
b_7 = [2];                                      %t is limited at this point to 2 so that no more than half the value of the portfolio is used for expenses


%% modify A and b
A_final = [const1; const2; const3; const4; const5; const6; const7];
b_final = [b_1; b_2; b_3; b_4; b_5; b_6; b_7];
ConSet2 = [A_final b_final];
