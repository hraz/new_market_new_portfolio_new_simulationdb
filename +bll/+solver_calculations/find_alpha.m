function alpha=find_alpha(ReturnMat,beta,Rm,IndexYTM)
%find the alpha using two methods: 1: as done until now - calc the expected
%return of bond/index by an average of returns 
%2 - use the YTM of the bond/index as the expected returns. 

method=1;
if method==1
ExpReturnMat=mean(ReturnMat);  %expected returns that were measured in the market
alpha=ExpReturnMat'-beta*Rm.'; %difference between the measured returns and the returns expected by the single index mode
 %It used to be the above two lines, but i changed it to YTM after reading more abour the multi index model. 
 
elseif method==2
 %alpha = year2week(YTM) - beta*year2week(IndexYTM.');           %multi index alpha - reduce the beta's * indices returns from the YTM of the asset. This equation suits both single and multi index model
 alpha = year2week(YTM) - beta*Rm.';           %multi index alpha - reduce the beta's * indices returns from the YTM of the asset. This equation suits both single and multi index model
 %the diag makes sure that each beta is multiplied with its corresponding
 %index mean return. This is an Nx2 x 2x2 matrix multipication.
 
else     
 alpha = year2week(YTM) - beta*year2week(IndexYTM.');           %multi index alpha - reduce the beta's * indices returns from the YTM of the asset. This equation suits both single and multi index model
 %%%%% CHECK! will need to change from YTM to priceyield here as well...  
end
 
 
 



