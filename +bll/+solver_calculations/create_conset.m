function [ConSet error]=create_conset(filtered, solver_props, nsin_data)
% Create the constraint martix, ConSet, which is used in portopt
% It is structured as [A b], where A is the coefficients matrix
%and b is the bounds vector.
import bll.data_manipulations.*;
import bll.solver_calculations.*;

% dbstop if error;
N=filtered.NumBonds; %number of bonds which passed the filters
TypeGroups=CreatTypeGroups(solver_props.TypeFilterParams', nsin_data.Dataset, solver_props.UserBondTypes,solver_props.BondsPerType,N); %creat a matrix of groups. each row is a group, with 1's in columns corresponding to the bonds indices
SectorGroups = [];
if solver_props.sector_weight_flag 
[SectorGroups]=create_sectors_groups(nsin_data.Sector,N); %MaxSectorWeight,MinSectorWeight, are vectors in length equal to the number of different Sectors there is in the origianl file
end
SectorLocations = solver_props.indices_of_used_sectors;
if sum(solver_props.CorpGovTypes) %if the user specified a global group of corporate and/or government
    CorpGovGroups=create_corp_gov_groups(solver_props.CorpGovTypes,nsin_data.Dataset,N); %creat groups of only corp bonds and/or only gov bonds
else %if not, dont add anything
    CorpGovGroups=[];
    solver_props.MaxCorpGovWeight=[];
    solver_props.MinCorpGovWeight=[];
end
%construct the groups and group lims
%if  ~handles.SpecBonHistLenError %if the specific bonds contraints are not included (due to the fact that at least one bond dont have enough history length, and the flag is up), dont take into account the specific bonds parameters
    Groups=[TypeGroups ; SectorGroups; CorpGovGroups]; %put the groups of the types and sectors in one matrix
    MaxGroupWeights=[solver_props.MaxTypeWeight; solver_props.MaxSectorWeight];%;solver_props.MaxCorpGovWeight(solver_props.CorpGovTypes>0) ]; %put the max weights of all groups in one vector. BondsPerType0 is used since it could be that after the user addede specific bonds constraints, suddenly there is a bond in a type the user didn't wanted. however, we assume the user is aware of it, so we allow that specific bond to enter the nsin l list, and we updated the bonds per type, but there is no constraint on the type that was added, so we just need the original bonds per type before that specific bond was addede
    MinGroupWeights=[solver_props.MinTypeWeight; solver_props.MinSectorWeight];%; solver_props.MinCorpGovWeight(solver_props.CorpGovTypes>0)]; %put the min weights of all groups in one vector
% else
%     [SpecBondGroup]=CreatSpecBondGroup(handles.SpecBondLoc,N);%creat groups for each of the specific bonds that the user wants
%     Groups=[TypeGroups ; SectorGroups; CorpGovGroups; SpecBondGroup]; %put the groups of the types and sectors in one matrix
%     SpecificMaxWeight=handles.MaxWeight(handles.SpecBondCon==1); %these are the max weight that the user specified. 
%     SpecificMaxWeight(SpecificMaxWeight==0)=1e-4; %in the locations that the user put 0 as maximum weight (i.e., he doesn't want this bond to get weight, we change the 0 to 1e-4, which is very close to zero, but allows the algorithm to have some tolerance
%     SpecificMinWeight = handles.MinWeight(handles.SpecBondCon==1);
%     MaxGroupWeights=[handles.MaxTypeWeight(handles.BondsPerType0>0) ; handles.MaxSectorWeight(SectorLocations) ;handles.MaxCorpGovWeight(handles.CorpGovTypes>0); SpecificMaxWeight]; %put the max weights of all groups in one vector
%     MinGroupWeights=[handles.MinTypeWeight(handles.BondsPerType0>0) ; handles.MinSectorWeight(SectorLocations) ; handles.MinCorpGovWeight(handles.CorpGovTypes>0); SpecificMinWeight]; %put the min weights of all groups in one vector
% end
[A b]=CreatDurationConstraints(nsin_data.Duration,solver_props.MaxPortDuration,solver_props.MinPortDuration); %creat a custom made constraint of Years To Maturity
if solver_props.TargetBetaFlag %If there is a target beta constraint    
    [A b]=AddTarBetaCon(A,b,nsin_data.beta,solver_props.TargetBeta);
end
[ConSet error] = port_conset('Default', N, 'GroupLims',Groups,MinGroupWeights,MaxGroupWeights,'AssetLims',solver_props.MinBondWeight,solver_props.MaxBondWeight,N,'Custom',A,b); %Creat constraint matrix

%Note: we needed BondsPerType0 since BondsPerType might have an element
%which is larger than 0, only due to the user specific constraint. in this
%case, we dont have a constraint for this type, and groups will be shorter
%than the min and max weights. so, we use the origianl BondsPerType only in
%order to know which types passed the filters, only to them there is a
%group constraint