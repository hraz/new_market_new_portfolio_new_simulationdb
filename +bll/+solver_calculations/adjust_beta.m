function AdjBeta=adjust_beta(beta)
%This weighted average of the estiamted beta with a beta of 1 is done
%according to one of the text books.
AdjBeta=1/3+2/3*beta;
end