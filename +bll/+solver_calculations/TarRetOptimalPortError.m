function [PortDuration PortWts RiskyWts PortReturnYear PortRiskYear MarkerPosition TangPortRetYear TangPortRiskYear error warning]=TarRetOptimalPortError(YTM,ExpCovMat,NumPorts,ConSet,RisklessRateDay, BorrowRateDay, RiskAversion,TarRet,FminConFlag,DurationVector, handles)
% Solves the optimal portfolio optimization probelm,
%and plots the efficient frontier and the capital market line. It also enables the user
%to specify a target return, such that either a point on the CML is returned,
%or a point on the efficient frontier is returned, depending on the target return
%and on the maximum Shape ratio point return.
import gui.*;
TarRetDay=Year2Day(TarRet);                                 %translate to Daily terms, since the problems is solved in the Day domain, and plotted in the annual domain, for convinience (since the Covariance matrix is in Daily terms)
Daily_YTM=Year2Day(YTM);                                    %extract the Bruto yields of the bonds in the scope. these are the internal rate of return.
if ~isempty(handles.MyBonds)                            %if we are in where am i mode (%MyBonds are the bonds in where am i mode), init the algorithm with the users weights
    InitWeight(ind) = handles.MyWeights;                %init of weights according to users bonds
else                                                    %if not in where am i mode
    InitWeight = [];                                    %if not initial weights are assigned, inside portoptError the initial weigth will intialized to 1/N
end
AddTCConstraint = handles.AddTCconstraints;
if handles.AddTCconstraints
    [Daily_YTM ExpCovMat ConSet InitWeight] = UpdateConsetWithTCconstraints(handles.buy_cost, handles.sell_cost, InitWeight, Daily_YTM, ExpCovMat, ConSet);
end
if FminConFlag==0                                           %if we are in the regular optimizatio problem - no need to use the Fmincon optimization function
    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat, NumPorts, [], ConSet, InitWeight, AddTCConstraint); %Optimization
else                                                        %if advanced constraint are required, and so is the fmincon function
    [ConMinNumOfBonds ConMaxNumOfBonds ConMinWeight2AlloBonds ConMaxWeight2NonAlloBonds...
        MinNumOfBonds MaxNumOfBonds MinWeight2AllocBonds MaxWeight2NonAllocBonds]=GetAdvConParams(handles); %extract the advanced constraints params from handles
    [PortRisk, PortReturn, PortWts, error, warning] = portoptErrorFmincon(Daily_YTM,ExpCovMat,NumPorts, [], ConSet,...
        ConMinNumOfBonds, ConMaxNumOfBonds, ConMinWeight2AlloBonds, ConMaxWeight2NonAlloBonds,...
        MinNumOfBonds, MaxNumOfBonds, MinWeight2AllocBonds,MaxWeight2NonAllocBonds);                        %Optimization
end

if isempty(PortRisk)  || sum(isnan(PortRisk))>length(PortRisk)/2                                            %there there was no solution or more than half of points unfeasable, don't continue
    PortWts=[];
    RiskyWts=[];
    PortRiskYear=[];
    PortReturnYear=[];
    PortReturnOpt=[];
    PortRiskOpt = [];
    MarkerPosition = 1;
    TangPortRetYear =[];
    TangPortRiskYear=[];
    PortDuration =[];
    
    [PortRisk, PortReturn, PortWts, error, warning] = portoptError(Daily_YTM,ExpCovMat,2, [], ConSet, InitWeight, AddTCConstraint); %Optimization
    
    if isempty(PortRisk)
        return
    end
    PortReturnYear=Day2Year(PortReturn);                        %tranlate returns back to yearly returns, in order to plot in yearly terms
    PortRiskYear=PortRisk*sqrt(365);
    if PortReturn(2)<TarRetDay
        TarRetInd=2;
    else
        TarRetInd=1;
    end
    ClosestErrorFlag=0;
    %     error=102;
else
    TarRetInd=1;
    ClosestErrorFlag=0;
end

% take only legal points, that are not NaN
SolutionPoints = (~isnan(PortRisk));                        %indices of points which had a solution
PortReturn=PortReturn(SolutionPoints);                      %take only the returns of points with a solution
PortRisk=PortRisk(SolutionPoints);                          %take only the risk of points with a solution
PortWts=PortWts(SolutionPoints,:);       %take only the weights of points with a solution, and the ones that correspond only to the final weight of the optimization, and not to the weights of buying and selling

%if TC are active, save the buy weights and sell weight, and override
%PortWts such that it will hold only the final weights of the optimization
if handles.AddTCconstraints                                         %before overriding PortWts in 3 lines from this line, save the weights to buy, to sell and the t values that were obtained
    WeightsToBuy = PortWts(:,handles.NumBonds+1:2*handles.NumBonds);
    WeightsToSell = PortWts(:,2*handles.NumBonds+1:3*handles.NumBonds);
    t = PortWts(:,3*handles.NumBonds+1);                                %this factor scales the optimal weights, so in order to find the true optimal weight, this factor should be removed by dividing the optimal portfolio weight with it
end
PortWts = PortWts(:,1:handles.NumBonds);

% The next function can not be found in minimun variance optimization and
% target return, since they don't optimize the sharpe ratio
[RiskyRisk, RiskyReturn, RiskyWts, RiskyFraction,OverallRisk, OverallReturn, error] =...
    portallocError(PortRisk, PortReturn,PortWts, RisklessRateDay, BorrowRateDay, RiskAversion); %find the weights that maximize the Sharpe ratio

%% find the optimal solution in two scenraios: 1. the target return is lower than the optimal solution's return - return a combination of the risk free asset and the optimal solution. 2. the targer return is higher than the optimal - return a point on the efficient (no leveraging)

% RiskyWts=RiskyWts';


% RiskyRiskYear=RiskyRisk*sqrt(365);                        %the annual risk of the optimal risky portfolio
RiskyReturnYear=Day2Year(RiskyReturn);                      %the annual return of the optimal Risky portfolio
PortReturnYear=Day2Year(PortReturn);                        %tranlate back to yearly returns, in order to plot in yearly terms
PortRiskYear=PortRisk*sqrt(365);                            %translate to annual s.t.d., in order to plot the return vs. s.t.d. in yearly tersm
%hold on
%plot(PortRiskYear,PortReturnYear*100,'b','LineWidth',2);    %Plot the efficient first
%hold on
%xlim([PortRiskYear(1)*0.8 PortRiskYear(end)*1.2])
TangPortRisk=linspace(0,RiskyRisk,10).';                      %divide the tangent line into parts in the risk axis
slope=(RiskyReturn-RisklessRateDay)/RiskyRisk;              %calc slope of the tangent line
TangPortRet=slope*TangPortRisk+RisklessRateDay;             %calc return according to slope
TangPortRetYear=Day2Year(TangPortRet);                      %translate back into years
TangPortRiskYear=TangPortRisk*sqrt(365);                    %same for risk
%plot(TangPortRiskYear,TangPortRetYear*100,'g','LineWidth',2)%plot the tangent capital market line
if TarRet<RiskyReturnYear - 1000*eps                        %if the target return is lower than the opitmal, find the best time sharing between the optimal risky portfolio and the riskless asset, such that the return is the closest to the target
    [~,TarRetInd]=FindClosestTarRet(TangPortRet,TarRetDay,NumPorts);                          %find the index of the closest return to the target return, in the tangent portfolio returns
    if TarRetInd==1
        TarRetInd = 2;
    end
    RiskyPortWt=TangPortRisk(TarRetInd)/RiskyRisk;           %This is the portion of the tangent line which is associated with the optimal risky portfolio. when TarRet=RiskyReturnYear, TangentX(TarRetInd)= RiskyRiskYear, and RiskyPortWt=1.
    RiskyWts=RiskyPortWt*RiskyWts';                          %modify the wts of the optimal portfolio. since now only RiskyPortWt out of the total porfolio is allocated to the optimal risky portfolio (the rest is allocated to the riskless asset), the weights of the assets comprising the optimal risky portfolio should be modified by multipling with RiskyPortWt. %the rest is allocated to the riskless asset. Note that you can calc this number by RiskLessWt=1-sum(RiskyWts_
    TangRetYear=Day2Year(TangPortRet(TarRetInd));            %translate the closest point that was found (its index is TarRetInd) to year terms
    TangRiskYear=TangPortRisk(TarRetInd)*sqrt(365);          %translate teh closest point that was found from daily s.t.d. to annual s.t.d.
    plot([0 TangRiskYear],TangRetYear*100*ones(1,2),'r--','LineWidth',2')                      %plot the red dotted line of the best point
    plot(TangRiskYear,TangRetYear*100,'r*','markersize',16)
    %DurationVector=handles.Duration;                                     %extract the duration of all the bonds in the BondScope
    PortDuration=DurationVector.'*RiskyWts;
    tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',TangRetYear*100,TangRiskYear,PortDuration);
    AddFigNames(tit,'\sigma','Annual Return [%]',1)
else                                                                                    %need to borrow in order to use the riskless asset. if we don't want to borrow, we just find the closest portfolio on the efficient, like in a regular target return.
    [~,TarRetInd]=FindClosestTarRet(PortReturn,TarRetDay,NumPorts);                     %replace the target return in the return vector
    RiskyWts=PortWts(TarRetInd,:)';                                                             %These are the target weights
    PortReturnYear=Day2Year(PortReturn);                                                        %tranlate back to yearly returns, in order to plot in yearly terms
    PortRiskYear=PortRisk*sqrt(365);                                                            %same for s.t.d.
    plot([0 PortRiskYear(TarRetInd)],PortReturnYear(TarRetInd)*100*ones(1,2),'r--','LineWidth',2) %plot red dotted line
    plot(PortRiskYear(TarRetInd),PortReturnYear(TarRetInd)*100,'r*','markersize',16)            %plot optimal point
    PortReturnOpt =PortReturnYear(TarRetInd)*100;
    PortRiskOpt = PortRiskYear(TarRetInd);
    %DurationVector=handles.Duration;                                                            %extract the duration of all the bonds in the BondScope
    PortDuration=DurationVector.'*RiskyWts;
    tit=sprintf('Return: %1.2f%%, Risk: %1.3f, Duration: %1.2f years',PortReturnYear(TarRetInd)*100,PortRiskYear(TarRetInd),PortDuration);
    AddFigNames(tit,'\sigma','Annual Return [%]',1)
end
[~,MarkerPosition] = min(abs(TarRetDay - PortReturn));
PortReturnYear = PortReturnYear(MarkerPosition); PortRiskYear = PortRiskYear(MarkerPosition);