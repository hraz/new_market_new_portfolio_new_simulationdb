function nsin_data = EstCovMat_Marko(nsin_data)
% Estimate the covariance matrix according to Markowitz model, using a
% simple averaging of the correlation between the returns of the different
% assets, across HistLen observations (days). This is done in Matlab's buit
% in function, ewstats.
if ~sum(sum(isnan(nsin_data.ReturnMat)))                                    %if there are no nan's in the return matrix
[nsin_data.ExpReturn, nsin_data.MarkoExpCovMat] = ewstats(nsin_data.ReturnMat); %estimate the covarince matrix and the expected return.
else
    nsin_data.ExpReturn = 0;                  %since this metric is not really used (it is just another output of ewstate), we can set it to 0. (what we really use as the expected return is price yield or some kind of variant of it)
    nsin_data.MarkoExpCovMat = nancov(nsin_data.ReturnMat);
end
nsin_data.ExpCovMat=nsin_data.MarkoExpCovMat; %use the covariance matrix which was found using full markowitz model
% nsin_data.MarkoVar=diag(nsin_data.MarkoExpCovMat); %variances of the bonds, according to markowitz estimation