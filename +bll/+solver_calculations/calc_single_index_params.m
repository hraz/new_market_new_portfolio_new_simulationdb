function  [alpha,AdjBeta,Sigma_ei,SingIndExpCovMat]=calc_single_index_params(ReturnMat,IndexPriceYld,IndexYTM,Sigma_eiMethod,HistLen)
%Calculate the single index model params (alpha,beta), in order to estimate the
%covariance matrix using these params.
import bll.solver_calculations.*;

Rm=mean(IndexPriceYld);                            %market expected return
beta=FindBeta(ReturnMat,IndexPriceYld,Rm,HistLen);      %calc betas
AdjBeta=adjust_beta(beta);                          %adjust betas
alpha=find_alpha(ReturnMat,AdjBeta,Rm,IndexYTM);            %calc alphas
Sigma_ei=FindSigma_ei(ReturnMat,IndexPriceYld,alpha,AdjBeta,Rm,min(size(ReturnMat, 1), HistLen),Sigma_eiMethod);  %calc unsystematic risk, which can be diversified away. Try to estimate using variance_i-beta_i^2*variance_market
SingIndExpCovMat=beta_2_cov(AdjBeta,IndexPriceYld,Sigma_ei,Rm,min(size(ReturnMat, 1), HistLen));                     %convert the betas to covariances, using the assumption that cov(i,j)=beta_i*beta_j*VarMar
