function [solution nsin_data solver_props] = solver_ultimate_box(constraints, filtered, solver_props, DateNum, account_name)
% SOLVER_ULTIMATE_BOX solves for optimal portfolio
%
% [solution] = solver_ultimate_box(constraints, filtered, solver_props, DateNum) - black box version of solver,
%     receives constraints, filtered, solver_props and DateNumas guidelines for portfolio and universe to solve from
%     and solves for optimal portfolio.
%
%   Input:
%       Required:
%       constraints - data structure - required, contains constraints input by user - check bll/filtering_functions/filter_by_constraints
%
%       filtered - data structure - required, contains nsins list, and
%          their breakdown into cells for coset - see filter_by_constraints
%
%       solver_props - data structure - required, contains various fields
%          required for the solver_utlimate which determine the algorithm to
%          be used for solution and the final make-up of the portfolio.
%
%       DateNum - DATENUM - required, datenum of date for which the
%          portfolio is solved
%
%       account_name - char - name of the account.
%
%       Optional:
%       nsin_data - data structure - structure containing data pulled on
%          nsins filtered, including YTM, price_dirty, etc. and various
%          calculated measures - such as InfoRatio and Sharpe.
%
%       solution - data structure - structure containing solved portfolio,
%          portfolio weights breakdown, expected portfolio return, risk and
%          duration, allocation, fields for nsins removed and for those kept.
%
%   Output:
%       solution - data structure, see above
%
%       nsin_data - data structure, see above
%
%       solver_props - data structure ,see above
%
%   See also:
%       solver_utlimate, simulation_function
%
% Hillel Raz, June 6th, Sharon's due day
% Copyright 2013, Bond IT Ltd
import bll.solver_calculations.*;
import bll.data_manipulations.*;

[solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum);
%% Check error messages here
errorDS  = dataset({}, 'VarNames', 'err_msg');


% for solutionNumber = 1:numSolutions

if ~isempty(solver_props.ConSeterror) % Can only be in the first run
    errors1 = [errors1 ' ConSet, first  run'];
    error(errors1);
end

if ~isempty(solution.error)
    errors1 = [solution.error ' solver error, first run' ];
    error(errors1);
end

if isempty(solution.PortReturn)
    solution.PortDuration = NaN; solution.PortReturn = NaN; solution.PortRisk = NaN;
end

if ~isempty(solution.nsin_sol)&~isnan(solution.PortReturn)
    [solution solver_props nsin_data] = Lawnmower(solver_props, solution, nsin_data); % Removes bonds of low weight
end

if any(solution.selected_indx)
    nsin_data.redemption_type = [nsin_data.redemption_type(solution.selected_indx)];
end

if strcmp(solver_props.frontier_point, 'TarOS')&& ~isempty(solution.nsin_sol)
    solver_props.solved = 1; % Ensure that solver is ran again so that risk free bond is chosen if necessary
end

if size(solution.PortWts, 1) == 0
    solution.PortWts = sum(solution.PortWts);
end
if ~isnan(solution.PortReturn)
    if (1-sum(solution.PortWts) < solver_props.MaxWeight2NonAllocBonds)&&~strcmp(solver_props.frontier_point, 'TarOS') % If very little was removed - reconfigure weights, don't re-solve
        solution.PortWts = solution.PortWts/sum(solution.PortWts);
    elseif solver_props.solved && any(~isnan(solution.PortDuration))
        [solution nsin_data solver_props] = solver_ultimate(constraints, filtered, solver_props, DateNum, nsin_data, solution);
    end
end

if ~isempty(solution.error)
    errors1 = [solution.error ' solver error, post lawnmower run' ];
    error(errors1);
end

if isempty(solution.PortReturn)
    solution.PortDuration = NaN; solution.PortReturn = NaN; solution.PortRisk     = NaN; solution.PortRetExpectedWReinvestment = NaN;
    error('After lawnmower, no solution found');
end

%% Changed all types to multi for now
% solution.flagMultiRedemption = strcmp(nsin_data.redemption_type, 'multi');
solution.flagMultiRedemption = ones(length(nsin_data.redemption_type), 1)>0;
solution.port_beginning = DateNum;
% end

end