function solverOutputDs = save_output_results(solverOutputDs, sim_num_save, call_num,ErrorFlag, Output, Aineq, Bineq, initWeights, finalWeights, options)

if length(solverOutputDs.sim_num_save)==1 && call_num==1
    n=1;
else
    n=length(solverOutputDs.sim_num_save)+1;
end

%the only time we'de like the data set to create a new line eventhough a call_num that was already used is reused, is when inside the for loop Point = StartPoint:EndPoint which solves for different point on the efficient. In all of the rest of the calls to the differnt optimization functions, if an algorithm is recalled since it faild, we don't want to create a new line for it. 
if any(sim_num_save==solverOutputDs.sim_num_save) && any(call_num==solverOutputDs.call_num) %if this solver is called for the second time in this sum_num, override the previous results with these resutls rather than create a new line
 n = find(and(logical(sim_num_save==solverOutputDs.sim_num_save),logical(call_num==solverOutputDs.call_num)));
end

solverOutputDs.sim_num_save(n,1) = sim_num_save;
solverOutputDs.call_num(n,1) = call_num;
solverOutputDs.error_flag(n,1) = ErrorFlag;

if isfield(Output,'iterations')
    solverOutputDs.iterations(n,1) = Output.iterations;
end
if isfield(Output,'constrviolation') && ~isempty(Output.constrviolation)
    solverOutputDs.constr_violation(n,1) = Output.constrviolation;
end

if isfield(Output,'algorithm') && ~isempty(Output.algorithm)
    solverOutputDs.algorithm(n,1) = cellstr(Output.algorithm);
end

if isfield(Output,'cgiterations')  && ~isempty(Output.cgiterations)
    solverOutputDs.cg_iterations(n,1) = Output.cgiterations;
end

if isfield(Output,'message') && ~isempty(Output.message)
    solverOutputDs.msg(n,1) = cellstr(Output.message);
end

if isfield(Output,'firstorderopt') && ~isempty(Output.firstorderopt)
    solverOutputDs.first_order_opt(n,1) = Output.firstorderopt;
end

if isfield(Output,'firstexcessduration') && ~isempty(Output.firstExcessDuration)
    solverOutputDs.first_excess_duration(n,1) = Output.firstExcessDuration;
end

if isfield(Output,'secondexcessduration') && ~isempty(Output.secondExcessDuration)
    solverOutputDs.second_excess_duration(n,1) = Output.secondExcessDuration;
end

if isempty(options.TolCon)
    TolCon = 1e-6;
else
    TolCon = options.TolCon;
end

infeasible_inequality_const_indices_start = find(Aineq*initWeights-Bineq>TolCon);

if any(infeasible_inequality_const_indices_start<length(Aineq(:,1))-1)
    solverOutputDs.infeasible_non_duration_const_start(n) = 1;
end

if any(infeasible_inequality_const_indices_start>=length(Aineq(:,1))-1) %if at least of the invalid constraints indices is greater than the number of rows in Aineq, it means that the invalid constrait is a duration constratins
    solverOutputDs.infeasible_duration_const_start(n) = 1;
end

infeasible_inequality_const_indices_finish = find(Aineq*finalWeights-Bineq>TolCon);

if any(infeasible_inequality_const_indices_finish<length(Aineq(:,1))-1)
    solverOutputDs.infeasible_non_duration_const_finish(n) = 1;
end

if any(infeasible_inequality_const_indices_finish>=length(Aineq(:,1))-1) %if at least of the invalid constraints indices is greater than the number of rows in Aineq, it means that the invalid constrait is a duration constratins
    solverOutputDs.infeasible_duration_const_finish(n) = 1;
end
