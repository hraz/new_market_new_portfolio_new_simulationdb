function [mtxTransactions] = portfolio_reinvestment(cashIn, securityIdCashInBank,...
    mtxAllocation, oldCash, desiredDate, securityIdCashNotInBank)

%% NOT COMPLETE AT ALL. NEEDS FURTHER WORK!!! 


cashAvailable = cashIn;
x = find(mtxAllocation(:, 1)~=securityIdCashInBank);
mtxTransactions = zeros(length(x), 5);
priceOfOneUnitPortfolio = sum(mtxAllocation(x, 3));
mBonds = size(mtxAllocation, 1);
if (cashAvailable+oldCash)>priceOfOneUnitPortfolio
    nUnitsToBuy = floor(cashAvailable/priceOfOneUnitPortfolio);
    mtxTransactions(1:mBonds, 1) = datenum(desiredDate)*ones(mBonds, 1);
    mtxTransactions(1:mBonds, 2) = mtxAllocation(:,1);
    mtxTransactions(1:mBonds, 3) = mtxAllocation(:, 2);
    mtxTransactions(1:mBonds, 4) = 0; 
    mtxTransactions(1:mBonds, 5) = nUnitsToBuy*priceOfOneUnitPortfolio;
else
    mtxTransactions = zeros(1, 5);
    mtxTransactions(1:mBonds, 1) = datenum(desiredDate);
    mtxTransactions(1:mBonds, 2) = securityIdCashNotInBank;
    mtxTransactions(1:mBonds, 3) =1;
    mtxTransactions(1:mBonds, 4) = 0; 
    mtxTransactions(1:mBonds, 5) = cashAvailable;
end