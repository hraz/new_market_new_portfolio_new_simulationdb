function [mtxUpdatedInputAllocation]= current_price_update_in_allocation(mtxAllocation, ftsPrice, callDate)
% CURRENT_PRICE_UPDATE_IN_ALLOCATION updates prices in allocation matrix
%
%     [mtxUpdatedInputAllocation]= current_price_update_in_allocation(mtxHistoricalAllocationOut, ftsPrice, callDate)
%     Function receives an allocation matrix, a price fts and a wanted date, and according to the date, updates
%     the allocation matrix with the prices.
%
%       input:  mtxAllocation - 3 X NASSETS matrix, first column is
%                   nsins, 2nd column is count of each nsin, 3rd column is their price
%                   at current date
%
%               ftsPrice - fints structure of dirty price of nsins in mtxHistoricalAllocationOut
%
%               callDate - VALUE of date at which price is wanted
%
%       output: mtxUpdatedInputAllocation - 3 X NASSETS matrix, first column is
%                   nsins, 2nd column is count of each nsin, 3rd column is their price
%                   at current date
%
%   See als: calc_total_value
%
% Written by Hillel Raz, January 2013
% Copyright BondIT Ltd

securityIdCashInBank = 9;
securityIdCashNotInBank = 0;
beginSpot = 1;

mtxUpdatedInputAllocation(:, 1) = mtxAllocation(:, 1);

mtxUpdatedInputAllocation(:, 2) = mtxAllocation(:, 2);

%% Update prices according to callDate
dates = ftsPrice.price_dirty.dates;
dateIndex = ismember(dates, callDate);
mtxPrice = fts2mat(ftsPrice.price_dirty);
x = isnan(mtxPrice); %Remove NaN's
mtxPrice(x) = 0;

%In case there is cash in the account that's not a bond purchased, update
%as below
if mtxUpdatedInputAllocation(1, 1) == securityIdCashInBank || mtxUpdatedInputAllocation(1, 1) == securityIdCashNotInBank
    beginSpot = beginSpot + 1;
    mtxUpdatedInputAllocation(1, 2) = 1;
    mtxUpdatedInputAllocation(1, 3) = mtxAllocation(1, 3);
end
if size(mtxUpdatedInputAllocation, 1) > 1
    if  mtxUpdatedInputAllocation(2, 1) == securityIdCashInBank
        beginSpot = beginSpot + 1;
        mtxUpdatedInputAllocation(2, 3) = mtxAllocation(2, 3);
    end
end

if any(dateIndex)
    mtxUpdatedInputAllocation(beginSpot:end, 3) = mtxPrice(dateIndex, :);
else %If empty then we are not on a business day, take previous business day
    firstDateSmallerThanCallDate = find(dates  <= callDate, 1, 'last');
    mtxUpdatedInputAllocation(beginSpot:end, 3) = mtxPrice(firstDateSmallerThanCallDate, :);
end