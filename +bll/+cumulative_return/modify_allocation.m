function [mtxAllocation] = modify_allocation(mtxTransactions, mtxInputAllocation, portfolioDuration, lastCallDate, desiredDate, benchmarkID)

%MODIFY_ALLOCATION changes the allocation due to transactions in the
%portfolio or reinvestment of money.
%			
%    [mtxAllocation] = modify_allocation(mtxTransactions, mtxInputAllocation, portfolioDuration,...
%                                           lastCallDate, desiredDate, benchmarkID)
%   recieves the current allocation, a matrix of transactions and several additional
%   variables. It provides the new allocation according 
%	Input:
%       mtxTransactions �  matrix of dimensions (nTransactions)*(5). It
%                                                 tells how many units of a given security we buy or sell at a given
%                                                  date. The format is:
%                                                 date | security_id | unit | TC | amount |
%                                                  ---------|-----------------------|----------|-----|--------------|
%                                                             Comment: If units are sold then the units entered are negative.
%       mtxInputAllocation �  matrix of dimensions (nSecurities)*(3).
%                                                                            The format is:
%                                                                            security_id's | units | price |
%                                                                            -------------------------|------------|-----------|
%       portfolioDuration - the duration of the portflio. This needs to be
%                                                     changed into something like the time to maturity. It only has
%                                                     effect if we put money in the bank. For now, we either buy risk
%                                                     free assets or hold the money until we can buy them, so it makes no
%                                                     difference.
%       lastCallDate - a datenum variable of the last date the function was called upon.   
%       desiredDate - a datenum variable of the current date.  
%       benchmarkID - nsin of benchmark if needed. At this point it should
%                                      be empty.
%   Output:
%       mtxAllocation � matrix similar to mtxAllocation but
%                                           updated according to the transactions which took place (mtxTransactions).
%   Sample:
%			Some example of the function using.
% 	
%	See Also:
%       REINVESTMENT
%
%   Comments:
%       9 is the 'nsin' of cash in the bank.
%       0 is the 'nsin' of cash not in the bank.
%	
% Idan Oren, 30/1/13
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.

import bll.cumulative_return.*;
import dal.market.get.base_data.*; % Because of: da_nominal_yield_between_dates

securityIdCashInBank = 9;
securityIdCashNotInBank = 0;

%% Check input arguments type:
flagType(1) = isa(mtxTransactions, 'double');
flagType(2) = isa(mtxInputAllocation, 'double');
flagType(3) = isa(portfolioDuration, 'numeric');
flagType(4) = isa(lastCallDate, 'numeric');
flagType(5) = isa(desiredDate, 'numeric');
if ~isempty(benchmarkID)
    flagType(6) = isa(benchmarkID, 'double');
else
    flagType(6) = 1;
end
if sum(flagType) ~= length(flagType)
     error('modify_allocation:wrongInput', ...
                'One or more input arguments is not of the right type');
end
%% Check input arguments sizes:
if ~isempty(mtxTransactions)
    nColumns1 = size(mtxTransactions, 2);
else
    nColumns1 = 5;
end
nColumns2 = size(mtxInputAllocation, 2);
sizes = [nColumns1 nColumns2];
flagSizes = sizes == [5 3];
if sum(flagSizes) ~= length(flagSizes)
     error('modify_allocation:wrongInput', ...
                'One or more input arguments is not of the right size');
end


mtxAllocation = mtxInputAllocation;
nTransactions = size(mtxTransactions, 1);
for iTransaction = 1:nTransactions
    securityID = mtxTransactions(iTransaction, 2);
    indexOfSecurityID = find(mtxAllocation(:, 1)==securityID);
    if (securityID~=securityIdCashInBank) && (securityID~=securityIdCashNotInBank) %% Meaning the transaction is not putting money in the bank or just holding it, but rather buying or selling bonds.
        if ~isempty(indexOfSecurityID)
            mtxAllocation(indexOfSecurityID, 2) = mtxAllocation(indexOfSecurityID, 2)+mtxTransactions(iTransaction, 3); % Additional... 
            % purchase of bond in portfolio
        else % Purchase of new bond
            mRowsAllocation = size(mtxAllocation, 1)+1;
            mtxAllocation(mRowsAllocation, 1) =  securityID; %%% Check the index.
            mtxAllocation(mRowsAllocation, 2) =  mtxTransactions(iTransaction, 3);
            mtxAllocation(mRowsAllocation, 3) =  mtxTransactions(iTransaction, 5)./mtxTransactions(iTransaction, 3);
        end
    elseif securityID==securityIdCashInBank %% Meaning the transaction is putting money in the bank. FIXME!!!
        if ~isempty(indexOfSecurityID) % We add the cash to cash already held in the bank which accumulated interest.
            % interestPeriod = portfolioDuration;
            interestPeriod = datenum(lastCallDate):datenum(desiredDate);
            riskFreeInterestsVec  = fts2mat(da_nominal_yield_between_dates(interestPeriod, lastCallDate, desiredDate));
            interests = 1+riskFreeInterestsVec;
            mtxAllocation(indexOfSecurityID, 2) = mtxAllocation(indexOfSecurityID, 2)*prod(interests);
            mtxAllocation(indexOfSecurityID, 2) = mtxAllocation(indexOfSecurityID, 2)+mtxTransactions(iTransaction, 3);
        else % There is no money currently in the bank.
            mRowsAllocation = size(mtxAllocation, 1);
            mtxAllocation(mRowsAllocation+1, 1) = securityIdCashInBank;
            mtxAllocation(mRowsAllocation+1, 2) = 1;
            mtxAllocation(mRowsAllocation+1, 3) = mtxTransactions(iTransaction, 5);
        end
    else  %% Meaning the transaction is just holding the money.
        if ~isempty(indexOfSecurityID) % We add the cash to cash already held.
            mtxAllocation(indexOfSecurityID, 3) = mtxAllocation(indexOfSecurityID, 3)+mtxTransactions(iTransaction, 5);
        else % We currently hold no cash.
            mRowsAllocation = size(mtxAllocation, 1);
            mtxAllocation(mRowsAllocation+1, 1) = securityIdCashNotInBank;
            mtxAllocation(mRowsAllocation+1, 2) = 1;
            mtxAllocation(mRowsAllocation+1, 3) = mtxTransactions(iTransaction, 5);
        end
    end
end

if ~issorted(mtxAllocation(:, 1)) % Sorting mtxAllocation in acending order of security Id's.
    [~, I] = sort(mtxAllocation(:, 1));
    mtxAllocation = mtxAllocation(I, :);
end

