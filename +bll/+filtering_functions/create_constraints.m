function [constraints] = create_constraints()
%CREATE_CONSTRAINTS collection of default constraints
%
%   [constraints] = create_constraints()
%   function returns a data structure, constraints, with default
%   constraints for filtering
%
%   input: none
%
%   output: data structure - containing default values for filtering
%      purposes
%
% 
% Hillel Raz, 12/02/2013
% Copyright 2013, Bond IT Ltd.
% Updated by Updater Name, date, short description of the update.


constraints.dynamic.yield.min = 0;
constraints.dynamic.turnover.max = 1e16;

constraints.dynamic.hist_len  = 60; % history length required for bonds

constraints.sectors.minWeight = 0;
constraints.static.minWeight = 0;
constraints.sectors.maxWeight = 1;
constraints.static.maxWeight = 1;

end