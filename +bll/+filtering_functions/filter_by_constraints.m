function [filtered error_id benchmarkWts] = filter_by_constraints(constraints,reqDate,relatedBenchmark)
% filter_by_constraints returns list of securities filtered according to preferences
% 
%     [filtered] = filter_by_constraints(constraints,reqDate)
%     this function return the filter result (nsin list) of an 'advanced-constraint'
%     filter (relevant for both the pilot product and development simulation) 
%     
%     [filtered benchmarkWts] = filter_by_constraints(constraints,reqDate,relatedBenchmark)
%     this function return the filter result (nsin list) of an 'advanced-constraint'
%     filter (relevant for both the pilot product and development simulation) after
%     intersection with a specified benchmark
% 
%     Input:
%         required:
%             constraints - structure holding the detailed requested constraints.
%                 necessary and optional fields are detailed below.
%             reqDate - DATENUM requested date of filtering
%             
%             required fields of constraints are:
%             constraints.dynamic.min_ttm, constraints.dynamic.max_ttm - desired TTM-range of the securities
%                 if constraints.dynamic.min_ttm, constraints.dynamic.max_ttm are empty - 
%                 one should fill constraints.dynamic.ttm_filter_flag according to 
%                 constraints.dynamic.ttm_filter_flag- 
%                     - 1 for all durations
%                     - 2 for durations >= portfolio_duraion - ceil(0.25*portfolio_duration)
%                     - 3 for durations <= portfolio_duraion + ceil(0.25*portfolio_duration)
%                     - 4 for duration in portfolio_duration +- ceil(0.25*portfolio_duration)
%             constraints.dynamic.port_dur - portfolio target duration, 
%                 used for setting bonds TTM-range if not specified
%             constraints.dynamic.hist_len - DOUBLE minimum length of history.
%             constraints.model_id : 1X3 DOUBLE - according to Hillel's spec
%         
%         optional:
%             relatedBenchmark - reference benchmark with which components the results 
%                 are intersected                
%             
%             optional fields of constraints are:
%                 constraints.dynamic.yield    - 1 X 2 DOUBLE with 1st entry=min_yield and 2nd entry=max_yeild
%                 constraints.dynamic.turnover - 1 X 2 DOUBLE with 1st entry=min_turnover
%                                                                                              and 2nd entry=max_turnover (in millions of units)
% 
%                 constraints.rating.min_maalot (_id) - STRING (DOUBLE) setting the minimum of maaolt rating
%                 constraints.rating.max_maalot (_id)- STRING (DOUBLE) setting the maximum of maaolt rating
%                 constraints.rating.min_midroog (_id)- STRING (DOUBLE) setting the minimum of midroog rating
%                 constraints.rating.max_midroog (_id)- STRING (DOUBLE) setting the maximum of midroog rating
%                 constraints.rating.logical_operator - 'And' \ 'Or' (default - 'Or')
% 
%                 constraints.static.dataset - dataset of NType rows with the 
%                     correct format for 'bond_ttl_multi.m'
%                 constraints.static.minWeight - NType X 1 vector of the minimum constraint on types
%                 constraints.static.maxWeight - NType X 1 vector of the minimum constraint on types
%                 constraints.excludeSecurities.nsin - NAssets of 'bad nsins'
% 
%     Output:
%         filtered.nsin_list - NASSETS X 1 DOUBLE list of nsins
%         filtered.nsin_cell_lists - NType X 1 cell array of the bonds belonging to
%             each filter, meant for the building of the conset
%         filtered.min_histlen - DOUBLE the minimal constraint on history length that 
%             satisfies the solver model demands
%         
%         Optional:
%         error_id - [] for no error, 116 for empty index
%         benchmarkWts - NASSETS X 1 DOUBLE the relative weights of the benchmark 
%             securities' appearing in the filtered results
% 
% Written by Amit Godel, 05-Feb-2013
% Copyright 2013, BondIT Ltd.
import dal.market.filter.total.*;
import dal.market.filter.risk.*;
import dal.market.get.dynamic.*;
import dal.market.get.market_indx.*; % market_indx_components
import utility.pilot_utility.*;
import bll.*;
import bll.filtering_functions.*;

error_id=[];
%%
bad_nsin_list1=[1094655
     1100791
     7460207
     1010016
     1086842
     1089952
     1091925
     1091958
     1092121
     1092139
     1092220
     1093715
     1095348
     1095793
     1095827
     1095918
     1095967
     1100791
     1101039
     1101054
     1101211
     1101674
     1101682
     1101690
     1101963
     1101971
     1102060
     1102805
     1105238
     1109503
     1110089
     1110097
     1110642
     1110980
     1113026
     1113034
     1113398
     1117902
     1119734
     1122118
     1122282
     1127588
     1240068
     1410224
     1940246
     1940261
     2070019
     2070035
     2070050
     2229979
     2410082
     2459998
     2489995
     3130259
     3160025
     3440021
     3860053
     3960044
     3970019
     3970027
     4730024
     5490107
     5550090
     5740022
     6110118
     6110126
     6110431
     6130140
     6370126
     6999973
     7020225
     7020233
     7190077
     7259997
     7411176
     7480122
     7509953
     7590052
     7710098
     7710155
     8129991
     8189979
];
bad_nsin_list=[bad_nsin_list1];
if isfield(constraints,'excludeSecurities') && ...
        isfield(constraints.excludeSecurities,'nsin')
    bad_nsin_list=[bad_nsin_list;constraints.excludeSecurities.nsin];
end
%

% setting the dynamic structure for filtering
hist_len=constraints.dynamic.hist_len;
dyn_param.history_length = {hist_len 100000};
portfolio_duration=constraints.dynamic.port_dur;
if isfield(constraints.dynamic,'min_ttm') && ~isempty(constraints.dynamic.min_ttm)
    horizon_min=constraints.dynamic.min_ttm;
else
    switch constraints.dynamic.ttm_filter_flag
        case 1
            horizon_min=0;
        case 2
            horizon_min=portfolio_duration-(0.2*portfolio_duration);            
        case 3
            horizon_min=0;            
        case 4
            horizon_min=portfolio_duration-(0.2*portfolio_duration);            
    end
end
if isfield(constraints.dynamic,'max_ttm') && ~isempty(constraints.dynamic.max_ttm)    
    horizon_max=constraints.dynamic.max_ttm;
else
    switch constraints.dynamic.ttm_filter_flag
        case 1
            horizon_max=9999;
        case 2
            horizon_max=9999;
        case 3
            horizon_max=portfolio_duration+(0.2*portfolio_duration);
        case 4
            horizon_max=portfolio_duration+(0.2*portfolio_duration);
    end
end
dyn_param.ttm = {horizon_min horizon_max};
dyn_param.yield={constraints.dynamic.yield.min constraints.dynamic.yield.max};
dyn_param.turnover={constraints.dynamic.turnover.min constraints.dynamic.turnover.max};

% Setting the rating constraints for filtering, if exists
if ~isempty(constraints.rating.min_maalot)&&~isempty(constraints.rating.max_maalot)...
        &&~isempty(constraints.rating.min_midroog)&&~isempty(constraints.rating.max_midroog)
    mlt_lb=constraints.rating.min_maalot;
    mlt_ub=constraints.rating.max_maalot;
    mdg_lb=constraints.rating.min_midroog;
    mdg_ub=constraints.rating.max_midroog;
    log_opr=constraints.rating.logical_operator;
    rating_FLAG=1;
else
    rating_FLAG=0;
end
% AFTER YIGAL WILL CHANGE BOND_RANK TO GET ID'S, SHOULD CHANGE TO SOMETHING LIKE THIS:
% if ~isempty(constraints.rating.min_maalot_id)&&~isempty(constraints.rating.max_maalot_id)...
%         &&~isempty(constraints.rating.min_midroog_id)&&~isempty(constraints.rating.max_midroog_id)
%     mlt_lb=constraints.rating.min_maalot_id;
%     mlt_ub=constraints.rating.max_maalot_id;
%     mdg_lb=constraints.rating.min_midroog_id;
%     mdg_ub=constraints.rating.max_midroog_id;
%     log_opr=constraints.rating.logical_operator;
% end

% Setting the static dataset for filtering
staticDataset=constraints.static.dataset;

% Getting the benchmark components, for filter-by-benchmark
if nargin==3
    [ cmp ] = get_market_indx_components( relatedBenchmark, reqDate );
    if ~isempty(cmp)
        benchmarkNsin=cmp.id;
        benchmarkWts=cmp.weight;
    else
        benchmarkNsin=[];
        benchmarkWts=[];
        error_id=116;
    end
end

%filtering
if rating_FLAG
    %in case we have rating constraint, it's best to intersect the result
    %of each filter with the result of the rating, so we still have the
    %bonds devided to filter results
    nsin_cell_lists=cell(size(staticDataset,2),1);
    nsin_list=[];
    nsin_list_multi = bond_ttl_multi( reqDate,  staticDataset, dyn_param );
    nsin_list_rank = bond_rank_complex(mlt_lb, mlt_ub, mdg_lb, mdg_ub, log_opr, reqDate );
    for m=1:size(staticDataset,2)        
        if filter_is_corporate_check(staticDataset(m).bond_type,staticDataset(m).class_name)            
            nsin_cell_lists{m}=intersect(eval(sprintf('nsin_list_multi.filter_%i',m)),nsin_list_rank);
        else
            nsin_cell_lists{m}=eval(sprintf('nsin_list_multi.filter_%i',m));            
        end
        if nargin==3
            nsin_cell_lists{m}=intersect(nsin_cell_lists{m},benchmarkNsin);
        end
        nsin_list=union(nsin_list,nsin_cell_lists{m});
    end
else
    % if there's no rating constraints - no need to intersect.
    nsin_cell_lists=cell(size(staticDataset,2),1);
    nsin_list=[];
    nsin_list_multi = bond_ttl_multi( reqDate,  staticDataset, dyn_param );
    for m=1:size(staticDataset,2)
        nsin_cell_lists{m}=eval(sprintf('nsin_list_multi.filter_%i',m));
        if nargin==3
            nsin_cell_lists{m}=intersect(nsin_cell_lists{m},benchmarkNsin);
        end
        nsin_list=union(nsin_list,nsin_cell_lists{m});
    end
end
nsin_list=setdiff(nsin_list,bad_nsin_list);

% reducing bond's number in case of Markowitz
min_histlen=hist_len;
if (constraints.model_id(1)==1) && (length(nsin_list)>hist_len)
    temp_ds = get_bond_history_length(  nsin_list,  reqDate );
    histlen_of_nsins=temp_ds.history_length;    
    min_histlen=find((arrayfun(@(x) sum(histlen_of_nsins>=x)-x,1:800))<=(-1),1,'first');
    selected_indx=find(histlen_of_nsins>=min_histlen);
    nsin_list=nsin_list(selected_indx);    
end

% Setting output variables:
% Finding the surviving benchmark securities and picking their weights
if nargout>2
    if nargin==3        
        benchmarkNsinIndx=zeros(size(nsin_list));
        for iNsin=1:length(nsin_list)
            benchmarkNsinIndx(iNsin)=find(benchmarkNsin==nsin_list(iNsin));
        end
        benchmarkWts=benchmarkWts(benchmarkNsinIndx);
        benchmarkWts=benchmarkWts/sum(benchmarkWts);
    else
        error('no benchmark was entered but output requested benchmark components weight');
    end
end
filtered.nsin_list=nsin_list;
filtered.nsin_cell_lists=nsin_cell_lists;
filtered.min_histlen=min_histlen;
filtered.dyn_param=dyn_param;
