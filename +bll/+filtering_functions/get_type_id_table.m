function outres = get_type_id_table
% provides the table matching DB-type-id to types constructed in the
% 'build_type_struct.m' function.
%
% output: nTypes X 1 vector of type_id's as appearing in the market DB. the
%                    i'th element is the type_id of the i'th entry in the
%                    full structure built in 'build_type_struct.m'

outres = [1;3;8;5;2;4;9];