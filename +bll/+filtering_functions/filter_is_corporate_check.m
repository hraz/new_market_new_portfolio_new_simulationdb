function check_result = filter_is_corporate_check(bond_type,class_name)
% check whether a filter, or more precisely its bond_type or class_name 
% fields, is of corporate kind (then its results should be intersected with
% the ranking filter).
%
% NOTE: this function should be in sync with 'bll\build_type_struct.m'
%
check_result = strcmp(bond_type,'''Corporate Bond''')||strcmp(class_name,'''NON-GOVERNMENT BOND''');