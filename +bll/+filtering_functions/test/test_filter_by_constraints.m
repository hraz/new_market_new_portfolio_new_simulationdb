reqDate=735298;
constraints.dynamic.ttm_filter_flag=1;
constraints.dynamic.port_dur=3;
constraints.dynamic.hist_len=30;
constraints.model_id=2;
constraints.dynamic.yield.min=-0.2;
constraints.dynamic.yield.max=0.6;
constraints.dynamic.turnover.min=0;
constraints.dynamic.turnover.max=1e12;
constraints.rating.min_maalot_id=[];
constraints.rating.max_maalot_id=[];
constraints.rating.min_midroog_id=[];
constraints.rating.max_midroog_id=[];
constraints.rating.min_maalot=[];
constraints.rating.max_maalot=[];
constraints.rating.min_midroog=[];
constraints.rating.max_midroog=[];
constraints.sectors=[];
import bll.filtering_functions.*; % this line is if you work in the new files organization
% import bll.*; % this line is if you work in the old files organization
constraints.static.dataset=build_staticDataset(constraints);
fltr200=filter_by_constraints(constraints,reqDate)