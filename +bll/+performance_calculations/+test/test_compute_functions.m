import gui.*
import utility.statistics.*
clear
%% creat raw data to be used in the statistic functions
%% random data
marketReturn = (1+randn(1000,1))/10000;
portfolioReturns = 1 +(1+randn(1001,1))/10000;                          % creat a daily return vector around 1 (which represetns 0% change)
portfolioValue = cumprod(portfolioReturns);                                % the product of the returns is the daily value

% correlated data
marketReturn = (1+randn(1000,1))/10000;
alpha = 1e-4;
beta=1;
portfolioValue = [1; cumprod(1+alpha + beta*marketReturn)];

% non correlated data
marketReturn = (1+randn(1000,1))/10000;
alpha = 1e-4*randn(1000,1);
beta=1;
portfolioValue = [1; cumprod(1+alpha + beta*marketReturn)];

%% other setting
assetReturns = (1+randn(1000,10))/10000; %10 assets
mar = 0.05;
risklessRate=0.03;
%% statistics

portfolioSimpleReturn = compute_simple_return(portfolioValue);
[portfolioReturn portfolioStd portfolioSkewness portfolioKurtosis] = compute_moments(portfolioValue);
[sharpeRatio informationRatio] = compute_ratios(portfolioValue, marketReturn, risklessRate);
beta = compute_beta(portfolioValue,marketReturn);
alpha = compute_alpha(portfolioValue, marketReturn, beta);
omega = compute_omega(portfolioValue, mar);
downsideVolatilty = compute_downside_volatility(portfolioValue);
downsideVolatilty_mar = compute_downside_volatility(portfolioValue,mar);
jensenAlpha = compute_jensen_alpha(portfolioValue, marketReturn, risklessRate);
[rsquare rmse] = compute_rsquare(portfolioValue,marketReturn)


%% Var
confidnece=0.95;
expectedCovarianceMatrix = rand(10,10);
weights= rand(10,1);
weights = weights/sum(weights);
[valueAtRisk conditionalValueAtRisk marginalValueAtRisk componentValueAtRisk]=compute_var(portfolioValue, expectedCovarianceMatrix, weights, confidnece);
disp('done')

% %% Rsqare tests
% x1 = 0:0.1:10;
% x2 = 10.1:0.1:20;
% %  y1 = 2.*x1 + 1 + randn(size(x1));
% %  y2 = -x2 +31 + randn(size(x2));
% f1 = x1.^4 -9*x1.^3 + 1*x1.^2 -50*x1 + 10;
% y1 = x1.^4 -9*x1.^3 + 1*x1.^2 -50*x1 + 10 + 50*randn(size(x1));
% close; plot(x1,f1); hold on ; plot(x1,y1,'r.')
% 
% y=[y1 y2];
% p1 = polyfit(x1,y1,1);
% p2 = polyfit(x2,y2,1);
% f1 = polyval(p1,x1);
% f2 = polyval(p2,x2);
% f=[f1 f2];
% [r2 rmse] = rsquare(y1,f1);
% figure; plot(x,y,'b-');
% hold on; plot(x,f,'r-');
% title(strcat(['R2 = ' num2str(r2) '; RMSE = ' num2str(rmse)]))
% 
% [fitobject,gof] = fit(y1',f1','poly2')
% 
% %% Simulate values of market and portfolio to check for linear relation between them
% clear
% close all
% clc
% N=1e4;
% portfolioReturn = (1+10*randn(N,1))/10000;                          % creat a daily return vector around 1 (which represetns 0% change)
% portfolioValue = cumprod(1+portfolioReturn);                                % the product of the returns is the daily value
% marketReturn = (1+10*randn(N,1))/10000;
% marketValue = cumprod(1+marketReturn);
% figure;
% plot(marketValue)
% hold on
% plot(portfolioValue,'r')
% ylabel('value')
% xlabel('time')
% legend('market','portfolio')
% figure;
% plot(marketValue,portfolioValue)
% ylabel('marketValue') ; xlabel('portfolioValue')
