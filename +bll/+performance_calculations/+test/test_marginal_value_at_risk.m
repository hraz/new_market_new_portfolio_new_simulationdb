clear; clc;
dbstop if error;

load 'D:\GitRepo\simulation_code\bll\performance_calculations\test\ftsCumulativeValues.mat'

import bll.performance_calculations.*;

percentileRisk = 0.1;

histLength = 35; %default is 30

tic
[marginalValueAtRisk componentValueAtRisk] = marginal_value_at_risk(percentileRisk, ftsHoldingValuePerBond, histLength) 
toc

% Expected Answers:
%
% marginalValueAtRisk =
% 
%    -0.7068
%    -0.9043
%    -0.7777
%    -0.7068
%    -0.6821
%    -0.7561
%    -0.7777
%    -0.7777
%    -0.8024
%    -0.7345
%    -0.8673
%    -0.7098
%    -0.6296
%    -0.8209
%    -0.7777
%    -0.7777
%    -0.8487
%    -0.7777
%    -0.7777
% 
% 
% componentValueAtRisk =
% 
%   1.0e+005 *
% 
%     0.8273
%     1.0585
%     0.9104
%     0.8273
%     0.7984
%     0.8851
%     0.9104
%     0.9104
%     0.9393
%     0.8598
%     1.0152
%     0.8309
%     0.7370
%     0.9609
%     0.9104
%     0.9104
%     0.9935
%     0.9104
%     0.9104