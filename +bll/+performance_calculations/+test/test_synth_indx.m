clear; clc;

%% Import external packages:
import bll.simulation.*

%% find value of bonds at time t_f starting from t_0
nsin =[   6390215
6390157
1115385
7980162
];

bnd_amts = [100; 200; 300; 50];

tot_bnds = sum(bnd_amts);
wts = 1/tot_bnds * bnd_amts;

BondList = [nsin wts];

t_0= '2008-10-23';
t_f = '2010-10-23';

[ synth_indx_change ] = synth_indx( BondList, t_0, t_f )