
import bll.performance_calculations.*;

 PortWts = [0.0118    0.0123    0.0079    0.0145    0.0156    0.0262    0.0205    0.2000    0.1223    0.0337    0.0064    0.0028 0.2000    0.0094    0.0520    0.0079    0.0050    0.0353    0.0063    0.2000    0.0099];


Duration = [
    3.0575
    3.9397
    4.9753
    6.4904
    6.0027
    4.3644
    4.3644
    0.0192
    0.5288
    3.8192
    3.9781
    1.7753
    0.4959
    8.3123
    0.5562
    8.3123
    4.5836
    5.3014
    1.1397
    1.1425
    3.9233];

PortDuration =    1.5000;

YTM =[
    0.0481
    0.0473
    0.0280
    0.1231
    0.0795
    0.1323
    0.1448
    0.0061
    0.0117
    0.0399
    0.0816
    0.0275
    0.0122
    0.0602
    0.0947
    0.0428
    0.0754
    0.0916
    0.0020
    0.0144
    0.0428];


ReinvestmentStrategy = 'risk-free';


 riskfreeRates= [   0.0182    0.0258    0.0314    0.0359    0.0395    0.0428    0.0459];


riskfreettm= [    1.6329    2.5534    3.5562    4.5562    5.3918    6.3945    7.4740];

 [expReturn] = calc_expected_return_w_reinvestment_strategy(PortWts, Duration, PortDuration, YTM, ReinvestmentStrategy, riskfreeRates, riskfreettm)

