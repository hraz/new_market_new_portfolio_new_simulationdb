function downsideVolatilty = compute_downside_volatility(portfolioValue, varargin)


%   COMPUTE_DOWNSIDE_VOLATILITY computes the downside volatility of the portfolio.
%
%   downsideVolatilty = compute_downside_volatility(portfolioValue, mar)
%   receives the portfolio values over time, and an optional minimum
%   acceptable return, and returns the downside volatility of the
%   portfolio.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days.
%       mar � (optional) minimum acceptable return, i.e., the minimal return which is
%       acceptable by the investor. Any return which lower than mar is
%       considered a failaur, while any return above mar is considered a
%       success. The investor's mar should be in annual terms, which is
%       then converted to daily terms in order to be used with the daily
%       portfolioValue vector.
%   Output:
%       downsideVolatilty � downside volatility of the portfolio, i.e., the
%       volatility of the returns which fall below the mean return of below
%       the mar, if given. Let x be a vector of returns of length N, then:
%       downsideVolatilty = sqrt( sum( ( min(x-mar,0) ) .^ 2 ) / N-1 );
%   Sample:
%		Some example of the function using.
%
%		See Also:
%		compure_alpha, compute_beta, compute_omega, compute_moments,
%		compute_ratios, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

%% input check
s1=size(portfolioValue);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');
elseif sum(s1>1)==2 || sum(s1>1) == 0
    error('compute_omega:wrongInputSize','portfolioValue is expected to be a vector.')
end
if nargin==2
    mar=varargin{1};    
    s2=size(mar);
    if ~isa(mar,'numeric')
        error('compute_omega:wrongInputType','mar is expected to be a numeric argument');
    elseif sum(s2)~=2
        error('compute_omega:wrongInputSize','mar is expected to be a scaler')
    elseif mar<0
        error('compute_omega:wrongInputSize','mar is expected to be a positive scaler')
    elseif mar>1
        error('compute_omega:wrongInputSize','mar is expected to be a fractional scaler, e.g., for mar=6 percent, enter 0.06 instead of 6');
    end
end
import utility.data_conversions.*;
%% compute downside volatility
portfolioReturnSeries = portfolioValue(2:end)./portfolioValue(1:end-1) - 1;    % Daily return series of the portfolio in percentages.
if nargin==1                                                                   % If mar was not entered as an input, use the mean expected return of the portfolio as mar, in daily terms
    marDaily = mean(portfolioReturnSeries);
else                                                                           % If mar was entered as an argument, use it to compute the daily mar
    marDaily = Year2Day(mar);                                                  % Daily minimum acceptable return
end
saturatedDifferenceFromMar = min(portfolioReturnSeries-marDaily ,0);           % substruct the daily mar from the daily returns. Zero positive excess returns (beyond the daily mar), such that only negative returns are left
downsideVolatilty = sqrt( sum(saturatedDifferenceFromMar.^2) / length(saturatedDifferenceFromMar)-1 );  %compute downside risk according to definition

