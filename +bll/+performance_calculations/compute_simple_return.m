function portfolioSimpleReturn = compute_simple_return(portfolioValue)

%   COMPUTE_SIMPLE_RETURN computes the the simple return that the portfolio
%   has yielded by dividing the final value with the intial one, and
%   annualizing the result according to the investement horizon.
%			
%   portfolioSimpleReturn = compute_simple_return(portfolioValue)
%   receives the portfolio values over time and returns the simple return of the portfolio.
%	Input:
%       portfolioValue � a vector of portfolio values over time, i.e., the
%       total value of a certain portfolio (including received copuns) in
%       different days. 
%   Output:
%       portfolioSimpleReturn � a simple annual return of the portfolio
%   Sample:
%		Some example of the function using.
% 	
%		See Also:
%		compure_alpha, compute_beta, compute_omega, compute_moments,
%		compute_ratios, compute_downside_volatility, compute_jensen_alpha, compute_rsquare
%
% Yoav Eisenberg, 22/2/2013
% Copyright 2013, Bond IT Ltd.

import utility.data_conversions.*;


%% input check
s1=size(portfolioValue);
if ~isa(portfolioValue,'numeric')
    error('compute_omega:wrongInputType','portfolioValue is expected to be a numeric argument');    
elseif sum(s1>1)==2 || sum(s1>1) == 0                                              
end
%% compute return
portfolioSimpleReturn = (portfolioValue(end)/portfolioValue(1))-1;                 % simple portfolio return. 


