function [valueAtRisk conditionalValueAtRisk ] = value_at_risk(percentileRisk, ftsCumulativeValues, histLength)
%VALUE_AT_RISK function computes value at risk of a portfolio and
%conditional value at risk.
%
% function computes the value at risk of a portfolio based on its value
% changes over specified period as well as its conditional value at risk
%
% input:
%        percentileRisk - double - scalar between 1-10% saying the percentile wanted
%
%        ftsCumulativeValues - NDays X 1 vector of values of portfolio
%
%        histLength - double - scalar telling the number of days back for the function to check, default is 30
%
%
%   output: valueAtRisk - double - amount of money which is at risk for the percentile given
%
%           conditionalValueAtRisk - double - average amount of money at
%           the percentile given, calculated using left Riemann sum (and not
%           trapezoidal sum) for simplicity

%
%   See also:
%   calc_total_value_envelope
%
% written by Hillel Raz, January, 2013
% updated by Amit Godel, June 2013, to handle both positive and negative
% values of VaR
% Copyright 2013, BondIT Ltd.

if ~isa(percentileRisk, 'double')||~isa(ftsCumulativeValues, 'fints');
     error('value_at_risk:wrongInput', ...
                'One or more input arguments is not of the right type');
end

defaultLength = 30; %number of days on which to base VaR computation
if nargin == 2
    histLength = defaultLength;
end

%% Setting up percent returns
cumulativeValues = fts2mat(ftsCumulativeValues);
cumulativeValues = cumulativeValues(end-histLength+1:end); %take only last 'hist_length' number of days

cumulativeReturnsWithoutLastEntry = cumulativeValues;
cumulativeReturnsWithoutLastEntry(end) =[]; %remove last entry

percentReturns = diff(cumulativeValues)./cumulativeReturnsWithoutLastEntry;
%will be used to get percentage change from previous return - hence don't
%need the last return
sortedReturns = sort(percentReturns);

%% Finding percentile wanted in terms of number of returns
percentileLocation = percentileRisk*length(sortedReturns); %Where does the percentile wanted fall in terms of the number of returns

percentileInt = floor(percentileLocation); %exact location in vector of returns - integer
percentileRemainder = percentileLocation-percentileInt; %non-integer part

if percentileInt ~= 0
    extraAmountDueToRemainder = sortedReturns(percentileInt+1)-sortedReturns(percentileInt);
    initialVar = sortedReturns(percentileInt);
else %In case that there are not enough history days for a small percentile (i.e. 1% corresponds to 100 days of history)
    initialVar = (sortedReturns(2) - sortedReturns(1))*percentileRemainder + sortedReturns(1);
    extraAmountDueToRemainder = 0;
end

%% Calculating VaR and cVaR
percentValueChangeAtRisk = initialVar + extraAmountDueToRemainder*(percentileRemainder);

valueAtRisk = -cumulativeValues(end)*percentValueChangeAtRisk; %Value of portfolio now (end) * percent possible loss
% negative sign is for convention (positive VaR means there's a chance
% of loss, negative VaR means the percentile is at positive return.

if percentileLocation<1 %if not, percentile sought is smaller than minimum bin size, take part of worst return
    conditionalValueAtRisk = -percentileLocation*sortedReturns(1)*cumulativeValues(end);
else
    sumRercentValueChangeTillPercentile = sum(sortedReturns(1:percentileInt));
    conditionalValueAtRisk = -cumulativeValues(end)*(sumRercentValueChangeTillPercentile +...
        extraAmountDueToRemainder*percentileRemainder )/percentileLocation;
    %averaged amount of money upto the given percentile, with linear
    %interpolation upto the exact location
end
