function [ cov covar covar2] = hist_risk_calc( t_0, t_f, BondList  )
%calculates historical risk (covar) for a given bond list
% 
% input: 
% t_0 - starting time
% 
% t_f - end time
% 
% BondList - 2 x n list of bonds (first column) and their weights in the portfolio (second column)
% 
% 
% output:
% cov_mat_final - matrix of covariances of all bonds in BondList
% 
% covar - risk of BondList
% 
global NBUSINESSDAYS;

import utility.statistics.*;
import dal.market.get.static.*;
import bll.simulation.*
import dml.*;

t_0 = datenum(t_0); 
t_f = datenum(t_f);

nsin_in =  BondList( :, 1);
[nsin Ind_n] = sort(nsin_in);
weights_in = BondList(:, 2);
weights = weights_in(Ind_n)/sum(weights_in(Ind_n));


%% Step 1 - Organize bonds according to date of maturity

% data_name = 'maturity_date';
% mat_dates = get_bond_stat(nsin, t_0, data_name);
% 
% mat_dates = datenum(mat_dates.maturity_date);
% 
% [mat_dates indx] = sort(mat_dates); %sort dates according to increasing maturity
% 
% nsin = nsin(indx); %sort nsins according to maturity dates

%% Step 2 - Get bond returns

[returns mat_price_yld prices]= BondRetHist( nsin, t_0, t_f );

num_bonds = length(returns);

%% Step 3 - calculate expected returns, volatility and covariance matrix

% exp_ret = zeros(1, num_bonds); %will hold expected returns for all bonds
% volatility = zeros(1, num_bonds); %will hold volatility for all bonds
% cov_cell = cell(1, num_bonds); %will hold covariance matrix for all bonds

%% Recalculating weights based on new prices

for num=1:num_bonds
    if isnan(prices(num))
        prices(num)= 0;
    end
end

tot_val = weights'*prices'; %total value of portfolio

weights_new = zeros(1, num_bonds); %store new weights based on prices on t_f

for num = 1:num_bonds
    
    weights_new(num) = weights(num)*prices(num)/tot_val;
    
end



%% for calculating risk based on returns
% bond_ret = [];
% for num = 1:num_bonds
% 
% bond_ret = [bond_ret returns{num, 2}(:, 1) ] ;%create matrix containing all returns
% 
% end
% 
% 
% 
% exp_ret = nanmean(bond_ret);
% volatility = nanstd(bond_ret);
% cov_mat = nancov(bond_ret);


%% for calculating risk based on yield

pri_yield_ret = mat_price_yld(:, 2:end);

[ exp_ret, volatility, cov ] = nanstat(pri_yield_ret);


% cov_diag = sqrt(diag(1./diag(cov))); %diagonal matrix made of squareroots of variance terms, to normalize covariance matrix
% 
% cov_mat_final = cov_diag * cov * cov_diag;

covar = sqrt(NBUSINESSDAYS)* sqrt(weights'*cov*weights);

bond_ret = [];
for num = 1:num_bonds

bond_ret = [bond_ret returns{num, 2}(:, 2) ] ;%create matrix containing all returns

end

[ exp_ret2, volatility2, cov2 ] = nanstat(bond_ret); %risk calcs according to returns calculated
covar2 = sqrt(NBUSINESSDAYS)* sqrt(weights'*cov2*weights);

end






