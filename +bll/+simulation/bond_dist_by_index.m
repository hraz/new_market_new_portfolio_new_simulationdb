function [ indx_uniq indx_wts ] = bond_dist_by_index( bnd_list_in, bond_holdings_in, t_0 )
%This function takes a given portfolio and the weights of the bonds in it
%and returns the association of portfolio according to the indices.  if
%bonds are not in an index, then function doesn't put them into calculation
%and instead removes them from weight consideration.  
% 
% input:
% 
% bond_list_in = list of bond nsins in portfolio
% 
% bond_holdings_in = list of bond weights in the portfolio, total of weights should add up to 1.
% 
% t_0 = date for which list of bond associated indices is wanted
%     
% 
% output:
% 
% indx_list = list of indices to which bonds are associated
% 
% indx_wts = list of weighs for each index, sum should equal 1
% 
% 
% Hillel Raz, 2012

import dal.market.filter.market_indx.*

%% Sort data according to bond list

[bnd_list Indx_bnd] = sort(bnd_list_in);

bond_holdings = bond_holdings_in(Indx_bnd);

num_bnds = length(bnd_list);

%% Recover index information

 [ indx_data ] = benchmark( bnd_list, t_0 );%get each associated index for each bond
   bond_not_indx = []; %for bonds that are not in index
   
if length(indx_data) ~= num_bnds
        
    [bond_not_indx bnd_not_indx] = setdiff(bnd_list, indx_data.security_id);  %get bonds that are not in any index
    
    [bond_in_indx Indx_in Indx2] = intersect(bnd_list, indx_data.security_id);%bonds that are in index
    
%     data_name = 'price_dirty';
%     
%     [fts_prices ] = get_bond_dyn_single_date( bond_in_indx, data_name, t_0 );
%    
%     bond_prices = fts2mat(fts_prices);
 
%   new_wts = bond_holdings(Indx_in)'*bond_prices/sum(bond_prices); %calculate weights of bonds in port that are in an index
   
new_wts = bond_holdings(Indx_in)/sum(bond_holdings(Indx_in));

    bnd_list = bond_in_indx; %create new list with new weights
    bond_holdings = new_wts;

end

num_bnds_new = length(bnd_list);

 indx_mat = [indx_data.index_id, bond_holdings];

 [indx_types, I_indx] = sort(indx_mat(:, 1)); %sort index according to different indices
 
 indx_uniq = unique(indx_types); %get number of different indices
 
 num_ind = length(indx_uniq); %number of unique indices
 
indx_wts = zeros(num_ind, 1); %to store weights of each index based on bonds they contain from their portfolio

location = 1; %hold location in matrix

%% Calculate associated index weights for portfolio

for num_i = 1:1:num_ind
         
    while  location < (num_bnds_new+1) && indx_uniq(num_i) == indx_types(location, 1)
    
        indx_wts(num_i) = indx_wts(num_i) + indx_mat(I_indx(location), 2); %add bond weight in corresponding index to get index weight
        location = location + 1;
    
    end
    
end

if sum(bond_not_indx )
        warning('The following bond is not in an index: %i \n', bond_not_indx);
end

end

