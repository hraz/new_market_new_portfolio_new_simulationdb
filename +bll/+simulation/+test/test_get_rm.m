clear; clc;

%% Import external packages:
import dal.market.filter.total.*;
import dal.market.get.base_data.*;
import dal.market.get.dynamic.*;
import bll.simulation.*;

%%  Define time period:
start_date = '2005-01-02';
end_date = '2006-01-02';

%% Some filterring of existed assets at the start date:
stat_param.coupon_type = {'Fixed Interest'};
stat_param.linkage_index = {'CPI'};
stat_param.bond_type = {'Corporate Bond'};
dyn_param.mod_dur = {2 7};

[ nsin ] = bond_ttl( start_date,stat_param, dyn_param );

%% Build the portfolio allocation data:
holdings = 1000*ones(length(nsin), 1);
for i = 1:1:length(nsin)
    holdings(i) = i*holdings(i);
end
price = da_bond_dyn(nsin, 'price_dirty', start_date);
weight = (holdings2weights(holdings', fts2mat(price)))';

port_alloc = dataset(nsin, holdings, weight);

%% Calculation the risk measures:
mar = 0.1; % minimal accepted return on the portfolio

tic
rm = get_rm(port_alloc, end_date, mar, start_date )
toc