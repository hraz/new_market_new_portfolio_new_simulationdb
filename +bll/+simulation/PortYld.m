function [ port_yld bnd_wts ] = PortYld(bnd_list, bnd_amts, t_f);
%PortYld gives the change in the yield of the portfolio from the previous
%business day as well as the weights of the bonds in the portfolio
%according to their price and the portfolio's value on t_f

% input:
% 
% bnd_list - an n x 1 list of bond nsins in the portfolio
% 
% bnd_amts - an n x 1 list of amounts of each bond in portfolio 
% 
% t_f -  date for which the calculation is to be made
% 
% 
% output: 
% 
% port_val - scalar representing value of portfolio at time t_f
% 
% bnd_wts - an n x 1 list of the weights of the bonds in the portfolio
% according to their value on t_f

import dal.market.get.dynamic.*

t_f = datenum(t_f);

data_name = 'price_dirty';
[port_val]= get_bond_dyn_single_date(bnd_list, data_name, t_f); %get price yields for all bonds in portfolio

val_mat = fts2mat(port_val);%convert data to matrix

port_val = bnd_amts * val_mat'; % calculate total value of portfolio

num_bonds= length(bnd_list); %get number of bonds

bnd_wts = zeros(1,num_bonds); %to store weights of bonds according to value in portfolio 

for num=1:1:num_bonds

    bnd_wts(num) = bnd_amts(num)*val_mat(num)/port_val;
    
end

%% Get price_yield

data_name = 'price_yld';
[bnd_yld]= get_bond_dyn_single_date(bnd_list, data_name, t_f); %get price yields for all bonds in portfolio

yld_mat = fts2mat(bnd_yld);%convert data to matrix

port_yld = bnd_wts * yld_mat'; % calculate yield change of portfolio

end






