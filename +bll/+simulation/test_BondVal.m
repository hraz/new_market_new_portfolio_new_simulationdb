clear; clc;
%% Import external packages:
import bll.simulation.*

%% find value of bonds at time t_f starting from t_0
nsin =[ 1104405;
     1105329; 1110931];

t_0 = '2008-01-02';

t_f =  '2012-01-31';

money = BondVal(nsin, t_0, t_f);