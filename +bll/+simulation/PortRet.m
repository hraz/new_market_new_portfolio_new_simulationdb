function [ PortVal PercRet ytm_port ret] = PortRet( BondList, t_0, t_f )

%calculates portfolio returns given a BondList, a starting time for
%portfolio and a final time for portfolio.
% 
% 
% Input:  BondList - n x 2 matrix made of list of nsins of bonds in portfolio and weights for each bond
% 
%                 t_0 - starting time for portfolio
%                 
%                 t_f - finishing time for portfolio
%                 
%                 
% Output:  PortVal - total value of portfolio returned minus initial price for each bond
% 
%                    PercRet - percent return on initial investment
%
%                   ytm_port - ytm of portfolio

import dal.market.get.dynamic.*
import bll.simulation.*

bond_nsin_in = BondList( :, 1);
bond_holdings_in = BondList(:, 2);

[bond_nsin Ind_n] = sort(bond_nsin_in);
bond_holdings = bond_holdings_in(Ind_n);


%% Calculate initial investment

%num_bonds =length(bond_nsin);
    
%      data_name = 'price_dirty';
%      tic
%  [ fts_price ] = get_bond_dyn_single_date( bond_nsin, data_name , t_0 );
%  toc
% initial_price =  fts2mat(fts_price);


%% Calculate current value of bonds


[initial_price ret ]= BondVal(bond_nsin, t_0, t_f);

bond_val = ret(:, 1); 
CurVal = bond_holdings'*bond_val;       

Price = (bond_holdings'*initial_price');

%% calculate final value of portfolio


PortVal = CurVal - Price;
PercRet = PortVal/Price;

time_inv = ( datenum(t_f)-datenum(t_0) )/365;

ytm_port= ((CurVal/Price)^(1/time_inv) - 1)*100;
    
end






