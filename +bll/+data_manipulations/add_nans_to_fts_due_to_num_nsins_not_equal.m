function [ftsLessNsinsNowWithMore] = add_nans_to_fts_due_to_num_nsins_not_equal(ftsMoreNsins, ftsLessNsins)
%ADD_NANS_TO_FTS_DUE_TO_NUM_NSINS_NOT_EQUAL adds columns of nans to fts for nsins in
%one fts and not in the other.
%
%   [ftsLessNsinsNowWithMore] = add_nans_to_fts_due_to_num_nsins_not_equal(ftsMoreNsins, ftsLessNsins)
%   function adds a column of nans to the fts with missing nsins, for each nsins missing
%
%   Input:  ftsMoreNsins - mDates X nNsins fts
%
%           ftsLessNsins - mDates X pNsins fts
%
%
%   Output: ftsLessNsinsNowWithMore - mDates X nNsins fts
%
%   See also:   get_cash_flow
%
% Written by Hillel Raz, May 2013
% Copyright, BondIT Ltd

import utility.*;

fieldsMore = fieldnames(ftsMoreNsins);
listNsinsMore = strid2numid(fieldsMore(4:end));
fieldsLess = fieldnames(ftsLessNsins);
listNsinsLess = strid2numid(fieldsLess(4:end));

diffNsins = setdiff(listNsinsMore, listNsinsLess);

newFieldName = numid2strid(diffNsins);
        ftsLessNsinsNowWithMore = fints(ftsLessNsins.dates, NaN(length(ftsLessNsins), numel(newFieldName)), newFieldName);
        ftsLessNsinsNowWithMore = merge(ftsLessNsins, ftsLessNsinsNowWithMore);
