function Group=CreatTypeGroups(TypeFilterParams, Dataset, UserBondTypes,BondsPerType,N)
% Creat the Group matrix which is needed for matlab to build
%ConSet. Each row is a different group, where 1 indicates a bonds that is a
%member of the same group
aux=1:length(UserBondTypes);  %its length is as the number of types of bonds
LogUserBondTypes=logical(UserBondTypes); %make the user required bond types be logical, such that the next line will be possible
NonZeroBondsPerType=logical(BondsPerType>0)'; %find locations of indices such that BondsPerType is not zero
if isequal(size(LogUserBondTypes),size(NonZeroBondsPerType))
    BondsToInclude=and(LogUserBondTypes,NonZeroBondsPerType); %perform a logical and operation to find the locations of bonds that both the user wants and that the number of bonds of that type that passed the filter is not zero
else
    BondsToInclude=and(LogUserBondTypes,NonZeroBondsPerType'); %perform a logical and operation to find the locations of bonds that both the user wants and that the number of bonds of that type that passed the filter is not zero
end
UserBondLocations=aux(BondsToInclude);  %find the location of the user required types


Group=zeros(length(UserBondLocations),N); %preallocate
for i=1:length(UserBondLocations) %for each desired group creat a line with ones and zeros in the correct places
    for k=1:length(TypeFilterParams{UserBondLocations(i)}.bond_type) %there could be more than one names for a typs, e.g., Gilon and new Gilon...
        for m=1:length(TypeFilterParams{UserBondLocations(i)}.linkage_index) %there could be more than one linkage_index_index for a type, e.g., 'USD','UKP','EURO',...
            for n=1:length(TypeFilterParams{UserBondLocations(i)}.coupon_type) %for bonds that are linked to a coin that is not NIS, they are taken as fixed and variable interest
                Group(i,and(and(logical(strcmp(Dataset.bond_type,TypeFilterParams{UserBondLocations(i)}.bond_type(k))),...
                    logical(strcmp(Dataset.linkage_index,TypeFilterParams{UserBondLocations(i)}.linkage_index(m)))),...
                    logical(strcmp(Dataset.coupon_type,TypeFilterParams{UserBondLocations(i)}.coupon_type(n)))))  = 1;
            end
        end
    end
end
