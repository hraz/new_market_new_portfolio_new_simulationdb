function [ftsLessDatesNowWithMore] = add_nans_to_fts_due_to_num_dates_not_equal_for_excel(ftsMoreDates, ftsLessDates)
%ADD_NANS_TO_FTS_DUE_TO_NUM_DATES_NOT_EQUAL adds nans to fts for dates in
%one fts and not in the other.
%
%   [ftsLessDatesNowWithMore] = add_nans_to_fts_due_to_num_dates_not_equal(ftsMoreDates, ftsLessDates)
%   function adds dates to the fts with missing dates with data entries of
%   nan.
%
%   Input:  ftsMoreDates - mDates X nNsins fts
%
%           ftsLessDates - pDates X nNsins fts
%
%
%   Output: ftsLessDatesNowWithMore - mDates X nNsins fts
%
%   See also:   get_cash_flow
%
% Written by Hillel Raz, May 2013
% Copyright, BondIT Ltd


import utility.*;

datesMore = ftsMoreDates.dates;
datesLess = ftsLessDates.dates;

[diffDatesFirst, indexDates] = setdiff(datesLess, datesMore);
l= length(diffDatesFirst);
moveDates =[];
for j= 1:l
moveDate = find(diffDatesFirst(j)<= datesMore, 1, 'first');
moveDates=[moveDates moveDate];
end
datesLess(indexDates) = datesMore(moveDates);
 ftsLessDates.dates = datesLess;
diffDates = setdiff(datesMore, datesLess);


namesFields = fieldnames(ftsLessDates);
nsins = namesFields(4:end);

ftsLessDatesNowWithMore = fints(diffDates, NaN(length(diffDates), length(nsins)), nsins);
ftsLessDatesNowWithMore = merge(ftsLessDates, ftsLessDatesNowWithMore);
