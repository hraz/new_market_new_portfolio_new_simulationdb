function [solver_props] = clear_relevant_solver_props_fields(solver_props, wgt_per_bond_choice)
% CLEAR_RELEVANT_SOLVER_PROPS_FIELDS resets properties for solver
% 
%     [solver_props] = clear_revelant_solver_props_fields(solver_props, wgt_per_bond_choice) - function 
%         resets relevant properties that were altered during solution process.
%         
%     Input: solver_props - DATA STRUCTURE - contains fields relevant for solver. In particular fields
%                             are reset are: solved
%                                            max_weight_changed_flag
%                                            MaxBondWeight
%                                            dates_kept_vector
%                                            MaxPortDuration
%                                            MinPortDuration
%                                            ConSeterror
%                                            Duration
%                                            
%            wgt_per_bond_choice - DOUBLE - max weight allowed per bond
%            
%     Output: solver_props - with relevant fields reset or zeroed.
%     
%     See also: simulation_function, create_solver_props
%     
% Written by Hillel Raz, May 2013
% Copyright, BondIT Ltd
                                          

solver_props.solved = 0; % After solution

solver_props.max_weight_changed_flag = 0; % From Lawnmower
solver_props.MaxBondWeight = wgt_per_bond_choice; 

solver_props.dates_kept_vector = []; %From solver
solver_props.MaxPortDuration = [];
solver_props.MinPortDuration = [];
solver_props.Duration = [];
solver_props.ConSeterror = []; 