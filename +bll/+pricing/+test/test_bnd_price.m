clear; clc;

%% Import external packages:
import bll.pricing.*;
import dal.market.get.cash_flow.*;
import dal.market.get.dynamic.*;
import utility.cash_flow.*;
import utility.fts.*;
import utility.*;
import bll.term_structure.bootstrap_mdl.*;

%% Build start parameters:
settle = datenum('2005-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.
param.compounding = 1;
param.basis = 10;
time_flag = 0; % 0 or 1

%% Buil list of priced assets:
% Cash flow build:
[ cf ] = cfbuilder(settle, time_flag, true);

% Collect nsin list:
nsin = strid2numid(tsnames(cf));

%% Collect market dirty price of all assets from the list:
[ market_price ] = get_bond_dyn_single_date(nsin, 'price_dirty', settle);

%%  Build some interest rate curve:
[ ircurve ] = bootstrap( 'zero', cf, param );

%% Calculate expected dirty prices:
[ exp_price ] = bnd_price(cf, ircurve, param);

%% Error estimating:
err = fts2mat(market_price - exp_price)
err_mean = mean(err)
err_std = std(err)




