clear; clc;
import bll.term_structure.term_structures_simulation.*;
start_settle = '2005-01-02'; % The minimum possible date is 02-01-2005 according to given data from database.
end_settle = '2011-12-31';
settle = [datenum(start_settle):1:datenum(end_settle) ]';

stat_param.class_name = {'GOVERNMENT BOND'};
stat_param.coupon_type = {'Fixed Interest'};
stat_param.linkage = {'NON LINKED'};
stat_param.sector = {};
stat_param.type = {};

dyn_param.history_length = {NaN NaN};
dyn_param.ytm = {NaN NaN};
dyn_param.mod_dur = {NaN NaN};
dyn_param.turnover = {NaN NaN};
dyn_param.ttm = {6 7};

examed_data = cell(length(settle),2);
tic
for i = 1 : length(settle)
    [ nsin ] = examed_asset(settle(i), stat_param, dyn_param);
    if length(nsin) > 2
        examed_data{i,1} = datestr(settle(i));
        examed_data{i,2} = nsin;
    end
end
toc
% find(cellfun(@(x)~isempty(x),examed_data(:,1)))