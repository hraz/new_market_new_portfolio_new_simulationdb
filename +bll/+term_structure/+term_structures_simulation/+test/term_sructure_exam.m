clear; clc;

%% Import external packages:
import bll.term_structure.term_structures_simulation.sim_funcs.* 
import bll.term_structure.*;
import dml.*;
import dal.market.filter.static.* 
import dal.market.filter.dynamic.*;
import dal.market.get.base_data.*;
import dal.market.get.static.* 
import dal.market.get.dynamic.* 
import dal.market.get.cash_flow.*;
import utility.* 
import utility.cash_flow.* ;

%% Basic data for the test:
start_date = '2009-01-02'; % The minimum possible date is 02-01-2005 according to given data from database.
sim_step = 1; % business days

model = {'bootstrap', 'nelson_siegel', 'svensson'};
mdl_num = length(model);
dyn_param.ttm = {2 100};
wnd_sz = 247; %?!

%% Build bond groups:
gov.class_name = {'GOVERNMENT BOND'};
gov.coupon_type = {'Fixed Interest'};
gov.linkage_index = {'NON LINKED'};

corp.class_name = {'NON-GOVERNMENT BOND'};
corp.coupon_type = {'Fixed Interest'};
corp.linkage_index = {'NON LINKED'};
corp.bond_type = {'Corporate Bond'};

%% Build list of examined assets:
[ gov_nsin ] = examined_asset(start_date, gov, dyn_param);
[ gov_data ] = get_bond_stat( gov_nsin, start_date, {'last_ex_date', 'maturity_date', 'last_trading_date'} );

[ corp_nsin ] = examined_asset(start_date, corp, dyn_param);
[ corp_data ] = get_bond_stat( corp_nsin, start_date, {'last_ex_date', 'maturity_date', 'last_trading_date'} );

% Filtering assets with premature termination of trading:
min_last_trading_date = min(min([datenum(gov_data.last_trading_date);datenum(corp_data.last_trading_date)] ));
min_ex_date = min(min([datenum(gov_data.last_ex_date);datenum(corp_data.last_ex_date)] ));
min_obs_date = datestr(min([min_last_trading_date; min_ex_date]), 'yyyy-mm-dd');
max_maturity = datestr(max(max([datenum(gov_data.maturity_date); datenum(corp_data.maturity_date)])), 'yyyy-mm-dd');

[ lived_nsin ] = get_lived_assets(min_obs_date);
gov_ind = ismember(gov_nsin, lived_nsin);
corp_ind = ismember(corp_nsin, lived_nsin);

gov_nsin = gov_nsin(gov_ind);
gov_data = gov_data(gov_ind,:);

corp_nsin = corp_nsin(corp_ind);
corp_data = corp_data(corp_ind,:);

gov_nassets = numel(gov_nsin);
corp_nassets = numel(corp_nsin);

% Get required historic data for simulations:
[ gov_market ] = get_multi_dyn_between_dates( gov_nsin, {'price_dirty'}, 'bond', start_date, min_obs_date );
[ corp_market ] = get_multi_dyn_between_dates( corp_nsin, {'price_dirty'}, 'bond', start_date, min_obs_date );

[ gov_cf ] = da_bond_amrt( gov_nsin, start_date );
[ corp_cf ] = da_bond_amrt( corp_nsin, start_date );

[ gov_ex_days ] = get_bond_cf_dates( start_date, max_maturity, gov_nsin );
[ corp_ex_days ] = get_bond_cf_dates( start_date, max_maturity, corp_nsin );

%% Determine simulation window and rolling period:
obs =  intersect(gov_market.price_dirty.dates, corp_market.price_dirty.dates);
num_obs = numel(obs);
nsim = floor((num_obs - wnd_sz + 1) / sim_step)-2;

%% Get historic data according to the observation list:
ind_gov = ismember(gov_market.price_dirty.dates, obs);
ind_corp = ismember(corp_market.price_dirty.dates, obs);

gov_market_price = fts2mat(gov_market.price_dirty(ind_gov),1);
corp_market_price = fts2mat(corp_market.price_dirty(ind_corp),1);

% Here we have all market information about examined government and non-government assets. 

%% Memory allocation for simulation results:
examined_time_period = [1 2 3 6 12]' ./ 12;
examined_nobs = floor(examined_time_period .* wnd_sz);
num_examined_obs = length(examined_time_period);

for i = 1:mdl_num
    gov_fitted_price.(model{i}) = zeros(num_examined_obs, gov_nassets, nsim);
    corp_fitted_price.(model{i}) = zeros(num_examined_obs, corp_nassets, nsim);

    gov_fit_err.(model{i}) = zeros(num_examined_obs, gov_nassets, nsim);
    corp_fit_err.(model{i}) = zeros(num_examined_obs, corp_nassets, nsim);
    
    gov_mean_err.(model{i}) = zeros(num_examined_obs, gov_nassets);
    corp_mean_err.(model{i}) = zeros(num_examined_obs, corp_nassets);
    
    gov_std_err.(model{i}) = zeros(num_examined_obs, gov_nassets);
    corp_std_err.(model{i}) = zeros(num_examined_obs, corp_nassets);
end

%% Simulations:
param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';   % For bootstrap model.
ir_type = 'forward';
coupon_type_flag = -1; % means use tbills and shahar together

exitflag = NaN(nsim, mdl_num);
%   Possible values of EXITFLAG and the corresponding exit conditions are listed
%   below. See the documentation for a complete description.
%
%     1  LSQNONLIN converged to a solution.
%     2  Change in X too small.
%     3  Change in RESNORM too small.
%     4  Computed search direction too small.
%     0  Too many function evaluations or iterations.
%    -1  Stopped by output/plot function.
%    -2  Bounds are inconsistent.
%    -3  Regularization parameter too large (Levenberg-Marquardt).
%    -4  Line search failed.

start_sim_time = tic;
examined_obs_ind = zeros(num_examined_obs,nsim);

observed_gov_market_price = zeros(num_examined_obs, gov_nassets, nsim);
observed_corp_market_price = zeros(num_examined_obs, corp_nassets, nsim); 

model_curves = cell(nsim,1);
examined_obs = cell(nsim,1);
for k = 5: nsim
    % Term Structure building:
    tic
    k
    if k == 1
        settle_ind = k;
    else
        settle_ind = k * sim_step + 1;
    end

    [mdl, exitflag(k, :)] = examined_model(model, ir_type, obs(settle_ind), coupon_type_flag, param);
    model_curves{k} = mdl;
    
    % Bond pricing on base given Term Structures:
    examined_obs_ind(:,k) = settle_ind + examined_nobs;
    examined_obs{k} = obs(examined_obs_ind(:,k));
    gov_mdl_price = uniquesim( examined_obs{k}, mdl, gov_cf, gov_ex_days, param );
    corp_mdl_price = uniquesim( examined_obs{k}, mdl, corp_cf, corp_ex_days, param );
    
    observed_gov_market_price(:,:,k) = gov_market_price(examined_obs_ind(:,k), 2:end);
    observed_corp_market_price(:,:,k) = corp_market_price(examined_obs_ind(:,k), 2:end);

    for m = 1: mdl_num
        gov_fitted_price.(model{m})(:,:,k) = gov_mdl_price.(model{m});
        corp_fitted_price.(model{m})(:,:,k) = corp_mdl_price.(model{m});
        
        % Error calculation:
        gov_fit_err.(model{m})(:,:,k) = (log(gov_fitted_price.(model{m})(:,:,k)) - log(observed_gov_market_price(:,:,k)));
        corp_fit_err.(model{m})(:,:,k) = (log(corp_fitted_price.(model{m})(:,:,k)) - log(observed_corp_market_price(:,:,k)));
    end
    toc
end
total_sim_time = toc(start_sim_time);

%% Error normalization:
% time_norm = sqrt(3./ (examined_time_period .^ 3));
for i = 1:mdl_num
    gov_mean_err.(model{i}) = nanmean(gov_fit_err.(model{i}),3);
    corp_mean_err.(model{i}) = nanmean(corp_fit_err.(model{i}),3);
    
    gov_std_err.(model{i}) = nanstd(gov_fit_err.(model{i}),0,3);
    corp_std_err.(model{i}) = nanstd(corp_fit_err.(model{i}),0,3);
end

save 'test_result.mat';