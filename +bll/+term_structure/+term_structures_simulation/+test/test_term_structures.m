clear; clc;
tic

%% Import external packages:
import bll.term_structure.term_structures_simulation.sim_funcs.* 
import bll.term_structure.*;
import dml.*;
import dal.market.filter.static.* 
import dal.market.filter.dynamic.*;
import dal.market.get.base_data.*;
import dal.market.get.static.* 
import dal.market.get.dynamic.* 
import dal.market.get.cash_flow.*;
import utility.* 
import utility.cash_flow.* ;

%% Basic data for the test:
settle = '2005-01-02'; % The minimum possible date is 02-01-2005 according to given data from database.

param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';   % For bootstrap model.

ir_type = 'forward';
model = {'bootstrap', 'nelson_siegel', 'svensson'};
mdl_num = length(model);
wnd_sz = 247; %?!

% bond_type
stat_param.class_name = {'GOVERNMENT BOND'};
stat_param.coupon_type = {'Fixed Interest'};
% stat_param.linkage_index = {'CPI'};
stat_param.linkage_index = {'NON LINKED'};
% stat_param.sector = {'COMMERCIAL BANKS', 'MORTGAGE BANKS AND FINANCIAL INSTITUTIONS', 'INSURANCE COMPANIES'};
dyn_param.ttm = {6 7};

%% Build list of examined assets:
[ examined_nsin, examined_ttm ] = examined_asset(settle, stat_param, dyn_param);
[ examined_data ] = get_bond_stat( examined_nsin, settle, {'last_ex_date', 'maturity_date', 'last_trading_date'} );

% Filterring assets with premature termination of trading:
min_ex_day = datestr(min(datenum(examined_data.last_ex_date)), 'yyyy-mm-dd');
max_maturity = datestr(max(datenum(examined_data.maturity_date)), 'yyyy-mm-dd');
[ lived_nsin ] = get_lived_assets(min_ex_day);
ind = ismember(examined_nsin, lived_nsin);
examined_nsin = examined_nsin(ind);
examined_ttm = examined_ttm(ind);
examined_data = examined_data(ind,:);
num_assets = numel(examined_nsin);

% Get required historic data for simulations:
[ examined_history ] = get_multi_dyn_between_dates( examined_nsin, {'price_dirty', 'ttm'}, 'bond', settle, max_maturity );
[ examined_cf ] = get_cash_flow( settle, max_maturity, examined_nsin );
[ examined_ex_days ] = get_bond_cf_dates( settle, max_maturity, examined_nsin );

%%  Determine simulation window and rolling period:
num_obs = numel(examined_history.price_dirty.dates);
num_sim = floor(num_obs/wnd_sz);
sim_date = examined_history.price_dirty.dates(1: num_sim*wnd_sz);
sim_date = reshape(sim_date, wnd_sz, num_sim);

%% Historic data reshape:
examined_history.ttm = examined_history.ttm(1: num_sim*wnd_sz);
examined_history.price_dirty = examined_history.price_dirty(1: num_sim*wnd_sz);

asset_ttm = zeros(wnd_sz,num_assets, num_sim);
market_price = zeros(wnd_sz,num_assets, num_sim);

for k = 1 : num_sim
    asset_ttm(:,:,k) = fts2mat(examined_history.ttm(ismember(examined_history.ttm.dates, sim_date(:,k))));
    market_price(:,:,k) = fts2mat(examined_history.price_dirty(ismember(examined_history.price_dirty.dates, sim_date(:,k))));
end

%% Memory allocation for simulation results:
fitted_price = zeros(wnd_sz,num_assets,num_sim, mdl_num);

%% Simulations:
coupon_type_flag = -1; % for type of basic bonds for curve building
colour = {'b', 'g', 'r'};
for k = 1: num_sim

    % Term Structure building:
    mdl = examined_model(model, ir_type, sim_date(1,k), coupon_type_flag, param);
    mdlprice = uniquesim( sim_date(:,k), mdl, examined_cf, examined_ex_days, param );
    
%     for j = 1: num_assets
%         figure(k)
%         plot(sim_date(:,k), market_price(:,j,k),':k'); hold on;
%         for m = 1:mdl_num
%             plot(sim_date(:,k), mdlprice.(model{m})(:,j), ['-' colour{m}]); hold on;
%         end
%         title(['simulation: ' num2str(k)])
%         hold off;
%     end
    % Bond pricing on base given Term Structures:
    for m = 1: mdl_num
        fitted_price(:,:,k, m) = mdlprice.(model{m});
    end
    
end

%% Error calculation:
fit_err = zeros(wnd_sz,num_assets,num_sim, mdl_num);
for m = 1:mdl_num
    for k = 1: num_sim
        fit_err(:,:,k,m) = (log(fitted_price(:,:,k, m)) - log( market_price(:,:,k))) .* sqrt(asset_ttm(:,:,k));
    end
end

%% Error statistics:
[time_stat, sim_stat, crv_stat] = error_stat(fit_err);

%% Fts creation:
market_price_per_sim = cell(num_assets,num_sim); % each cell includes fts of prices per assets
asset_ttm_per_sim = cell(num_assets,num_sim);
fit_err_per_sim = cell(num_assets,num_sim);
fitted_price_per_sim = cell(num_assets,num_sim);
for k = 1: num_sim 
    t = sim_date(:,k);
    for j = 1:num_assets
        market_price_per_sim{j,k} = fints(t, market_price(:,j,k), {'MarketPrice'}, [], ['asset: ' numid2strid(examined_nsin(j)) ', curve: ' num2str(k)]); 
        asset_ttm_per_sim{j,k} = fints(t, asset_ttm(:,j,k), {'ttm'}, [], ['asset: ' numid2strid(examined_nsin(j)) ', curve: ' num2str(k)]); 
        fit_err_per_sim{j,k} = fints(t, fit_err(:,j,k,:), model, [], ['asset: ' numid2strid(examined_nsin(j)) ', curve: ' num2str(k)]); 
        fitted_price_per_sim{j, k} = fints(t, [ fitted_price(:,j, k, 1),  fitted_price(:,j, k,2),  fitted_price(:,j, k, 3)], ...
            {'BootstrapPrice', 'NelsonSiegelPrice','SvenssonPrice'}, [], ['asset: ' numid2strid(examined_nsin(j)) ', curve: ' num2str(k)]); 
    end
end

%% Error statistics:
obs = [1:1:wnd_sz]';
for j = 1: num_assets
    % Statistics per asset and per each step throught am simulation
    time_error.mean{j} = fints(obs, time_stat.mean(:,j,:), model, [], numid2strid(examined_nsin(j)));
    time_error.std{j} = fints(obs, time_stat.std(:,j,:), model, [], numid2strid(examined_nsin(j)));
    time_error.min{j} = fints(obs, time_stat.min(:,j,:), model, [], numid2strid(examined_nsin(j)));
    time_error.max{j} = fints(obs, time_stat.max(:,j,:), model, [], numid2strid(examined_nsin(j)));
    time_error.skew{j} = fints(obs, time_stat.skew(:,j,:), model, [], numid2strid(examined_nsin(j)));
    time_error.kurt{j} = fints(obs, time_stat.kurt(:,j,:), model, [], numid2strid(examined_nsin(j)));
    
    % Statistics per asset and per simulation throught the time-window
    sim_error.mean{j} = fints(sim_date(1,:)', squeeze(sim_stat.mean(:,j,:))', model, [], numid2strid(examined_nsin(j)));
    sim_error.std{j} = fints(sim_date(1,:)', squeeze(sim_stat.std(:,j,:))', model, [], numid2strid(examined_nsin(j)));
    sim_error.min{j} = fints(sim_date(1,:)', squeeze(sim_stat.min(:,j,:))', model, [], numid2strid(examined_nsin(j)));
    sim_error.max{j} = fints(sim_date(1,:)', squeeze(sim_stat.max(:,j,:))', model, [], numid2strid(examined_nsin(j)));
    sim_error.skew{j} = fints(sim_date(1,:)', squeeze(sim_stat.skew(:,j,:))', model, [], numid2strid(examined_nsin(j)));
    sim_error.kurt{j} = fints(sim_date(1,:)', squeeze(sim_stat.kurt(:,j,:))', model, [], numid2strid(examined_nsin(j)));
end

% Statistics per simulation and per each step
for k = 1: num_sim
    crv_error.mean{k} = fints(sim_date(:,k), squeeze(crv_stat.mean(:,:,k)), model, [], ['curve' num2str(k)]);
    crv_error.std{k} = fints(sim_date(:,k), squeeze(crv_stat.std(:,:,k)), model, [], ['curve' num2str(k)]);
    crv_error.min{k} = fints(sim_date(:,k), squeeze(crv_stat.min(:,:,k)), model, [], ['curve' num2str(k)]);
    crv_error.max{k} = fints(sim_date(:,k), squeeze(crv_stat.max(:,:,k)), model, [], ['curve' num2str(k)]);
    crv_error.skew{k} = fints(sim_date(:,k), squeeze(crv_stat.skew(:,:,k)), model, [], ['curve' num2str(k)]);
    crv_error.kurt{k} = fints(sim_date(:,k), squeeze(crv_stat.kurt(:,:,k)), model, [], ['curve' num2str(k)]);
end

%% Resulting graphs:
yname = 'mean';
asset_ind = 1;
sim_ind = 2;
model = strrep(upper(model), '_', '-');
asset_legend = strrep(numid2strid(examined_nsin), 'id', '');
curve_legend = strrep(numid2strid((1:1:num_sim)'), 'id', 'curve');

err_graphs(market_price_per_sim, fitted_price_per_sim,  fit_err_per_sim, 'assets', asset_ind);
err_graphs(market_price_per_sim, fitted_price_per_sim,  fit_err_per_sim, 'simulations', sim_ind);

err_stat_graphs(time_error.(yname), yname, model, 'assets');
err_stat_graphs(time_error.(yname), yname, asset_legend, 'models');

err_stat_graphs(sim_error.(yname), yname, model, 'assets');
err_stat_graphs(sim_error.(yname), yname, asset_legend,  'models');

err_stat_graphs(crv_error.(yname), yname, model, 'simulations');
err_stat_graphs(crv_error.(yname), yname, curve_legend, 'models');