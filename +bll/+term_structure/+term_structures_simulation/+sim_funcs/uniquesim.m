function [ price ] = uniquesim( obs, curve, source_cf, ex_day, param )
%UNIQUESIM Summary of this function goes here
%   Detailed explanation goes here

    import bll.pricing.*;
    
    mdl_type = fieldnames(curve);
    data = NaN(length(obs), size(source_cf, 2));
    % Bond pricing on base given Term Structures:
    for k = 1: length(mdl_type)

        for i = 1: length(obs)
            % Get examinated bond cash flow:
            cf = source_cf(source_cf.dates > obs(i));
            ex_mat = fts2mat(ex_day(ex_day.dates > obs(i)), 1);
            ind = find(ex_mat(1,2) <= obs(i) & obs(i) <= ex_mat(1,1));
            if ~isempty(ind)
                tmp_cf = cf(ind+1:end);
            else
                tmp_cf = cf;
            end       

            if ~isempty(tmp_cf)
                data(i, :) = bnd_price(tmp_cf, curve.(mdl_type{k}) , obs(i), param);                                                                   
            end
        end
        price.(mdl_type{k}) = data;
    end

end

