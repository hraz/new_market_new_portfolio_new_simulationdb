clear; clc;

import dal.market.filter.static.*;
import dal.market.get.static.* 
import dal.market.get.dynamic.*;
import utility.*
import bll.term_structure.*;
import dml.*;

rdate = '2010-09-26';

global BASE_DATA;
BASE_DATA = base_data(rdate);

%% Coupon Type + Linkage + Sector + Bond Type:

 filter_name = 'type';
% filter_cond = {'Convertable'};
 filter_cond = {'Short Term Treasury Bill'};
% filter_cond = {'Government Bond - Shachar'};

[ nsin_makam ] = bond_static_partial( filter_name, filter_cond, rdate );%makam


filter_cond = {'Government Bond - Shachar'};

[ nsin_shahar ] = bond_static_partial( filter_name, filter_cond, rdate );%shachar


%% Get data static

 [ ds ] = get_bond_stat( nsin_makam, rdate );

%% Get data Dyn

data_name = 'ttm';

 [ fts ] = da_bond_dyn( nsin_makam, data_name, rdate );
 
t = fts(rdate);
ttm = fts2mat(t)'; %extract ttm for relevant day and put in matrix form

 
 data_name = 'ytm';

 [ fts ] = da_bond_dyn( nsin_makam, data_name, rdate );

 y = fts(rdate);
 
 ytm = fts2mat(y)'; %extract ytm for relevant day and put in matrix form

 [ttm_makam_sorted indx_mak] = sort(ttm);
 ytm_makam_sorted = ytm;
 
 indx_mak_leng = length(indx_mak);
 
    for k=1:1:indx_mak_leng
       
    ytm_makam_sorted(k) = ytm(indx_mak(k));
   end
 
 ttm = ttm_makam_sorted;
 ytm = ytm_makam_sorted;
 
 plot(ttm, ytm);

 
%% extract shahar bonds
data_name = 'ttm';
 
  [ fts ] = da_bond_dyn( nsin_shahar, data_name, rdate );
 
 ttm_date = fts(rdate); %extract ttm for appropriate date
 
ttm_date_mat = fts2mat(ttm_date)'; %matrix form

[ttm_sorted indx] = sort(ttm_date_mat); 
% sort matrix according to decreasing ttm in order to sort shahar
% bonds in the same way

indx_leng = length(indx);

 shahar_ids = fieldnames(ttm_date);
% 
  sh = zeros(indx_leng, 1);

  for k=1:1:indx_leng-1
      
      
    sh(k) = strid2numid(shahar_ids(indx(k) +3));
% %          %get ids of bonds in increasing ttm order
% % 
%  
  [ttm ytm] = shahar_calc(sh(k), rdate, ttm, ytm);
  hold off
% % 
  end
  
  
    plot(ttm, ytm)





 
