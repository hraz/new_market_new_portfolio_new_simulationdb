function [ zero_rate] = get_zero_rates(obj, req_dates, settle, varargin)
%IR_DATA_CURVE/GET_ZERO_RATE Get zero rates for input dates
%
%   [ zero_rate] = get_zero_rates( obj, req_dates )
%   [ zero_rate] = get_zero_rates( obj, req_dates, settle )
%   [ zero_rate] = get_zero_rates( obj, req_dates, settle, varargin ) returns zero
%   rates for the input dates.
%
%   Input:
%       obj - is an ir_data_curve object represented the given curve.
%
%       req_dates - is an NDATESx1 cell-array of strings or numeric vector of required dates.
%
%   Optional input:
%       settle - is a scalar value specifying some settlement date different from the settlement of
%                   the curve.
%
%       compounding - is a compounding frequency for curve, acceptable values are -1,1(default),2,3,4,6,12.
%
%       basis - is a day-count basis.
%                   Possible values include:
%                       0 - actual/actual
%                       1 - 30/360 SIA
%                       2 - actual/360
%                       3 - actual/365
%                       4 - 30/360 PSA
%                       5 - 30/360 ISDA
%                       6 - 30/360 European
%                       7 - actual/365 Japanese
%                       8 - actual/actual ISMA
%                       9 - actual/360 ISMA
%                       10 - actual/365 ISMA (default)
%                       11 - 30/360 ISMA
%                       12 - actual/365 ISDA
%                       13 - bus/252
%
%   Output:
%       zero_rate- is an NREQDATESx1 financial time-series object represented zero rates for
%                               the input dates. 

% Yigal Ben Tal
% Copyright 2012.

    %% Import external variables:
    import bll.term_structure.*;

    %% Input validation:
    error(nargchk(1,4,nargin));
    
    % Check the object class:
    if ~isa(obj, 'bll.term_structure.ir_data_curve')
        error('ir_data_curve:get_zero_rates:invalidObject',...
            'Object must be one of ir_data_curve class.');
    end
    
    % Reformat required dates:
     if (nargin <2) || isempty(req_dates)
        req_dates = obj.Data.dates;
    else
        req_dates = finargdate(req_dates);
        req_dates = req_dates(:);
     end
    
    % Validation of optional input:
    if (nargin < 3) || isempty(settle)
        settle = obj.Settle;
    end
    
    if ( nargin == 4 ) && isempty( varargin{:} )
        [req_comp_freq,req_basis] = parse_optinp( varargin{:} );
        if isempty(req_comp_freq),req_comp_freq = obj.Compounding;end
        if isempty(req_basis),req_basis = obj.Basis;end
    else
        req_comp_freq = obj.Compounding;
        req_basis = obj.Basis;
    end
    
    % Extract dates and data from the curve object:
    cur_dates = obj.Data.dates;
    cur_rates = fts2mat(obj.Data);
    
    % Remove current settlement date from the curve dates list:
    ind = (settle < cur_dates);
    cur_dates = cur_dates(ind);
    cur_rates = cur_rates(ind);
    
    %% Step #1 (Create zero rates on base existed interest rate curve):
    switch lower(obj.Type)
        case 'zero'
            [ zero_rate] = ir_data_curve.interp( cur_dates, cur_rates, req_dates, obj.InterpMethod );
            [ zero_rate] = ir_data_curve.rate_convert( zero_rate, obj.Compounding, obj.Basis, req_comp_freq, req_basis, settle, req_dates );
        case 'forward'
            dates = unique( [obj.Data.dates; req_dates] );
            [ fwd_rate ] = ir_data_curve.interp( cur_dates, cur_rates, dates, obj.InterpMethod );
            [ zero_rate] = fwd2zero( fwd_rate, dates, settle, obj.Compounding, obj.Basis, req_comp_freq, req_basis );
            idx = ismember( dates, req_dates );
            zero_rate= zero_rate( idx );
            req_dates = dates( idx );
        case 'discount'
            [ df ] = ir_data_curve.interp( cur_dates, cur_rates, req_dates, obj.InterpMethod );
            [ zero_rate] = disc2zero( df, req_dates, settle, obj.Compounding, obj.Basis );
    end
    
    %% Step #2 (Build fts):
    [ zero_rate] = fints( req_dates, zero_rate, 'zero_rate' );
    
end