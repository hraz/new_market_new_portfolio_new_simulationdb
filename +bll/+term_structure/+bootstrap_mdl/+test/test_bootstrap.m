clear; clc;

%% Import external packages:
import bll.term_structure.bootstrap_mdl.*;
import dal.data_acess.cash_flow.*;
import utility.cash_flow.*;
import dal.market.get.dynamic.*;

%% Build start parameters:
settle = datenum('2005-01-02'); % The minimum possible date is 02-01-2005 according to given data from database.

param.compounding = 1;
param.basis = 10;
param.interpmethod = 'cubic';

ir_type = 'forward';

%% Cash flow build:
[ tbill_cf, tbill_nsin ] = cfbuilder(settle, 0);
tbill_price = get_bond_dyn_single_date(tbill_nsin, 'price_dirty', settle);

[ shahar_cf, shahar_nsin ] = cfbuilder(settle, 1);
shahar_price = get_bond_dyn_single_date(shahar_nsin, 'price_dirty', settle);

gov_cf = merge(tbill_cf, shahar_cf);
gov_price = merge(tbill_price, shahar_price);

%% Build zero-coupon curve:
% [ tbill_ircurve ] = bootstrap( ir_type, tbill_price, tbill_cf, settle, param );
% figure(1);
% plot(tbill_ircurve.Data, '.-b')

% [ shahar_ircurve ] = bootstrap( ir_type, shahar_price, shahar_cf, settle, param );
figure(2);
% plot(shahar_ircurve.Data, '.-g'); hold on;

% figure(3);
[ gov_ircurve ] = bootstrap( ir_type, gov_price, gov_cf, settle, param );
plot(gov_ircurve.Data, '.-g')
