function [para]=VasParaMLEstimate(data,delta)
%VasParaMLEstimate returns the Maximum  Likelyhood estimated parameteres
%of the Vasicek model
%
%   [para]=VasParaMLEstimate(data,delta) receives a set of rates and
%   returns the values of the Vasicek parameters (k, teta, sigma)
%   calculated according to (3.14-3.16) in 'Interest Rate Models � Theory and
%   Practice' of Brigo & Mercurio.
%
%   Input:
%       data - is a NDateX1 numeric vector containing the instaneneous
%                     rates for different times. rates must be sampled periodically.
%       delta - the time interval of the sampling of rates.
%
%   Output:
%       para - structure containing calculated vasicek prameters, according to
%           the notation of (3.5) in 'Interest Rate Models � Theory and
%           Practice' of Brigo & Mercurio:
%           dr(t)=k*(teta - r(t)) * dt + sigma * dW(t)
% 
% Amit Godel
% Copyright 2012

N=length(data)-1;
alpha=(N*sum(data(1:end-1).*data(2:end)) - sum(data(1:end-1))*sum(data(2:end))) / ...
    (N*sum(data(1:end-1).^2) - (sum(data(1:end-1)))^2);
para.k = -log(alpha)/delta;
para.teta=sum(data(2:end) - alpha*(data(1:end-1)))/(N*(1-alpha));
V2=sum( (data(2:end)-alpha*data(1:end-1)-para.teta*(1-alpha) ).^2)/N;
para.sigma=sqrt(2*para.k*V2/(1-exp(-2*para.k*delta)));
