function [p p2]=BondPrice(rM,t,t0,T)
import bll.term_structure.vasicek.*
p=mean(exp(-sum(((rM(1,2:end)+rM(1,1:end-1))/2).*(t(2:end)-t(1:end-1)))));
p2=mean(exp(-TrapezInt(t,rM(1,:),t0,T)));
