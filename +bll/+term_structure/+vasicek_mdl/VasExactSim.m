function [rM t] = VasExactSim(rt0,para,t0,T,Ndt,M)
% VasExactSim returns the simulated rate paths according to Vasicek model
%
%    [rM t] = VasExactSim(rt0,para,t0,T,Ndt,M) receives the Vasicek
%    parameteres and the simulation properties (steps and repetition time )
%    and calculates stochastic rate-time paths according to the Vasicek
%    model using the exact expression for the mean rate after the time step
%    as given in (3.7) if Brigo&Mercurio:
%    r(t+dt)=r(t)*exp(-k*dt) + teta*(1-exp(-k*dt))+
%                       sigma*sqrt((1-exp(-2k*dt))/2k)*NormalDist(0,1)
%
%   Input:
%       rt0 - initial rate = r(t=0)
%       para - structure containing vasicek prameters
%       t0,T - desired initial and end times
%       Ndt - resolution of time steps
%       M - number of repetitions (different realizations)
% 
%   Output:
%       rM - M_X_NDates array containing the M different simulation of the
%               insatatneous rate
%       t - 1_X_NDates array containing dates for which the simulation  was
%           made
% 
% Amit Godel
% Copyright 2012
t=linspace(t0,T,Ndt+1);
dt=(T-t0)/Ndt;
rM=zeros(M,Ndt+1);
ekt=exp(-para.k*dt);
one_minus_ekt=1-ekt;
last_term=para.sigma*sqrt((1-ekt^2)/(2*para.k));

rM(:,1)=rt0;
for k=2:length(t)
    randM=randn(M,1);
    rM(:,k)=rM(:,k-1)*ekt + para.teta*one_minus_ekt + last_term*randM;
end
