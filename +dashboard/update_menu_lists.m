function [ handles ] = update_menu_lists( handles, isrnd )
%UPDATE_MENU_LISTS updates the constraint listboxes for the Model and
%   Additonal Contraint Options tabs on the dashboard.
%
%   [ handles ] = update_menu_lists( handles, isrnd ) receives the dashboard
%   GUI handle and sets the constraint listboxes to values retrieved from the
%   database based on selections made by the dashboard user.
%
%   Input:
%       handles - is the dashboard GUI handles.
%
%       isrnd - is a boolean flag specifying whether random updating of the
%       constraint listboxes has been requested. (Default is FALSE).
%
%   Output:
%       handles - is the dashboard GUI handles that have been updated using
%       database data.
%   
%       drpbx_choosed_value - is a struct with three fields: model,
%       opt_type and hold_period. These fields are scalar values
%       representing, respectively, chosen model, optimization type and
%       holding period length.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 22, 2013 (implemented multiple selections in the
% constraint listboxes and updated comments)
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Import external packages:
    import dashboard.*
    import utility.dataset.*
    import dal.portfolio.data_access.*
 
    %% Input validation:
    error(nargchk(1, 2, nargin));
    if nargin < 2
        isrnd = false; % default
    end
    
    %% Step #1 Get current selected value(s) in the constraint listboxes and and adapt them for the database:
    [ valueList, currentValue, timePeriodBound, simBound ] = get_current_menu_value( handles );
    [ adaptedCurrentValue, adaptedTimeBound, adaptedSimBound ] = adapt_current_value_4db( valueList, currentValue, timePeriodBound, simBound );
    
    %% Step #2 Get constraint lists as specified by the user from the database
    [ constraintList ] = get_menu_lists(adaptedCurrentValue,  adaptedSimBound.from_sim, adaptedSimBound.to_sim, adaptedTimeBound.from_date, adaptedTimeBound.to_date );    
    
    %% Step #3 Build full constraint lists, and define chosen values in these full constraint lists:
    menuName = fieldnames(constraintList);
    for i = 1 : numel(menuName)
        
        if isnumeric(constraintList.(menuName{i}))
                constraintList.(menuName{i}) = cellstr(num2str(constraintList.(menuName{i})));
        end
            
        if size(constraintList.(handles.filter_list{i}),1) > 1
            constraintList.(menuName{i}) = [valueList.(menuName{i})(1); constraintList.(menuName{i})];
        else
            constraintList.(menuName{i}) = [valueList.(menuName{i})(1); constraintList.(menuName{i})];
        end
    end
 
 % temporary solution to have the reinvestment options display correctly
 constraintList.reinvestment = numbers_to_names_reinvestment(constraintList.reinvestment);
    
    %% Step #4 Set chosen constraint values:
    [ handles ] = set_menu_values( handles, constraintList , currentValue );
 
    %% Step #5 Set result simulation range:
    [ handles ] = update_sim_range( handles, num2str(simBound.from_sim), num2str(simBound.to_sim) );
    
end


