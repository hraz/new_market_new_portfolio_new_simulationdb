function [ handles ] = set_port_info( handles, portSummaryTbl, portStatisticsTbl )
%SET_PORT_INFO set given portfolio tables to dashboard handles.
%
%   [ handles ] = set_port_info( handles, portSummaryTbl, portStatisticsTbl )
%   receives dashboard GUI handles set and portfolio data information tables.
%
%   Input:
%       handles - a struct of GUI handles
%
%       portSummaryTbl - an Nx6 cell array.  The rows are different bonds
%       in a specific portfolio and the columns are 'Symbol', 'Sector',
%       'Security Class', 'Weight(%)', 'Duration', and 'Yield(%)'.
%
%       portStatisticsTbl - a 14xN cell array.  The rows are different
%       statistical properties of the portfolio:'Expected Return', 'Holding
%       Return', 'Volatility', 'Skewness', 'Kurtosis', 'Alpha', 'Beta',
%       'Unique Risk', 'Sharpe Ratio', 'Jensen Ratio', 'Omega', 'Info
%       Ratio', 'RSquare vs. Exp. Return', and 'RSquare vs. Benchmark'.
%       The first column are these row labels as char arrays, the rest of
%       the columns are different periods (Period 1, Period 2, etc), with
%       the values represented as doubles.
%
%   Output:
%       handles - a struct of GUI handles
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

%% Update portfolio listboxes to show info about current highlighted portfolio

% PLACE WHERE COMMAND IS NEEDED TO UPDATE PORTFOLIO LISTS - WILL BE
% INSERTED AFTER SWITCH TO NEW DATABASE

%% Update portfolio table data in the GUI:
set(handles.prt_summary_tbl, 'Data', portSummaryTbl);

if ~isempty(portStatisticsTbl)
    set(handles.prt_statistics_tbl, 'Data', portStatisticsTbl);
end
