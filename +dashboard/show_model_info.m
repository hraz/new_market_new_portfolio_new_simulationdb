function [  ] = show_model_info( histHandle, data, tblHandle, statTbl, flagNormHistogram )
%SHOW_MODEL_INFO displays the data in data as a histogram and displays the
%   data in statTbl.
%   
%   [  ] = show_model_info( histHandle, data, tblHandle, statTbl, flagNormHistogram )
%   takes in portfolio data and graphs that data.  It also takes in a table
%   of statistics about that data and displays that information below the
%   histogram.
%
%   Input :
%       histHandle - a double that represents the handle where the
%           histogram needs to be graphed.
%
%       data - Nx1 array of doubles, where N is the number of portfolios
%           returned by the database.
%
%       tblHandle - a double that represents the handle where the data in
%       statTbl needs to be displayed.
%
%       statTbl - 4x2 cell array, the first column are the labels ('Mean',
%       'Std', 'Positive values (%)', and 'Positive values> 1% (%)').  The
%       second column are doubles that correspond to those labels.
%
%       flagNormHistogram - a boolean that indicates whether to normalize
%           the histogram or not.
%
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.
%
% Modified by Eleanor Millman, July 31, 2013. Introduced function
% hist_fit_normalized to draw in red normalized fit line.
% Copyright 2013, BondIT Ltd.

import utility.statistics.*

    %% Distribution histogram of given data building
%     data = data * 100;
        
    axes(histHandle);
%     if length(data) > 30
        if flagNormHistogram
            hist_fit_normalized(data);
        else
            histfit(data);
        end
%     else
%         cla reset;
%     end
%     xlim([-6.5 6.5])
    set(histHandle,'XColor','green'); %[0.729 0.831 0.957]);
    set(histHandle,'YColor','green'); %[0.729 0.831 0.957]);
    
    %% Set data statistics table:
   if ~isempty(statTbl)
        set(tblHandle, 'Data', statTbl);
%         set(tbl_handle, 'ForegroundColor', 'black');
   end
    
end

 