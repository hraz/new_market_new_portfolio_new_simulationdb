function [ adaptedCurrentValue, adaptedTimeBound, adaptedSimBound ] = adapt_current_value_4db( valueList, currentValue, timeBound, simBound )
%ADAPT_CURRENT_VALUE_4DB prepare all filters choices to database acceptable string value.
%
%   [ adaptedCurrentValue, adaptedTimeBound, adaptedSimBound ] = adapt_current_value_4db( valueList, currentValue, timeBound, simBound )
%   receives lists of all filters' possible values, its choices, time and simulation bounds, and
%   returns list of current values, time and simulation bonds adapted to database input format.
%
%   Input:
%
%   Output:
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import utility.dal.*;
    
    %% Input validation;
    error(nargchk(4, 4, nargin));
    
    %% Step #1 (Build adapted data filter choices):
     
    % temporary solution to reinvestment options display problem
    import dashboard.*
    valueList.reinvestment = names_to_numbers_reinvestment(valueList.reinvestment);
    currentValue.reinvestment = names_to_numbers_reinvestment(currentValue.reinvestment);
    
    menuList = fieldnames(currentValue);
    for i = 1: length(menuList)
        %if all the constraint possibilities are being requested
        if isempty(currentValue.(menuList{i})) | (length(currentValue.(menuList{i})) == 1 & (any(strcmpi(currentValue.(menuList{i}), valueList.(menuList{i})(1)))))
            adaptedCurrentValue.(menuList{i}) = 'NULL';
            %if the constraint is one with only numbers as options
        elseif (ismember(menuList{i}, { 'target_return'; 'investment_horizon'; 'benchmark'; 'max_weight';'reinvestment'}))
            if isnan(str2double(currentValue.(menuList{i})))
                adaptedCurrentValue.(menuList{i}) = 'NULL';
            else
                adaptedCurrentValue.(menuList{i})  = cellstr2strlist(currentValue.(menuList{i}));
            end
        else
            adaptedCurrentValue.(menuList{i})  = cellstr2strlist(currentValue.(menuList{i}));
        end        
    end
    
    %% Step #2 (Build adapted time bounds):
    timeName = fieldnames(timeBound);
    for j = 1 : length(timeName)
        if isempty(timeBound.(timeName{j}))
            adaptedTimeBound.(timeName{j}) = 'NULL';
        else
            adaptedTimeBound.(timeName{j}) = cell2str(timeBound.(timeName{j}));
        end
    end
    
    %% Step #3 (Build adapted simulation bounds):
    simBoundName = fieldnames(simBound);
    for k = 1: length(simBoundName)
        if isempty(simBound.(simBoundName{k}))
            adaptedSimBound.(simBoundName{k}) = 'NULL';
        else
            adaptedSimBound.(simBoundName{k}) = simBound.(simBoundName{k});
        end
    end
    
end

