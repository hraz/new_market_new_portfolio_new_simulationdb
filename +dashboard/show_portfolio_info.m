function [ handles ] = show_portfolio_info( handles, portSummaryTbl, portStatisticsTbl, portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet )
%SHOW_PORTFOLIO_INFO takes information about a specified portfolio and
%   graphs that data.
%
%   [ handles ] = show_portfolio_info( handles, portSummaryTbl, portStatisticsTbl, portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet )
%   accepts as input various data series that represent the cumulative
%   return of the portfolio, the cumulative return of its corresponding
%   benchmark, the positive excess return, and the negative excess return,
%   all over the same time period, and graphs this information in the
%   portfolio tab.
%
%   Input:
%       handles - the struct of GUI handles
%
%       portSummaryTbl - Nx6 cell array that gives information about the
%           specific bonds in the specified portfolio.  Each row corresponds to
%           a different bond.
%
%       portStatisticsTbl - 14xN cell array that gives information about
%           the portfolio as a whole, according to each time period.  The
%           information given is 'Expected Return', 'Holding Return',
%           'Volatility', 'Skewness', 'Kurtosis', 'Alpha', 'Beta', 'Unique
%       	Risk', 'Sharpe Ratio', 'Jensen Ratio', 'Omega', 'Info Ratio',
%       	'RSquare vs. Exp. Return', and 'RSquare vs. Benchmark'
%
%       portCumulativeReturn - Nx1 fints, where N is the number of days for
%           which portfolio information will be displayed.  This is the
%           cumulative return of the portfolio.
%
%       benchCumulativeReturn - Nx1 fints, where N is the number of days for
%           which benchmark information will be displayed (this N is the
%           same N as in portCumulativeReturn).  This is the cumulative
%           return of the benchmark.
%
%       postExcessRet - Nx1 fints, where N is the number of days for
%           which portfolio information will be displayed.  This is the
%           positive excess return of the portfolio.
%
%       negExcessRet - Nx1 fints, where N is the number of days for
%           which portfolio information will be displayed.  This is the
%           negative excess return of the portfolio.
%
%   Output:
%       handles - the struct of GUI handles
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

import dashboard.*;

% A light blue that can be seen against both a dark grey and white background.
AXISCOLOR = [0.729 0.831 0.957];

%% Step #7 (Set dashboard variables):
set_port_info( handles, portSummaryTbl, portStatisticsTbl );

%% Step #8 (Show given cumulative returns in the GUI):

if (portCumulativeReturn.dates(1) ~= benchCumulativeReturn.dates(1)) || (portCumulativeReturn.dates(end) ~= benchCumulativeReturn.dates(end))
    error('show_portfolio_info: portfolio and corresponding benchmark don''t have same start and/or end dates')
end

set(handles.prt_summary_pnl, 'Title', ['Portfolio Summary: ' num2str(size(portSummaryTbl,1)), ' bonds' ]);

axes(handles.prt_ret_vs_benchmark_graph);

% Turn on data cursor mode and set the update function so that the text
% for the x and y values will be correct in the GUI.
dcmObj = datacursormode(gcf);
set(dcmObj,'UpdateFcn',@cursor_update_function)

% Find the min and max of the data so all data is shown on the plots.
xMax = max([portCumulativeReturn.dates; benchCumulativeReturn.dates]);
xMin = min([portCumulativeReturn.dates; benchCumulativeReturn.dates]);
yMax = max([fts2mat(portCumulativeReturn); fts2mat(benchCumulativeReturn)]);
yMin = min([fts2mat(portCumulativeReturn); fts2mat(benchCumulativeReturn)]);
yWhiteSpace = 0.05*(yMax-yMin);
yMax = yMax + yWhiteSpace;
yMin = yMin - yWhiteSpace;

% First plot the portfolio
% Using plotyy in order to have a copy of the y-axis both on the left and the
% right hand side of the plot.
xData = portCumulativeReturn.dates;
yData = fts2mat(portCumulativeReturn);
[ax,h1a,h1b] = plotyy(xData,yData,xData,yData);

axis(ax,[xMin,xMax,yMin,yMax])

hold on

% Set the color to AXISCOLOR (a light blue) so that the tick labels can be seen
% against the black but the gridlines can be seen against the white.
set(ax(1),'YColor',AXISCOLOR)
set(ax(2),'YColor',AXISCOLOR)
set(ax,'XTickLabel','','XTick',[])
set(h1a,'Color','black', 'LineWidth',2 )
set(h1b,'Visible','off' )


% Now plot the benchmark
h2 = plot(benchCumulativeReturn);

legend('off')
set(h2,'Color','blue', 'LineWidth',2 )
set(gca,'XColor',AXISCOLOR,'YColor',AXISCOLOR)

hold off

% Display the legend
legend('Portfolio Cumulative Return', 'Benchmark Cumulative Return', 'Location','Best')
set(handles.prt_ret_vs_benchmark_graph,'XColor',AXISCOLOR,'YColor',AXISCOLOR)

% Make the Excess Return graph
axes(handles.prt_excess_return)
h3 = plot(posExcessRet);hold off;
set(h3,'Color',[1.000 0.000 0.000], 'LineWidth',2 )
hold on
h4 = plot(negExcessRet);hold off;
set(h4,'Color',[0.000 0.498 0.000], 'LineWidth',2 )
legend('Excess Return', 'location','best')
set(handles.prt_excess_return,'XColor',AXISCOLOR,'YColor',AXISCOLOR)
grid(handles.prt_excess_return, 'on')


set(handles.prt_graphed_benchmark_listbox,'UserData',portCumulativeReturn)
