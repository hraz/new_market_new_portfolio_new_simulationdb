function [ handles ] = set_model_info( handles, chitH, kstH, lltH, ttH1, ttH2, ttH3, d1Sts, d2Sts, d3Sts )
%SET_MODEL_INFO initialize dashboard handles fom model group with given data.
%
%   [ handles ] = set_model_info( handles, chitH, kstH, lltH, ttH1, ttH2, ttH3, d1Sts, d2Sts, d3Sts )
%   receives dashboard GUI handles set and all model statistical results on model and initialize the
%   handles with its data.
%
%   Input:
%       handles - is a set of dashboard GUI handles set.
%
%       chitH - is an 1xTWOVALUES vector specifying result of CHI-test with its probability.
%
%       kstH - is an 1xTWOVALUES vector specifying result of Kolmogorov-Smirnov-test with its probability.
%
%       lltH - is an 1xTWOVALUES vector specifying result of  Lilliefors test  with its probability.
%
%       ttH1 - is an 1xTWOVALUES vector specifying the T-test result with its probability on the
%                   delta1 data.
%
%       ttH2 - is an 1xTWOVALUES vector specifying the T-test result with its probability on the
%                   delta2 positive data.
%
%       ttH3 - is an 1xTWOVALUES vector specifying the T-test result with its probability on the
%                   delta3 positive data.
%
%       d1Sts - is an 1xTWOVALUES vector specifying the mean and std on the delta1 data.
%
%       d2Sts - is an 1xTWOVALUES vector specifying the mean and std on the delta2 data.
%
%       d3Sts - is an 1xTWOVALUES vector specifying the mean and std on the delta3 data.
%
%   Output:
%       handles - is a set of updated dashboard GUI handles set.
%
%	Note:
%       delta1 = (return - exp_return) / real_volatility.
%       delta2 = sharpe(portfolio) - sharpe(benchmark)
%       delta3 = info_ratio(portfolio)
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dashboard.*;

    %% Set statistical tests' results:
    set(handles.chi_tst_res, 'String', num2cell(chitH(1)));
    set(handles.chi_probability_res, 'String', [num2str((chitH(2)*100), 2), '%']);

    set(handles.ks_tst_res, 'String', num2cell(kstH(1)));
    set(handles.ks_probability_res, 'String', [num2str((kstH(2) * 100), 2), '%']);

    set(handles.ll_tst_res, 'String', num2cell(lltH(1)));
    set(handles.ll_probability_res, 'String', [num2str(lltH(2), 2), '%']);

    set(handles.td1_tst_res, 'String', num2cell(ttH1(1)));
    set(handles.td1_probability_res, 'String', [num2str((ttH1(2) * 100), 2), '%']);

    set(handles.td2_tst_res, 'String', num2cell(ttH2(1)));
    set(handles.td2_probability_res, 'String', [num2str((ttH2(2) * 100), 2), '%']);

    set(handles.td3_tst_res, 'String', num2cell(ttH3(1)));
    set(handles.td3_probability_res, 'String', [num2str((ttH3(2) * 100), 2), '%']);
    
    set(handles.delta1_mean_res, 'String', num2str(d1Sts(1)));
    set(handles.delta1_std_res, 'String', num2str(d1Sts(2)));
    
    set(handles.delta2_mean_res, 'String', num2str(d2Sts(1)));
    set(handles.delta2_std_res, 'String', num2str(d2Sts(2)));
    set(handles.pcnt_pos_d2, 'String', [num2str(round(d2Sts(3) * 100), 2), '%']);
    
    set(handles.delta3_mean_res, 'String', num2str(d3Sts(1)));
    set(handles.delta3_std_res, 'String', num2str(d3Sts(2)));
    set(handles.pcnt_pos_d3, 'String', [num2str(round(d3Sts(3) * 100), 2), '%']);
    
    %% Set Confidence level results:
    set(handles.conf_lvl_res, 'String', [num2str(95), '%']);

end

