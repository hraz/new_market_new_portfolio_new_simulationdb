function [ portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet ] = get_cumulative_return(portfolioID, benchmarkID, creationDate, lastUpdate )
%GET_CUMULATIVE_RET returns cumulative return during given period of the portfolio and its benchmark.
%
%   [ portCumulativeReturn, benchCumulativeReturn, posExcessRet, negExcessRet ] = get_cumulative_return(portfolioID, benchmarkID, creationDate, lastUpdate )
%   receives dml\portfolio object and returns its cumulative holding return with the same value of
%   its benchmark. The used period for the holding return calculation is a period from portfolio
%   creation date untill last known update.
%
%   Input:
%       port - is a dml\portfolio class object defines some investment portfolio.
%
%   Output:
%       portCumulativeReturn - is an NOBSERVATIONSx1 fts specifying the last known portfolio
%                                           cumulative holding return during period between creation date and last update.
%
%       benchCumulativeReturn - is an NOBSERVATIONx1 fts specifying the last known benchmark
%                                           cumulative holding return during period between given
%                                           portfolio creation date and ts last known update.
%
% Yigal Ben Tal
% Copyright 2012, BondIT Ltd.
%
% Modified by Eleanor Millman, July 30, 2013. Changed variable names to be
% consistant with new style guidelines (variable_name is changed to
% variableName).
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.market.get.dynamic.*;
    import dal.portfolio.data_access.*;
    import utility.fts.*;
    
    portCumulativeRet = da_port_income_history(portfolioID, creationDate, lastUpdate);
    portCumulativeRet.value = portCumulativeRet.value/portCumulativeRet.value(1);
    portCumulativeReturn = fints(portCumulativeRet.obs_date,portCumulativeRet.value, {'port_cumulative_ret'} );
    
    [ ~, benchCumulativeRet] = get_bench_holding_return( benchmarkID, portCumulativeReturn.dates(1), portCumulativeReturn.dates(end) );
    ind = ismember(benchCumulativeRet(:,1),portCumulativeReturn.dates);
    benchCumulativeRet = benchCumulativeRet(ind, :);
    benchCumulativeReturn = fints(benchCumulativeRet(:,1), benchCumulativeRet(:,2), {'bench_cumulative_ret'});

    
    posExcessRet = double(portCumulativeRet(:,2)) - benchCumulativeRet(:,2);
    negExcessRet = posExcessRet;
    negExcessRet(posExcessRet < 0) = NaN;
    
    posExcessRet = fints(portCumulativeReturn.dates, posExcessRet, {'pos_excess_ret'});
    negExcessRet = fints(portCumulativeReturn.dates, negExcessRet, {'neg_excess_ret'});
    
end

