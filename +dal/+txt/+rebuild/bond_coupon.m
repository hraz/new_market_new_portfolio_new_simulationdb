function [ fix_ds, float_ds ] = bond_coupon( source_ds, unique_field, cmn_id, last_ind )
%BOND_COUPON create a dataset for bond_fix_coupon & bond_float_coupon tables.
%
%   [ target_ds ] = bond_coupon( source_ds, unique_field, cmn_id, last_ind )
%
%   Inputs:
%       source_ds - it is a dataset object specifying the raw source data.
%                          See description about DATASET function in Statistic Toolbox. 
%
%       unique_field - it is a string value specifying the name of unique
%                            field in target dataset.
%
%       cmn_id - it is an NBONDSx1 vector specifying unique id numbers of
%                       bonds.
%
%       last_ind - it is an NBONDSx1 vector specifying unique row index of
%                       all bonds in source dataset.
%
%   Outputs:
%       fix_ds - it is a dataset specifying description of fixed coupon bonds.
%
%       float_ds - it is a dataset specifying description of floating coupon bonds.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(4, 4, nargin));
    
    if (~isa(source_ds, 'dataset'))
        error('dal_txt_rebuild:bond_coupon:wrongInput', 'The first input argument has not a dataset type');
    end
    
    if (~ischar(unique_field))
        error('dal_txt_rebuild:bond_coupon:wrongInput', 'Unique field name is not a string.');
    end
     
    if (~isnumeric(cmn_id)) || (~isnumeric(last_ind))
        error('dal_txt_rebuild:bond_coupon:wrongInput', 'At least one of cmn_id or last_ind arguments is not numerical vector.');
    end
    
    if (size(cmn_id, 1) ~= size(last_ind, 1))
        error('dal_txt_rebuild:bond_coupon:wrongInput', 'The dimenion of cmn_id or last_ind arguments is not equal.');    
    end
    
    %% Step #1 (Coupon type filtering):
    [coupon_type, ~, coupon_ind] = unique(source_ds(:, unique_field));

    %% Step #2 (Creating dataset for bond_fix_coupon table):
    [ fix_ds ] = bond_fix_coupon( cmn_id, coupon_type.coupon_type, coupon_ind( last_ind ) );
    
    %% Step #3 (Creating dataset for bond_float_coupon table):
    [ float_ds ] = bond_float_coupon( cmn_id, coupon_type.coupon_type, coupon_ind( last_ind ) );
    
end

function [ target_ds ] = bond_fix_coupon( cmn_id, coupon_type, coupon_ind )
%BOND_FIX_COUPON create dataset for bond_fix_coupon table.
%
%   [ target_ds ] = bond_fix_coupon( cmn_id, coupon_type, coupon_ind )
%   receive common bond unique id, coupon type list and list of row numbers
%   with given coupon types, and return dataset for required
%   bond_fix_coupon table.
%
%   Inputs:
%       cmn_id - it is an NBONDSx1 vector specifying unique index of all
%                       bonds.
%
%       coupon_type - it is an NTYPESx1 string vector specifying all types of coupons. 
%
%       coupon_ind - it is an NTYPESx1 vector specifying places of given
%                       types in source list of bonds.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
%     error(nargchk(3, 3, nargin));
    
    %% Step #1 (Find id number of bonds with fixed coupon type):
    bond_id = findrow( cmn_id, 'FIX', coupon_type, coupon_ind );
    
    %% Step #2 (Coupon rate simulation):
    coupon_rate = rand(length(bond_id), 1) / 10;
    
    %% Step #3 (Dataset creation):
    target_ds = dataset(bond_id, coupon_rate);

end

function [ target_ds ] = bond_float_coupon( cmn_id, coupon_type, coupon_ind )
%BOND_FLOAT_COUPON create dataset for bond_float_coupon table.
%
%   [ target_ds ] = bond_float_coupon( cmn_id, coupon_type, coupon_ind )
%   receive common bond unique id, coupon type list and list of row numbers
%   with given coupon types, and return dataset for required
%   bond_float_coupon table. 
%
%   Inputs:
%       cmn_id - it is an NBONDSx1 vector specifying unique index of all
%                       bonds.
%
%       coupon_type - it is an NTYPESx1 string vector specifying all types of coupons. 
%
%       coupon_ind - it is an NTYPESx1 vector specifying places of given
%                       types in source list of bonds.
%
%   Outputs:
%       target_ds - it is a dataset object. See description about DATASET
%                           function in Statistic Toolbox.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
%     error(nargchk(3, 3, nargin));
    
    %% Step #1 (Find id number of bonds with float coupon type):
    bond_id = findrow(cmn_id, 'VAR', coupon_type, coupon_ind);
    
    %% Step #2 (Floating index and spread simulations):
    floating_index = rand(length(bond_id), 1) / 10;
    floating_spread = rand(length(bond_id), 1) / 10;
    
    %% Step #3 (Dataset creation):
    target_ds = dataset(bond_id, floating_index, floating_spread);

end

function [ row ] = findrow( cmn_id, needed_type, coupon_type, coupon_ind )
%FINDROW looks for rows numbers with given type of coupon.
%
%   [ row ] = findrow(coupon_type, needed_type, coupon_ind, cmn_id)
%   receives  list of bond unique id numbers, needed coupon type, coupon
%   types for all bonds, row numbers of needed bonds, and return the vector
%   of required rows.

    row = cmn_id( coupon_ind == find(~cellfun('isempty',strfind(coupon_type, needed_type))));
end