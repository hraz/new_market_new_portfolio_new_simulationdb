function [] = datasetwrite( ds, file, delimiter, top_header ) 
%DATASETWRITE Summary of this function goes here
%   
%   [] = datasetwrite( ds, file, delimiter, top_header) 
%   writes the dataset array to a tab-delimited text file, including variable
%   names (if present). 
%
%   Inputs:
%       ds - it is a dataset object specifying all exported data to the
%               file. See description about DATASET function in Statistic Toolbox.
%
%       file - it is a string value specifying the full path to source txt-file
%                that includes needed data.
%
%       delimiter - it is a string value specifying the delimiter between
%               columns. It can be any of ' ', '\t', ',', ';', '|' or their
%               corresponding string names 'space', 'tab', 'comma', 'semi', or
%               'bar'.  Default value is '\t'.
%
%       top_header - it is boolean value (1 or 0) specifying existence of header
%               row in given file. Default value is true.
%
%   Output:
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(2, 4, nargin));
    
    if (nargin < 4)
        top_header = true;
    end
    
    if (nargin < 3)
        delimiter = '\t';
    end
    
    %% Write given data into file:
    new_file = [file '.txt'];
    old_file = [file, '_old.txt'];
    if (exist(new_file, 'file'))
        copyfile(new_file, old_file);
    end
    export(ds, 'file', new_file, 'Delimiter', delimiter, 'WriteVarNames', top_header); 
    
end

