function [] = txtrebuilder( source_path, target_path )
%TXTREBUILDER restructure given txt files to txt files according to data model.
%
%   [] = txtrebuilder( source_path, target_path ) receives paths to source
%   and to target folders and rebuild the data structure from source file to
%   data structure according to data model.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Import external packages:
    import dal.txt.rebuild.*;
    import dal.txt.import.*;
    import dal.txt.tools.*;
    import dal.txt.export.*;
    
    %% Step #1 (Bond.txt file read parameters declaration):
    delimiter = '\t';
    with_header = true;

    source_file_name = {'bond', 'bindex'};
%     source_file_name = {'bindex'};
    for i = 1 : length(source_file_name)
        file = fullfile( source_path, [ source_file_name{i}, '.txt' ] );
        data_format = source_header.([source_file_name{i} '_data_format']);
    
        %% Step #2 (Read bond.txt to dataset):
        source_ds = read2dataset( file, data_format, delimiter, with_header );

        %% Step #3 (Remove data in Hebrew):
        [ source_ds ] = rmhebrew( source_ds );

        %% Step #4 (Rename headers in raw dataset):
        [ source_ds ] = dsrename( source_ds, source_header.([source_file_name{i} '_new_header']) );

        %% Step #5 (Creating target datasets):
        switch source_file_name{i}
            case 'bond'
                [ ds ] = bond_rebuilder( source_ds, source_path );
            case 'bindex'
                [ ds ] = bindex_rebuilder( source_ds );
        end
    
        %% Step #6 (Write txt files according to built datasets):
        file_name = fieldnames(ds);
        file_count = length(file_name);
        for file = 1 : file_count
            datasetwrite( ds.(file_name{file}), fullfile( target_path, file_name{file}), delimiter, with_header );
        end
        
    end % for
    
end

function [ ds ] = bond_rebuilder( source_ds, source_path )
    import dal.txt.rebuild.*; 

    [ ds.bond_group, group_ind ] = bond_group( source_ds, 'group' );
    [ ds.bond_sector, sector_ind ] = bond_sector( source_ds, 'sector' );
    [ ds.bond_issue, last_ind, old_ind, issue_id ] = bond_issue( source_ds, 'isin', ...
                                                                                source_header.bond_issue_header, ...
                                                                                target_header.bond_issue_header, ...
                                                                                group_ind, sector_ind );
    [ ds.bond_daily ] = bond_daily( source_ds, source_header.bond_daily_header, ...
                                                     target_header.bond_daily_header, old_ind );
    [ ds.bond_fixed_coupon, ds.bond_float_coupon ] = bond_coupon( source_ds,  'coupon_type', issue_id, last_ind );
    [ ds.bond_rank, ds.bond_rank_maalot, ds.bond_rank_midroog ] = bond_rank( source_ds, old_ind, source_path );
end

function [ ds ] = bindex_rebuilder( source_ds )
    import dal.txt.rebuild.*;
    
    [ ds.bond_index_group, group_ind ] = bindex_group(unique(source_ds.uuid));
    [ ds.bond_index_cmn, ~, old_ind ] = bindex_issue( source_ds, 'uuid', ...
                                                                                target_header.bindex_issue_header, group_ind );
    [ ds.bond_index_daily ] = bindex_daily( source_ds, source_header.bindex_daily_header, ...
                                                     target_header.bindex_daily_header, old_ind );
%      [ds.bond_index_cmp ] = bindex_cmp();

end