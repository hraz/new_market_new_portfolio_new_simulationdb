function [ conn, err_msg ] = mysql_conn( dbname )
%MYSQL_CONN create connection to mysql database.
%
%   [ conn, err_msg ] = mysql_conn( dbname )
%   receives database name as an optional input and return connection if it was 
%   created or empty array if it wasn't. The default database is market (not 
%   required input).
%
%   Input:
%       dbname - is a string specifying the database name like
%                'test_db'.
%
%   Output:
%       conn - is a database connection object.
%
%       err_msg - is a string specifying the error message in case of
%                      connection failed.

% Author: Yigal Ben Tal
% Copyright 2011-2013
% Updated by: Yigal Ben Tal, 31-01-2013
% Reason: Was added a possibility to change access port; were updated comments and
%               function documentation.
% Updated by: Yigal Ben Tal, 18-06-2013
% Reason: Were removed new not needed and not described code line.


    %% Import external packages:
    import dal.txt.import.*;
    
    %% Input validation:
    error(nargchk(0, 1, nargin));
    if nargin == 0
        dbname = 'market';
    end
        
    %% Default values declaration:
    Drv = 'com.mysql.jdbc.Driver';
    
    %% CONNECTION PROPERTIES ARE READED FROM DBCONN.CNF FILE %%
    if exist('dbconn.cnf', 'file') 
        
        conn_prop = read2cell( 'dbconn.cnf' , 'r',  'n', '', '%s%s%s%s%s', '\t' );
        
        switch dbname
            case 'market'
                Host = conn_prop.host{1};
                Port = conn_prop.port{1};
                DbName = conn_prop.database{1};
                Usr = conn_prop.user{1};
                Pwd = conn_prop.password{1};
            case 'portfolio'
                Host = conn_prop.host{2};
                Port = conn_prop.port{2};
                DbName = conn_prop.database{2};
                Usr = conn_prop.user{2};
                Pwd = conn_prop.password{2};
            case 'simulation'
                Host = conn_prop.host{4};
                Port = conn_prop.port{4};
                DbName = conn_prop.database{4};
                Usr = conn_prop.user{4};
                Pwd = conn_prop.password{4};
            otherwise
                error('dal_mysql_connection:mysql_conn:wrongInput', 'There is a wrong database name.');
        end
                    
    else
        error('dal_mysql_connection:mysql_conn:mismatchInput', 'There is no dbconn.cnf file in the working directory.');
    end
  
    URL = ['jdbc:mysql://' Host ':' Port '/' DbName];
    
    %% Step #1 (Set time allowed to establish database connection):
    logintimeout('com.mysql.jdbc.Driver', 5); %max allowed time = 5sec
    conn = database(DbName, Usr, Pwd, Drv, URL);

    %% Step #2 (Check that the database connection status is successful):
    if (~isconnection(conn))
        err_msg = ['Connection failed: ', conn.Message];
    else
        err_msg = '';
    end
      
end
