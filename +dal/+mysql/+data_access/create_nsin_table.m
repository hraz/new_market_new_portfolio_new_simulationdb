function [ err ] = create_nsin_table( nsin_list ,conn )
%CREATE_NSIN_TABLE Creating a table of nsin called tt_nsin_list in the data base to join
%   [ error ] = create_nsin_table( nsin_list ,conn ) - recieves an array nsin, creates a temporary table in the database and insert nsin into the
%   table
%   Input:
%      nsin_list - is an Nxnsin specifying nsin number
%      conn - is a connection to the database
%      
%   Output:
%              error - empty if the insert succeded or the error message if not
% 
% 05/07/2012
% Yaakov Rechtman

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(2, 2, nargin));  

    if(isempty(nsin_list))
          error('dal_mysql_data_access:create_nsin_table:wrongInput', ...
            'nsin_list can not be empty');
    end

    if ~isnumeric(nsin_list)
        error('dal_mysql_data_access:create_nsin_table:wrongInput', ...
            'wrong input for nsin_list');
    end
    
    %% Step #1 (SQL sqlquery for this filter):
    sql_query_drop = 'DROP TEMPORARY TABLE IF EXISTS tt_nsin_list';
    sqlquery_create = ' CREATE TEMPORARY TABLE tt_nsin_list(security_id INT PRIMARY KEY); ' ;
    sqlquery_insert = [ ' INSERT INTO tt_nsin_list VALUES ' num2sqlbracket(nsin_list) ';' ] ;
    
    %% Step #2 (Execution of built SQL sqlquery):
    err1= exec(conn, sql_query_drop);
    err2 = exec(conn, sqlquery_create);
    err3 = exec(conn, sqlquery_insert);
    
    %% Step #3 (Collect possible error messages):
    err = [err1.Message err2.Message err3.Message];

end

