import dal.market.filter.total.*;
 import utility.*;
  import utility.dal.*;
  
rdate=734830;
ds=dataset();

class_name =  {};
coupon_type = {'Fixed Interest'};
linkage_index = { 'NON LINKED'};
sector = {};
bond_type = {'Government Bond Shachar'};

ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,ds);

class_name =  {};
coupon_type = {'Fixed Interest'};
linkage_index = { 'NON LINKED'};
sector = {'COMMERCE'};
bond_type = {'Government Bond Shachar'};

ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,ds);

class_name =  {};
coupon_type = {'Fixed Interest'};
linkage_index = { 'NON LINKED'};
sector = {'UNKNOWN','GOVERNMENT'};
bond_type = {'Government Bond Shachar'};

ds = add2dsforfilterttl(class_name, coupon_type, linkage_index, sector,bond_type,ds);
% 
% dyn_param.history_length =  {[60]  [100000]};
% dyn_param.ytm = {[0]  [0.5000]};
% dyn_param.mod_dur =  {[2]  [7]};
%  dyn_param.turnover = {[100000]  [1.0000e+009]};

 dyn_param.history_length =  {60  100000};
dyn_param.ytm = {0  0.5000};
dyn_param.mod_dur =  {2  7};
 dyn_param.turnover = {100000  1.0000e+009};
 
 [ struct_nsin ] = bond_ttl_multi( rdate,ds, dyn_param )
    