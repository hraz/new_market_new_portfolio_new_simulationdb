 clear; clc;

import dal.market.filter.total.*;

rdate = '2012-02-02';


% dyn_param.history_length = {60 100000};
% dyn_param.yield = {0 0.5};
% dyn_param.mod_dur = {2 7};
%  dyn_param.turnover = {100000 1.0000e+009};
% dyn_param.ttm = {NaN NaN};
% % 
% % stat_param.class_name = {};
% % stat_param.coupon_type = {'Fixed Interest'};
% % stat_param.linkage_index = {'CPI'};
% % stat_param.sector = {};
% % stat_param.bond_type = {'Corporate Bond'};
% 
% stat_param.class_name = {};
stat_param.coupon_type = {'Variable Interest', 'Fixed Interest'};
% stat_param.linkage = {'CPI', 'NON LINKED'};
% stat_param.sector = {'COMMUNICATIONS & MEDIA'};
% stat_param.bond_type = {'Corporate Bond'};
% stat_param.redemption_sum = {};

stat_param.class_name = {'NON-GOVERNMENT BOND'};
% stat_param.coupon_type = {'Fixed Interest'};
stat_param.linkage = {'CPI','USD'};
stat_param.sector = {'COMMUNICATIONS & MEDIA'};
stat_param.bond_type = {'Corporate Bond'};
stat_param.redemption_error = {'error'};

dyn_param.history_length = {1 1000000};
dyn_param.yield = {0 0.5};
dyn_param.mod_dur = {0 7};
 dyn_param.turnover = {1000000 1.0000e+009};
dyn_param.ttm = {0 7};
% tic
%  [ nsin ] = bond_ttl( rdate,stat_param,dyn_param )
  [ nsin ] = bond_ttl( rdate,stat_param, dyn_param )
%   toc 
% stat_param.class_name = {};
% stat_param.coupon_type = {'Fixed Interest'};
% stat_param.linkage_index = {'NON LINKED'};
% stat_param.sector = {};
% stat_param.bond_type = {};



% dyn_param.history_length = {NaN NaN};
% dyn_param.yield = {NaN NaN};
% dyn_param.mod_dur = {NaN NaN};
% dyn_param.turnover = {NaN NaN};
