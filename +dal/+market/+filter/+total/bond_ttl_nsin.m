function [ nsin ] = bond_ttl_nsin( rdate,  stat_param, dyn_param , nsin_list)
%BOND_TTL return list of bonds that satisfy the input conditions.
%
%   [ nsin ] = bond_ttl( type, sector, linkage, coupon_type, rdate, varargin ) receives user defined
%   conditions like bond type, sector of industry, linkage and/or coupon_type and list of dynamic
%   data conditions, and  return list of nsin numbers of all bonds that satisfy to these conditions.
%
%   Input:
%       rdate - is a scalar value specifying the required date.
%
%   Optional static input as fileds of struct:
%       class_name - is an 1x1 cell-array of string specifying the possible bond class_name like:
%                       'GOVERNMENT BOND' or 'NON-GOVERNMENT BOND'
%
%       coupon_type - is an 1xNCOUPONTYPES cell array of strings specifying the list of bond
%                   coupon types. Possible values are:  
%                   'Unknown', 'Fixed Interest', 'Variable Interest' or 'Special' and its combinations.
%
%       linkage - is an 1xNLINKAGES cell array of strings specifying list of required linkage
%                   variations, based on exist in database linkage types. Possible values are:
%                   'CPI', 'USD', 'UKP', 'EURO' and its combinations.
%
%       sector - is an 1xNSECTORS cell array of strings specifying list of required bond sectors
%                   based on exist in database sectors. 
%
%       type - is an 1xNTYPES cell array of strings specifying list of required bond types based on
%                   exist in database types. 
%
%   Optional dynamic input as fileds of struct:
%       'history_length' - is an 1x2 vector specifying lower and upper bounds of history length.
%
%       'yield' - is an 1x2 vector specifying lower and upper bounds of ytm bruto.
%
%       'mod_dur' - is an 1x2 vector specifying lower and upper bounds of modified duration.
%
%       'turnover' - is an 1x2 vector specifying lower and upper bounds of turnover.
%
%       'ttm' - is an 1x2 cell array of strings specifying lower and upper bounds of time 2 maturity.
%
%   Output:
%       nsin - is an NBONDSx1 cell array specifying the  nsin numbers of bonds that are satisfied to
%                   all filter conditions . 
%
% Yigal Ben Tal
% Copyright 2011.
    
    %% Import external packages:
    import dal.mysql.connection.*;
     import dal.mysql.data_access.*;
    import dal.market.filter.static.*;
    import dal.market.filter.dynamic.*;
    import utility.dal.*;
       
    %% Input validation:
    error(nargchk(0, 4, nargin));
    
     if (nargin == 0) || isempty(rdate)
        rdate = datestr(today, 'yyyy-mm-dd');
    elseif isnumeric(rdate)
        rdate = datestr(rdate, 'yyyy-mm-dd');
     end
    
     %% Step #1 (Input parsing):
    p_stat = inputParser;    % Create an instance of the class_name:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p_stat.addParamValue('class_name', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('coupon_type', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('linkage_index', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('sector', {}, @(x)(iscellstr(x) || iscell(x)));
    p_stat.addParamValue('bond_type', {}, @(x)(iscellstr(x) || iscell(x)));
    
    % Parse method to parse and validate the inputs:
    p_stat.StructExpand = true; % for input as a structure
    p_stat.KeepUnmatched = true;
    p_stat.parse(stat_param);

    %% Step #2 (MATLAB puts the results of the parse into a property named Results):
    input_stat = p_stat.Results;
    var_stat = p_stat.Parameters;
    num_vars_stat = numel(var_stat);
        
    %% Step #3 (Fill the missing parameters (empty values)):
    for i = 1 : num_vars_stat
        if isempty(input_stat.(var_stat{i})) || all(cell2mat(cellfun(@(x)isempty(x),input_stat.(var_stat{i}), 'UniformOutput', false)))
            input_stat.(var_stat{i}) = 'NULL';
        else
            input_stat.(var_stat{i}) = cell2str4sql( input_stat.(var_stat{i}));
        end
    end
    
  p_dyn = inputParser;    % Create an instance of the class:
    
    % Parameter pairs parsing declaration - p.addParamValue(name, default, validator):
    p_dyn.addParamValue('history_length', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('yield', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('mod_dur', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('turnover', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    p_dyn.addParamValue('ttm', {NaN, NaN}, @(x)(~isempty(x) && all(cellfun(@(x)isnumeric(x),x)) && all(size(x) == [1,2])));
    

    % Parse method to parse and validate the inputs:
    p_dyn.StructExpand = true; % for input as a structure
    p_dyn.KeepUnmatched = true;
    p_dyn.parse(dyn_param);
    try
         p_dyn.parse(dyn_param);
    catch ME
        newME = MException('dal_filter_dynamic:bond_dynamic_complex:optionalInputError',...
                'Error in input arguments');
        newME = addCause(newME,ME);
        throw(newME)
    end

    %% Step #2 (MATLAB puts the results of the parse into a property named Results):
    input_dyn = p_dyn.Results;
    var_dyn = p_dyn.Parameters;
    num_vars_dyn = numel(var_dyn);
    
    %% Step #3 (Fill the missing '' (empty values or NaN)):
    for i = 1 : num_vars_dyn
               emp_ind = cellfun(@(x)isempty(x), input_dyn.(var_dyn{i}));
               
               nan_ind = cellfun(@(x)isnan(x), input_dyn.(var_dyn{i}), 'UniformOutput', false);
               nan_ind(cellfun(@(x)isempty(x), nan_ind)) = {false};
               nan_ind = cell2mat(nan_ind);

               input_dyn.(var_dyn{i})(emp_ind | nan_ind) = {'NULL'};
               
               num_ind = cellfun(@(x)isnumeric(x), input_dyn.(var_dyn{i}), 'UniformOutput', true);
               input_dyn.(var_dyn{i})(num_ind) = cellfun(@(x)num2str(x), input_dyn.(var_dyn{i})(num_ind), 'UniformOutput', false); 
    end
    
    
     %% Step #4 (SQL query for this filter):
    sqlquery = ['CALL sp_fa_bond_ttl(''', rdate, ''', CONCAT(', input_stat.class_name, ...
                                                            '), CONCAT(', input_stat.coupon_type, ...
                                                            '), CONCAT(', input_stat.linkage_index, ...
                                                            '), CONCAT(', input_stat.sector, ...
                                                            '),CONCAT(', input_stat.bond_type, '),'...
                                                        input_dyn.history_length{1} ', ' input_dyn.history_length{2} ', ' ...
                                                        input_dyn.mod_dur{1} ', ' input_dyn.mod_dur{2} ', ' ...
                                                        input_dyn.turnover{1} ', ' input_dyn.turnover{2} ', ' ...
                                                        input_dyn.yield{1} ', ' input_dyn.yield{2} ', ' ...
                                                        input_dyn.ttm{1} ', ' input_dyn.ttm{2} ');'
                                                        ];
                                                    
    
    %% Step # 5 (Execution of built SQL query):
    setdbprefs ('DataReturnFormat','cellarray');
    conn = mysql_conn();
    
    %     checking if the input consists of nsin to filter
        if(nargin == 4)
          create_nsin_table(nsin_list,conn);
     end
    nsin = fetch(conn, sqlquery);
    
    %% Step #6 (Close the opened connection):
    close(conn);
    setdbprefs ('DataReturnFormat','dataset');                                                     
    
    %% Step #7 (Convert cell-array to numeric vector):
    nsin = cell2mat(nsin);
    
end

