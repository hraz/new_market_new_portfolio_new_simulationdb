%% Import external packages:
import dal.market.test.get_dynamic_data.*;
import utility.dal.*;


%% Test description:
funName = 'get_dynamic_data';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

start_date = '2005-01-01';
end_date = '2005-01-10';
nsin = [1081777,1081223];
data_name ={'ytm','price_dirty'};
tbl_name = 'bond';
obs_count = 5;

res1 = dataset( [ 
{'2005-01-04'}
{'2005-01-05'}
{'2005-01-06'}
{'2005-01-09'}
{'2005-01-10'}

],'VarNames',{'date'});
res1 =[res1, dataset( [ 
0.051916
0.053569
0.053628
0.053002
0.052464
],'VarNames',{'id1081777'}) ] ;

res1 =[res1, dataset( [ 
0.046108
0.046221
0.046335
0.046679
0.046795
],'VarNames',{'id1081223'}) ] ;

res2 = dataset( [ 
{'2005-01-04'}
{'2005-01-05'}
{'2005-01-06'}
{'2005-01-09'}
{'2005-01-10'}


],'VarNames',{'date'});
res2 =[res2, dataset( [ 
115.2300000
115.2300000
115.2300000
115.2300000
115.2300000
],'VarNames',{'id1081777'}) ] ;

res2 =[res2, dataset( [ 
128.4000000
128.4000000
128.4000000
128.4000000
128.4000000
],'VarNames',{'id1081223'}) ] ;

 struct_of_fts.(data_name{1}) = dataset2fts( res1, data_name(1) );
 struct_of_fts.(data_name{2}) = dataset2fts( res2, data_name(2) );

 test_script( funName, testDesc, expectedError, struct_of_fts,  nsin, data_name, tbl_name, start_date, end_date, obs_count )
