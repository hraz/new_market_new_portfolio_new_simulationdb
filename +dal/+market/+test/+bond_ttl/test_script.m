function [] = test_script( funName, testDesc, expectedError, expectedResult, rdate,  stat_param, dyn_param)
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.market.filter.total.*;
    import dal.market.test.*;
    
    if isempty(expectedError)
        %% Test execution:
       nsin = bond_ttl(rdate,stat_param,dyn_param);


        %% Result analysis:
        if (isequal(nsin,expectedResult))
            testResult = 'Success';
        else
            testResult = 'Failure';
        end
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
