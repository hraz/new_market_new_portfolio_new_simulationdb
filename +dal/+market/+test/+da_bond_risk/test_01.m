%% Import external packages:
import dal.market.test.get_bond_risk;


%% Test description:
funName = 'bond_ttl';
testDesc = 'Correct arguments. -> result set:';

expectedStatus = 1;
expectedError = '';

nsin = [1060045 1081777];

rdate = '2012-01-01';
ds1 = dataset([1060045
                            1081777],'VarNames',{'security_id'});
ds2 = dataset([{'2007-05-27'}
                            {'2004-12-23'}],'VarNames',{ 'last_update_maalot'});   
ds3 = dataset([{'NR'}
                            {'NR'}],'VarNames',{' rating_maalot'});   
ds4 = dataset([{'2007-06-04'}
                            {'1998-03-26'}],'VarNames',{'last_update_midroog '});              
ds5 = dataset([{'NVR'}
                            {'NVR'}],'VarNames',{'rating_midroog'});                           
                        
expectedResult = [ds1,ds2,ds3,ds4,ds5];
%% Test execution:
test_script( funName, testDesc, expectedError, expectedResult, rdate, nsin );
