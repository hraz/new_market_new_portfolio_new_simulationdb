%% Import external packages:
    import utility.dal.*;
    import dal.market.test.get_cash_flow.*;


%% Test description:
funName = 'get_cash_flow';
testDesc = 'Correct arguments. -> result set:';
expectedStatus = 1;
expectedError = '';

start_date = '2010-01-01';
end_date = '2012-01-01';
nsin = [1060045,1060078];


expectedResult = dataset( [ 
{'2010-11-30'		}
{'2011-05-31'	}
],'VarNames',{'date'});
expectedResult =[expectedResult, dataset( [ 
	3.65564825471707	
	42.4687374291245
],'VarNames',{'id1060045'}) ] ;

expectedResult =[expectedResult, dataset( [ 
NaN
3.97963910179261
],'VarNames',{'id1060078'}) ] ;
expectedResult = dataset2fts( expectedResult, 'cash_flow' );
%% Test execution:
 test_script( funName, testDesc, expectedError, expectedResult, start_date,end_date,nsin );
