function [ ds ] = get_makam_shachar_yield( )
 %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    sqlquery = ['select * from tase.makam_shachar '];
   
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn();
    ds = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    %% Step #4 (Transform dataset to fts):
    
end

