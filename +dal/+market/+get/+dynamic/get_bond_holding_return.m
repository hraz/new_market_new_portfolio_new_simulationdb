function [ cumulative_holding_ret, holding_ret, holding_income, stop_point ] = get_bond_holding_return( allocation, first_date, last_date, ir_lb, ir_ub, sim_freq) 
%GET_BOND_HOLDING_RETURN calculates the holding return of the given assets in required period.
%
%   [ holding_ret ] = get_bond_holding_return( allocation, first_date, last_date, sim_freq )
%   receives list of given assets and bounds of the required time period, and returns holding
%   returns of the given assets in required period depending on required frequency.
%
%   Input:
%       allocation - is an NASSETSx2 dataset represented portfolio allcation: [nsin, weight].
%
%       first_date - is a scalar numeric value specifying the lower bound of the required holding
%                        period.
%
%       last_date -  is a scalar numeric value specifying the upper bound of the required holding
%                        period.
%
%       ir_lb - is a scalar value represented the lower bound of the possible interest rate for
%                       reinvestment of given coupon payments. Default = 0;
%
%       ir_ub - is a scalar value represented the upper bound of the possible interest rate for
%                       reinvestment of given coupon payments. Default = 1;
%
%   Optional Input:
%       sim_freq - is a scalar numerical value specifying the number of stop-points per each year from
%                       required holding period. These stop-points will used for calculation of
%                       middle holding performance ratios. Default is [], it means no any
%                       stop-points, and the result will be NOBSERVATIONSx(NASSETS+1) matrix .
%
%   Output:
%       cumulative_holding_ret - is an NOBSERVATIONSx(NASSETS+1)xNSTOPPOINTS(+1) matrix where the first
%                       dimention specifying the existed trading dates in required holding period;
%                       the second dimention specifying list of the holded assets, and the last,
%                       third dimention specifying the number of stop-points. The formula for its
%                       calculation is: 
%      / dirty_price(t0+dt) + comp_coupon(dt) + sum(redemtion(dt)) + discounted_last_pmnt_after_last_date \ ^ (1/dt)
%     |---------------------------------------------------------------------------------------------------------|
%      \                                                              dirty_price(t0)                                                                       /
%
%       holding_ret - is an NOBSERVATIONSx(NASSETS+1)xNSTOPPOINTS(+1) matrix where the first
%                       dimention specifying the existed trading dates in required holding period;
%                       the second dimention specifying list of the holded assets, and the last,
%                       third dimention specifying the number of stop-points.The formula for its
%                       calculation is: holding_ret = cumulative_holding_ret - 1.
%
%       holding_income - is an 1xNASSETS vector specifying the total sum accumulated for each given
%                       allocated asset include coupon reinvestment and all reinvested to risk free
%                       income in case of early end of trading. 
%
%       stop_point - is a NPOINTSx1 vector specifying the stop-point date for performance valuation.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Import external packages:
    import dal.market.get.cash_flow.*;
    import dal.market.get.dynamic.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(5,6, nargin));
    if nargin < 6 | sim_freq == 0
        sim_freq = [];
    end
 
    %% Step #1 (Get cumulative income):
    [ income_data, stop_point ] = get_bond_cumulative_income(allocation, first_date, last_date, ir_lb, ir_ub, sim_freq);
    
    %% Step #2 (Build holding return in required period): 
    if ~isempty(income_data)
        count = allocation.count';
        port_income = squeeze(sum(income_data(:,2:end,:) .* count(ones(size(income_data,1),1), :, ones(size(income_data, 3),1)),2));
        holding_ret = NaN(size(port_income, 1), 2, size(port_income,2));
        holding_ret(:,1,:) = income_data(:,1,:);
        cumulative_holding_ret = NaN(size(port_income, 1), 2, size(port_income,2));
        cumulative_holding_ret(:,1,:) = income_data(:,1,:);
        if ~isempty(sim_freq)
            init_investment = port_income (1,:);
            holding_ret(:,2,:) = ( (port_income ./ init_investment(ones(size(port_income))) ) - 1); % ./ time_period(:, ones(length(allocation.nsin),1), ones(length(stop_point),1)) ); % .^ (1./ time_period(:, ones(length(allocation.nsin),1), ones(length(stop_point),1)));
            holding_income = port_income(end,end);
            cumulative_holding_ret(:,2,:) = holding_ret(:,2,:) + 1;
        else
            init_investment = port_income(1);
            holding_ret(:,2:end) =  ( (port_income / init_investment) - 1); % ./ time_period(:, ones(length(allocation.nsin),1), ones(length(stop_point),1)) ); % .^ (1./ time_period(:, ones(length(allocation.nsin),1)));
            holding_income = round(port_income(end) * 100) / 100;
            cumulative_holding_ret(:,2:end) = holding_ret(:,2:end) + 1;
        end
    else
        cumulative_holding_ret = [];
        holding_ret = [];
        holding_income = [];
        stop_point = [];
    end
end
