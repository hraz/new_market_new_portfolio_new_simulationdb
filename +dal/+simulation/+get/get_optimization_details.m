function [ details ] = get_optimization_details( errFlag )
%GET_OPTIMIZATION_DETAILS  returns solver data according to errFlag
%   [ details ] = get_optimization_details( errFlag )
%
%   Input:
%       errFlag - is an INTEGER value specifying some error flag. 
%                       If input is empty the function returns full list of saved data.
%
%   Output:
%       details -  it is a  nX15 dataset specifying data about the solver
%                             simID - is an INTEGER scalar specifying the number of the simulation.
%
%                             portfolioDesc - is a STRING value specifying the simulated portfolio description.
%
%                             callNum -  is an INTEGER value specifying the number of call that was made to any of 
%                                               the solver function in the current portfolio.
%
%                             errFlag - is an INTEGER value specifying the matlab exit flag (positive - solver was 
%                                               successful, negative - solver faild)
%
%                             iterations - is an INTEGER value specifying the number of iteration done by the solver.
%
%                             constrViolation - is a DOUBLE value specifying the size of constraint violation.
%
%                             algo - is a STRING value specifying the name of used algorithm in the current solver.
%
%                             cgIterations -  is a DOUBLE value specifying the total value of PCG iterations.
%
%                             msg - is a STRING value specifying the exit message.
%
%                             firstOrderOpt - is a DOUBLE value specifying the measure of first order optimality.
%
%                             firstExcessDuration - is a DOUBLE value specifying some unknown parameter.
%
%                             secondExcessDuration - is a STRING value specifying some unknown parameter.
%
%                             infeasibleNonDurationConstStart - is a BOOLEAN value specifying which indicates 
%                                               whether the initial weights are infeasible in the sense that one of the 
%                                               non-duration inequality constraints doesn't hold. 
%
%                             infeasibleDurationConstStart - is a BOOLEAN value specifying the same as above, 
%                                               for the duration constraints.
%
%                             infeasibleNonDurationConstFinish - is a BOOLEAN value specifyin the same as above, 
%                                               for the final weights returned from the solver
%
%                             infeasibleDurationConstFinish - is a BOOLEAN value specifying the same as above, 
%                                               for the final weights from the solver
%
% Created by Yaakov Rechtman.
% Date:		20.06.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
%                at: 16.07.2013. 
% The sense of update: removing data about the simulation into independent database.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    try
        error(nargchk(0, 1, nargin));
        if (nargin == 0) | (isempty(errFlag))
            errFlag = 'NULL';
        elseif isnumeric(errFlag)
            errFlag = num2str(errFlag);
        end
    
         %% Step #1 (SQL sqlquery for this filter):
         sqlquery = ['CALL simulation.get_optimization_details(', errFlag ,');' ];

        %% Step #2 (Create a connection):
        setdbprefs ('DataReturnFormat','dataset');
        [conn, errFlag] = mysql_conn('simulation');
        if ~isempty(errFlag)
            error('dal_simulation_get:get_optimization_details:connectionFailure', errFlag);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Execution of built SQL sqlquery):
        details = fetch(conn,sqlquery);
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_get:get_optimization_details:connectionFailure', ME.message);
    end
    
end
