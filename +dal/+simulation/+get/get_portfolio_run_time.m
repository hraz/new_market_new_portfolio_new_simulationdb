function [ runTimeList ] = get_portfolio_run_time( portfolioDescList )
%GET_PORTFOLIO_RUN_TIME return running time of the given portfolios.
%
%   [ runTimeList ] = get_portfolio_run_time( portfolioDescList )
%   return the list of running time for the given portfolios.
%
%   Input:
%       portfolioDescList - is a NSIMULATIONSx1 cell of strings specifying 
%                                       the portfolio descriptions' list.
%                                       In case of the empty input will be return all saved portfolios.
%
%   Output:
%       runTimeList - is a DATASET specifying the list of running time for 
%                                       the given portfolios with follow fields:
% 
%           portfolioDesc - is a string value describes some portfolio.
%
%           runTime - is a TIMESTAMP in seconds of the portfolio running time.

% Created by: Yigal Ben Tal
%                    at: 08.09.2013
% Copyright 2013, BondIT Ltd.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.cell2str4sql;

    try
        %% Input validation:
        error(nargchk(1, 1, nargin));   
        
        if isempty (portfolioDescList)
            portfolioDescList = '"NULL"';
        elseif ~iscellstr(portfolioDescList)
            error('dal_simulation_get:get_simulation_run_time:mismatchInput',...
                'The given simulation list must include just numerical values.');
        else
            portfolioDescList = ['"', cell2str4sql(portfolioDescList), '"'];
        end
        
        %% Step #1 (Create sql query):
        sqlquery = ['CALL simulation.get_portfolio_run_time(', portfolioDescList, ')'];

        %% Step #2 (Execution of built SQL query):
        setdbprefs ('DataReturnFormat','dataset');                                                     
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_get:get_portfolio_run_time:connectionFailure', errMsg);
        end

        %% Step #3 (Execution of built SQL sqlquery):
        runTimeList = fetch(conn, sqlquery);
       
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
        
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_get:get_portfolio_run_time:connectionFailure', ME.message);
    end
end