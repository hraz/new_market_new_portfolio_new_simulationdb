function [ detailCount ] = get_optimization_detail_count(  )
%GET_OPTIMIZATION_DETAIL_COUNT return for all saved error flags its own count.
%
%   [ detailCount ] = get_optimization_detail_count(  )
%   returns a full list of errorFlag values with its number of repetitions.
%
%   Input:
%       None.
%
%   Output:
%       detailCount - is a dataset value includes two follow fields:
%           errorFlag	- is an INTEGER value specifying some error flag.
%
%           errorFlagCount - is an INTEGER value specifying the  number of events 
%                                         with the error flag above.

%	Created by: Yaakov Rechtman,
%                       at: 28.10.2012
% 	Copyright 2012-2013, BondIT Ltd.	
% 
% 	Updated by:	Yigal Ben-Tal, 
%                       at: 08.09.2013. 
%	The sense of update: removing the simulation monitoring to 
%						 independent database.

%% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    try
        error(nargchk(0, 0, nargin));
        
         %% Step #1 (SQL sqlquery for this filter):
         sqlquery = 'CALL simulation.get_optimization_detail_count();';

        %% Step #2 (Create a connection):
        setdbprefs ('DataReturnFormat','dataset');
        [conn, errFlag] = mysql_conn('simulation');
        if ~isempty(errFlag)
            error('dal_simulation_get:get_optimization_detail_count:connectionFailure', errFlag);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Execution of built SQL sqlquery):
        detailCount = fetch(conn,sqlquery);
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
        error('dal_simulation_get:get_optimization_detail_count:connectionFailure', ME.message);
    end
    
end