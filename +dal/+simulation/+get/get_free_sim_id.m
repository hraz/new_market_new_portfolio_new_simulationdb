function [ simID ] = get_free_sim_id( minID, maxID )
%GET_SIM_ID get simulation id which has not done yet in given range.
%   
%  [ sim_id ] = get_free_sim_id( minID, maxID )
%   returns simulation id which has not been done yet between max and min sim_id.
% 
%    Input:
%           minID - is an INTEGER value specifying the minimum simulation id on which to get the sim id.
% 
%           maxID - is an INTEGER value specifying the maximum simulation id on which to get the sim id.
%     
%   Output:
%              simID - is an INTEGER specifying the sim_id which has not been done yet.    
%
% Created by: Yaakov Rechtman.
%                    at: 10.09.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
%                    at: 08.09.2013. 
% The sense of update: removing data about the simulation into independent database.

    %% Import external packages:
    import dal.mysql.connection.*;

    try
        %% Input validation:
        error(nargchk(2, 2, nargin));   
        
        if isempty (minID)
            error('dal_simulation_get:get_free_sim_id:mismatchInput',...
                'The given lower bound cannot be an empty.');
        elseif ~isnumeric(minID)
            error('dal_simulation_get:get_free_sim_id:mismatchInput',...
                'The given lower bound must be numerical value.');
        else
            minID = num2str(minID);
        end
        
        if isempty (maxID)
            error('dal_simulation_get:get_free_sim_id:mismatchInput',...
                'The given lower bound cannot be an empty.');
        elseif ~isnumeric(maxID)
            error('dal_simulation_get:get_free_sim_id:mismatchInput',...
                'The given lower bound must be numerical value.');
        else
            maxID = num2str(maxID);
        end
        
        %% Step #1 (Execution of sql function):
        sqlquery = ['CALL simulation.get_free_sim_id(', minID, ', ', maxID ');'];

        %% Step #2 (Execution of built SQL query):
        setdbprefs ('DataReturnFormat','numeric');                                                     
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_get:get_free_sim_id:connectionFailure', errMsg);
        end

        %% Step #3 (Execution of built SQL sqlquery):
        simID = fetch(conn, sqlquery);
       
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');  
        end
        
    catch ME
         %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
            setdbprefs ('DataReturnFormat','dataset');  
        end 
        error('dal_simulation_get:get_free_sim_id:connectionFailure', ME.message);
    end
    
end