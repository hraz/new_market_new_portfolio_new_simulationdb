%% Import tested directory:
import dal.simulation.test.t01_get_free_sim_id.*;

%% Test description:
funName = 'get_free_sim_id';
testDesc = 'Get free sim id -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';

%% Input definition:
minID = 1;
maxID = 100;

prediction = 1

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, minID, maxID, prediction);
