function [result] = test_script( funName, testDesc, expectedStatus, expectedError, minID, maxID )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.simulation.get.*
    import dal.simulation.test.*;
    
    %% Test execution:
    [status, errMsg] = get_free_sim_id( minID, maxID );

    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
%     save_test_result(result);

end
