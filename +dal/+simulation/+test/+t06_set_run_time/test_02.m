%% Import tested directory:
import dal.simulation.test.t06_set_run_time.*;

%% Test description:
funName = 'set_run_time';
testDesc = 'Empty portfolio ID. -> error message:';

%% Expected results:
expectedStatus = 0;
expectedError = 'inPortfolioID cannot be an empty.';

%% Input definition:
portfolioID = [];
runTime = 0.2346546;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portfolioID, runTime );
