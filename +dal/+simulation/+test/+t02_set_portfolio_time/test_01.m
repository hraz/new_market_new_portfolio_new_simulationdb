%% Import tested directory:
import dal.simulation.test.t02_get_sim_err.*;

%% Test description:
funName = 'get_sim_errors';
testDesc = 'Right input of the error message constraint: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';
expectedResult = [dataset({2, 'portfolioID'}), dataset({{'2010-07-01 14:12:12.0'}, 'obsDate'}), ...
                                dataset({{'Test error 2', 'Test description of the error 2.'}, ...
                                'errMsg', 'errDescription'})];

%% Input definition:
errMsg = 'Test error 2';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, errMsg );
