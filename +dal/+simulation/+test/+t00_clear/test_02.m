%% Import tested directory:
import dal.simulation.test.t00_clear.*;

%% Test description:
funName = 'clear';
testDesc = 'Clear simulation database without pre-defined simulation number -> empty result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';

%% Input definition:
newSimNum = 5;
username = 'Yigal';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, newSimNum, username);
