%% Import tested directory:
import dal.simulation.test.t06_set_run_time.*;

%% Test description:
funName = 'set_run_time';
testDesc = 'Set run time for existed portfolio ID: -> result set:';

%% Expected results:
expectedStatus = 1;
expectedError = '';

%% Input definition:
portfolioID = 1;
runTime = 0.2346546;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, portfolioID, runTime );
