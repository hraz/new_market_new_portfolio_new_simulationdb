function [outStatus, outException] = set_error( simID, portfolioDesc, errorTime, errorMsg, errorDesc )
%SET_ERROR insert error details into the simulation database.
%
%   [outStatus,outException]  = set_error( errorTime, portfolioDesc , errorDetails )
%   recives error messages and insert them to the database
%   according to errorTime and portfolioDesc.
%
%   Input:
%       simID - is an INTEGER value specifying the current simulation.
%
%       portfolioDesc - is a STRING value specifying the simulated portfolio description.
% 
%       errorTime - is a DATETIME specifying the time when the error occured.
%
%       errorMsg - is a STRING value specifying the given error message.
%
%       errorDesc - is a STRING specifying the more detailed error description.
%
%   Output:
%       outStatus - is a BOOLEAN value specifying if the insert was successful.
%
%       outException - is a STRING value specifying error message if the
%                        insert was unsuccessful, null otherwise.
%
% Created by: Yaakov Rechtman.
%                     at: 06.08.2012
% Copyright 2012-2013, BondIT Ltd.	
% 
% Updated by: Yigal Ben Tal
%                     at: 08.09.2013
% The sense of update: Migration of the simulation monitoring to the 
% independent database.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;

    try
        %% Input validation:
        error(nargchk(5, 5, nargin));

        if isempty(simID)
            error('dal_simulation_set:set_error:mismatchInput', ...
                'simID cannot be an empty.');
        elseif ~isnumeric(simID)
            error('dal_simulation_set:set_error:wrongFormat', ...
                'simID must be an integer number.');
        else
            simID = num2str(simID);
        end
        
        if(isempty(portfolioDesc))
            error('dal_simulation_set:set_error:mismatchInput', ...
                'portfolioDesc cannot be an empty.');
        elseif ~ischar(portfolioDesc)
            error('dal_simulation_set:set_error:wrongFormat', ...
                'portfolioDesc must be a string.');
        else
            portfolioDesc = ['"', portfolioDesc, '"'];
        end
        
        if(isempty(errorTime))
            error('dal_simulation_set:set_error:mismatchInput', ...
                'errorTime cannot be an empty.');
        elseif isnumeric(errorTime)
            errorTime = datestr(errorTime, 'yyyy-mm-dd HH:MM:SS');
        else
            errorTime = ['"', errorTime, '"'];
        end

        if isempty(errorMsg)
            error('dal_simulation_set:set_error:mismatchInput', ...
                'errorMsg cannot be an empty.');
        elseif ~ischar(errorMsg)
            error('dal_simulation_set:set_error:wrongFormat', ...
                'errorMsg must be a string.');
        else
            errorMsg = ['"', errorMsg, '"'];
        end

        if isempty(errorDesc)
            error('dal_simulation_set:set_error:mismatchInput', ...
                'errorDesc cannot be an empty.');
        elseif ~ischar(errorDesc)
            error('dal_simulation_set:set_error:wrongFormat', ...
                'errorDesc must be a string.');
        else
            errorDesc = ['"', errorDesc, '"'];
        end

        outException = '';
        
       %% Step #1 (Create sql query):
       sqlquery = ['CALL simulation.set_error(', simID, ',', portfolioDesc, ',', errorTime, ',', errorMsg, ',', errorDesc, ');'];
   
       %% Step #2 (Create connection to database):
       setdbprefs ('DataReturnFormat','cellarray');
       [conn, outExceptionMsg] = mysql_conn('simulation');
        if ~isempty(outExceptionMsg)
            error('dal_simulation_set:set_error:connectionFailure', outExceptionMsg);
        else
            set(conn,'AutoCommit','off');    
        end

       %% Step #3 (Insert data into database):
        fetch(conn,sqlquery);
       if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Error details are saved.'];
            end
       end
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
    
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    end
    setdbprefs ('DataReturnFormat','dataset');
end
