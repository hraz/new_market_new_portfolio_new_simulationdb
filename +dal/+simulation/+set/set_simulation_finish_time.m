function [ outStatus, outException ] = set_simulation_finish_time( simId )
%SET_SIMULATION_FINISH_TIME save the finish time of the given simulation.
%
%   [ outStatus, outException ] = set_simulation_finish_time( simId )
%   save for the given simulation id its finish time.
%
%   Input:
%       simId - is an INTEGER value specifying the given simulation ID.
%
%   Output:
%       outStatus - is a boolean value specifying if the insert was successful.
% 
%       outException - is a STRING value specifying error message.
%
%   Note: This function must be used in any case - simulation assumed or error occures!

% Created by: Yigal Ben Tal,
%                     at: 08.09.2013
% Copyright 2013, BondIT Ltd.	
% 
% Updated by: ______________
%                    at: ______________
% The sense of update: _____________________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;

    %% Input validation:
    try
        error(nargchk(1, 1, nargin));

        if (isempty(simId))
            error('dal_simulation_set:set_simulation_finish_time:mismatvhInput',...
                'simId cannot be an empty.');
        elseif ~isnumeric(simId)
            error('dal_simulation_set:set_simulation_finish_time:wrongFormat',...
                'simId must be a numeric value.');
        else
            simId = num2str(simId);
        end

        outException = '';
        
        %% Step #1 (Create sql query):
        sqlquery = ['CALL simulation.set_simulation_finish_time(', simId, ')';];

        %% Step #2 (Create connection to database):
        setdbprefs ('DataReturnFormat','cellarray');
        [conn, errMsg] = mysql_conn('simulation');
        if ~isempty(errMsg)
            error('dal_simulation_set:set_simulation_finish_time:connectionFailure', errMsg);
        else
            set(conn,'AutoCommit','off');    
        end

        %% Step #3 (Insert data into database):
        fetch(conn,sqlquery);
       if (nargout > 0) 
            outStatus = true;
            if (nargout > 1)
                outException = [outException, 'Simulation finish time is saved.'];
            end
       end
        
        %% Step #4.1 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end
    
    catch ME
        outException = ME.message;
        outStatus = false;
        
        %% Step #4.2 (Close the opened connection):
        if exist('conn', 'var')
            close(conn);
        end 
    end
    setdbprefs ('DataReturnFormat','dataset');
end
