function [outStatus, outException]  = set_statistics(inPortfolioID, inCalcDate, inRiskMeasures, inPerformanceMeasures, inPerBondVaRStatistics, inBenchmarkStatistics, inExcessStatistics)
%SET_STATISTICS save portfolio statistics.
%
%   [outStatus, outException]  = set_statistics(inPortfolioID, inCalcDate,  inRiskMeasures, inPerformanceMeasures,
%                                                                   inPerBondVaRStatistics, inBenchmarkStatistics, inExcessStatistics) 
%
%	Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. 
%
%       inCalcDate - is a scalar value specifying the insert data date.
%
%       inRiskMeasures - is a dataset specifying portfolio risk parameters and must have follow fields:
%           - expectedReturn - is a scalar value specifying portfolio expected return.
%
%           - expectedVolatility - is a scalar value specifying portfolio expected volatility.
%
%           - expReturnCalcMethod - is a scalar  value specifying the expected return calculation method ID. The description
%                                   of all possible calculation methods may be gotten using get_calc_method(�expRet�) procedure. 
%
%           - expVolatilityCalcMethod - is a scalar value specifying the expected volatility calculation method ID. The
%                                   description of all possible calculation methods may be gotten using
%                                   get_calc_method(�expVol�) procedure.  
%
%           - valueAtRisk - is a scalar value represented Value-at-Risk of the given portfolio.
%
%           - cVaR - is a scalar value represented Conditional Value-at-Risk of the given portfolio.
%        
%        inPerformanceMeasures - is a dataset specifying portfolio performance indicators and must have follow fields: 
%           - holdingReturn - is a scalar value specifying the percent of the holding income vs. investment amount at
%                                   the given time point. This value must take to account all investment amount that
%                                   were done from portfolio creation date until given date and current market portfolio
%                                   value together with free cash amount the portfolio has.   
%
%           - moneyWeightedReturn - is a scalar  value specifying the money weighted portfolio return.
%
%           - timeWeigthedReturn - is a scalar value specifying the time weighted portfolio return.
%
%           - volatility - is a scalar  value specifying the portfolio volatility of holding return.
%
%           - downsideVolatility - is a scalar  value specifying the downside part of the portfolio holding return volatility.
%
%           - skewness - is a scalar value specifying the third moment of the portfolio holding return at given date.
%
%           - kurtosis - is a scalar value specifying the forth moment of the portfolio holding return at given date.
%
%           - alpha - is a scalar value specifying the portfolio alpha vs. current risk free market rate (See CAPM model).
%
%           - beta - is a scalar r value specifying the portfolio beta vs. some given benchmark (See CAPM model).
%
%           - uniqueRisk - is a scalar  value specifying undiversified (specific) risk of the portfolio (See CAPM model).
%
%           - sharpe - is a scalar value represented portfolio Sharpe ratio value.
%
%           - jensen - is a scalar value represented portfolio Jensen ratio value.
%
%           - omega - is a scalar value represented portfolio Omega ratio value.
%
%           - infRatio - is a scalar value represented Information ratio value
%
%           - rsquareVsBench - is a scalar value represented R-Square ratio value of the portfolio vs. some portfolio
%                                   benchmark. 
%
%           - rmseVsBench - is a scalar value specifying the Route-Mean-Square_Error value of the portfolio vs. some
%                                   portfolio benchmark. 
%        
%        inPerBondVaRStatistics - is a dataset specifying the contribution of each asset portfolio Value-at-Risk and
%                                   must have follow fields: 
%           - securityID - is a column vector specifying the some security ID number.
%
%           - marginalVaR - is a column vector specifying the marginal Value-at-Risk value of the given asset in the
%                                   portfolio at given time point. 
%
%           - componentVaR - is a column vector specifying the component Value-at-Risk value of the given asset in the
%                                   portfolio at given time point. 
%        
%        inBenchmarkStatistics - is a dataset specifying benchmark performance indicators in the same with given
%                                   portfolio period, and must have follow fields: 
%           - benchmarkID - is a scalar value specifying the given benchmark ID number. 
%
%           - holdingReturn - is a scalar value specifying the benchmark�s holding return.
%
%           - volatility - is a scalar  value specifying the benchmark�s volatility of holding return.
%
%           - skewness - is a scalar value specifying the third moment of the benchmark�s holding return at a given date.
%
%           - kurtosis - is a scalar value specifying the forth moment of the benchmark�s holding return at a given date.
%
%           - sharpe - is a scalar value represented benchmark�s Sharpe ratio value.
%        
%        inExcessStatistics - is a dataset specifying portfolio excess statistics at maturity date and must have follow fields:
%           - delta_1 - is a scalar value specifying the delta between holding return of the portfolio vs. benchmark
%                                   return at the given date.  
%
%           - delta_2 - is a scalar value specifying the excess holding return between portfolio and expected return,
%                                   normalized by portfolio�s volatility at the given date. 
%
%           - delta_3 - is a scalar value specifying the excess Sharpe ratio of the portfolio above the benchmark�s one
%                                   at the given date. 
%        
%    Output:
%        outStatus - is a boolean value specifying the success of the operation.
%
%        outException - is a string value specifying the error message. If operation successed exception will be an
%                                   empty string. 
%
%	Note:
%        At the creation date performance indicators cannot be calculated because it has not any history. So, in this
%        case all of them must have NULL input. In addition a part of the risk parameters like Value-at-Risk may be not
%        calculated each time (according to Amit's remark).	  
% Sample: 
%       riskParameters = dataset([0.1213,0.214546,1,1,0.2565465,0.2945653], �VarNames�, {�ExpectedReturn�,
%                                               �ExpectedVolatility�, �ExpReturnCalcMethod�, �ExpVolatilityCalcMethod�,
%                                               �ValueAtRisk�, �CVar�});  
%       performanceIndicators = dataset([0.1002130, 0.1014546, 0.1123650, 0.023, 0.56465, 0.473, 1.513213, 0.236597,
%                                               0.35466501, 0.132133, 0.4654, 0.56321, 0.54687, 0.89798], �VarNames�,
%                                               {�holdingReturn�, �moneyWeightedReturn�, �timeWeigthedReturn�,
%                                               �volatility�, �downsideVolatility�, �skewness�, �kurtosis�, �alpha�,
%                                               �beta�, �uniqueRisk�, �sharpe�, �jensen�, �omega�, �infRatio�,
%                                               �rsquareVsBench�, �rmseVsBench�});  
%       partialVaRStatistic = dataset([1234567, 0.213, 0.2464; 2345678, 0.5462, 0.2165; 3456789, 0.3564, 0.4654],
%                                               �VarNames�, {� securityID�, �marginalVaR�, �componentVaR�});  
%       benchmarkStatistic = dataset([601, 0.0126, 0.021,  0.05, 0.5, -0.032], �VarNames�, {�benchmarkID�,
%                                               �holdingReturn�, �volatility�, �skewness�, �kurtosis�, �sharpe�}); 
%       excessStatistic = dataset([0.0126, 0.021, 0.05], �VarNames�, {�delta_1�, �delta_2�, �delta_3�}); 
%       [resStatus, errMessage] = set_statistics(1, 7560342, riskParameters, performanceIndicators, partialVaRStatistic,
%                                                                   benchmarkStatistic, excessStatistic); 
% Result set:
%               resStatus = 1, errMessage = ��.

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	___amit__________, at _aug 2013__________. The sense of update: ___setdbprefs, db return empty dataset which create
%     error in the fetch. changed to setdbprefs('cellarray')______________________________.
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;    

    %% Predefined constants:
    if (nargout > 1)
        outException = '';
    end
    try
        
        predefinedRiskMeasuresList = {'expectedReturn', 'expectedVolatility', 'expReturnCalcMethod', ...
                                                         'expVolatilityCalcMethod', 'valueAtRisk', 'cVaR'};
        predefinedRiskMeasuresNum = numel(predefinedRiskMeasuresList);

        predefinedPerformanceMeasuresList = {'holdingReturn', 'moneyWeightedReturn', 'timeWeigthedReturn', 'volatility', ...
                                                         'downsideVolatility', 'skewness', 'kurtosis', 'alpha', 'beta', 'uniqueRisk', 'sharpe', ...
                                                         'jensen', 'omega', 'infRatio', 'rsquareVsBench', 'rmseVsBench'};
        predefinedPerformanceMeasuresNum = numel(predefinedPerformanceMeasuresList);

        predefinedPerBondVaRStatisticsList = {'securityID', 'marginalVaR', 'componentVaR'};
        predefinedPerBondVaRStatisticsNum = numel(predefinedPerBondVaRStatisticsList);

        predefinedBenchmarkStatisticList = {'benchmarkID', 'holdingReturn', 'volatility', 'skewness', 'kurtosis', 'sharpe'};
        predefinedBenchmarkStatisticNum = numel(predefinedBenchmarkStatisticList);

        predefinedExcessStatisticList  = {'delta_1', 'delta_2', 'delta_3'};
        predefinedExcessStatisticNum = numel(predefinedExcessStatisticList);

        %% Input validation:
        error(nargchk(6, 7, nargin));
        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            inPortfolioID = 'NULL';
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_set:set_statistics:mismatchInput', 'inPortfolioID must be numeric.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        if (isempty(inCalcDate))
            error('dal_portfolio_set:set_statistics:mismatchInput', 'inCalcDate cannot be an empty.');
        elseif isnumeric(inCalcDate)
            inCalcDate = datestr(inCalcDate, 'yyyy-mm-dd HH:MM:SS');
        else
            error('dal_portfolio_set:set_statistics:mismatchInput', 'inCalcDate must be a numeric value.');
        end

        areRiskMeasuresEmpty = isempty(inRiskMeasures);
        arePerformanceMeasuresEmpty = isempty(inPerformanceMeasures);
        arePerBondVaRStatisticsEmpty = isempty(inPerBondVaRStatistics);
        areBenchmarkStatisticsEmpty =  isempty(inBenchmarkStatistics);
        if (nargin == 7)
            areExcessStatisticsEmpty = isempty(inExcessStatistics); 
        else
            areExcessStatisticsEmpty = true;
        end

        if (areRiskMeasuresEmpty)
            inRiskMeasures = dataset([{NaN(1,6)}, predefinedRiskMeasuresList]);
        elseif (~isa(inRiskMeasures, 'dataset'))
            error('dal_portfolio_set:set_statistics:wrongInput', 'inRiskMeasures must be a dataset.');
        else
            inRiskMeasuresList = dsnames(inRiskMeasures);
            if numel(inRiskMeasuresList) ~= predefinedRiskMeasuresNum
                error('dal_portfolio_set:set_statistics:wrongInput', ['inRiskMeasures must have strictly ', num2str(predefinedRiskMeasuresNum), ' fields.']);
            elseif (~all(strcmpi(inRiskMeasuresList, predefinedRiskMeasuresList)))
                error('dal_portfolio_set:set_statistics:wrongInput', 'All field names of inRiskMeasures dataset must be from predefined name-set and with predefined order.');
            end
        end

        if (arePerformanceMeasuresEmpty)
            inPerformanceMeasures = dataset([{NaN(1,16)}, predefinedPerformanceMeasuresList]);
        elseif (~isa(inPerformanceMeasures, 'dataset'))
            error('dal_portfolio_set:set_statistics:wrongInput', 'inPerformanceMeasures must be a dataset.');
        else
            inPerformanceMeasuresList = dsnames(inPerformanceMeasures);
            if (numel(inPerformanceMeasuresList) ~= predefinedPerformanceMeasuresNum)
                error('dal_portfolio_set:set_statistics:wrongInput', ['inPerformanceMeasures must have strictly ',num2str(predefinedPerformanceMeasuresNum),' fields.']);
            elseif (~all(strcmpi(inPerformanceMeasuresList, predefinedPerformanceMeasuresList)))
                error('dal_portfolio_set:set_statistics:wrongInput', 'All field names of inPerformanceMeasures dataset must be from predefined name-set and with predefined order.');
            end
        end

        if (areRiskMeasuresEmpty && arePerformanceMeasuresEmpty)
            statisticsExist = false;
        else
            statisticsExist = true;
        end

        if (arePerBondVaRStatisticsEmpty)
            perBondVaRStatisticsExist = false;
        elseif (~isa(inPerBondVaRStatistics, 'dataset'))
            error('dal_portfolio_set:set_statistics:wrongInput', 'inPerBondVaRStatistics must be a dataset.');
        else
            perBondVaRStatisticsExist = true;    
            inPerBondVaRStatisticsList = dsnames(inPerBondVaRStatistics);
            if numel(inPerBondVaRStatisticsList) ~= predefinedPerBondVaRStatisticsNum
                error('dal_portfolio_set:set_statistics:wrongInput', ['inPerBondVaRStatistics must have strictly ', num2str(predefinedPerBondVaRStatisticsNum),' fields.']);
            elseif (~all(strcmpi(inPerBondVaRStatisticsList, predefinedPerBondVaRStatisticsList)))
                error('dal_portfolio_set:set_statistics:wrongInput', 'All field names of inPerBondVaRStatistics dataset must be from predefined name-set and with predefined order.');
            end
        end

        if (areBenchmarkStatisticsEmpty)
            benchmarkStatisticsExist = false;
        elseif (~isa(inBenchmarkStatistics, 'dataset'))
            error('dal_portfolio_set:set_statistics:wrongInput', 'inBenchmarkStatistic must be a dataset.');
        else
            benchmarkStatisticsExist = true;
            inBenchmarkStatisticList = dsnames(inBenchmarkStatistics);
            if numel(inBenchmarkStatisticList) ~= predefinedBenchmarkStatisticNum
                error('dal_portfolio_set:set_statistics:wrongInput', ['inBenchmarkStatistic must have strictly ', num2str(predefinedBenchmarkStatisticNum), ' fields.']);
            elseif (~all(strcmpi(inBenchmarkStatisticList, predefinedBenchmarkStatisticList)))
                error('dal_portfolio_set:set_statistics:wrongInput', 'All field names of inBenchmarkStatistic dataset must be from predefined name-set and with predefined order.');
            end
        end

        if (nargin < 7) || (areExcessStatisticsEmpty)
           excessStatisticsExist = false;
        elseif (~isa(inExcessStatistics, 'dataset'))
            error('dal_portfolio_set:set_statistics:wrongInput', 'inExcessStatistic must be a dataset.');
        else
            excessStatisticsExist = true;
            inExcessStatisticList = dsnames(inExcessStatistics);
            if (numel(inExcessStatisticList) ~= predefinedExcessStatisticNum)
                error('dal_portfolio_set:set_statistics:wrongInput', ['inExcessStatistic must have strictly ', num2str(predefinedExcessStatisticNum), ' fields.']);
            elseif (~all(strcmpi(inExcessStatisticList, predefinedExcessStatisticList)))
                error('dal_portfolio_set:set_statistics:wrongInput', 'All field names of inExcessStatistic dataset must be from predefined name-set and with predefined order.');
            end
        end

        %% Step #1 (Build SQL query):
        if (statisticsExist)
            portfolioRegularStatisticsSqlQuery = ...
                ['CALL set_statistics(', inPortfolioID ', ''', inCalcDate, ''', ', ds2str4sql(inRiskMeasures,0, 'f'), ',', ds2str4sql(inPerformanceMeasures,0, 'f'), ');'];
        end

        if (perBondVaRStatisticsExist)
            portfolioPerBondVaRStatisticsSqlQuery = ...
                ['CALL set_partial_var_statistics(', inPortfolioID, ', ''',inCalcDate, ''', ', ['"' ds2str4sql(inPerBondVaRStatistics,0,'f') '"'], ');'];
        end

        if (benchmarkStatisticsExist)
            benchmarkStatisticSqlQuery = ...
                ['CALL set_benchmark_statistics(', inPortfolioID, ', ''', inCalcDate, ''', ', ds2str4sql(inBenchmarkStatistics,0, 'f'), ');'];
        end

        if (excessStatisticsExist)
            excessStatisticSqlQuery = ...
                ['CALL set_excess_statistics(', inPortfolioID, ', ''', inCalcDate, ''', ', ds2str4sql(inExcessStatistics,0, 'f'), ');'];
        end

        %% Step #2 (Open the connection):
        if statisticsExist | perBondVaRStatisticsExist | benchmarkStatisticsExist | excessStatisticsExist
            setdbprefs ('DataReturnFormat','cellarray');
            conn = mysql_conn('portfolio');
            set(conn,'AutoCommit','off');
        end

        %% Step #3(Execution of built SQL sqlquery):
        if (statisticsExist)
            fetch(conn, portfolioRegularStatisticsSqlQuery);
            if (nargout > 0)
            outStatus = true;
                if (nargout > 1)
                    outException = [outException, 'Portfolio statistics were saved.'];
                end
            end
        end
        
        if (perBondVaRStatisticsExist)
            fetch(conn, portfolioPerBondVaRStatisticsSqlQuery);
            if (nargout > 0)
            outStatus = true;
                if (nargout > 1)
                    outException = [outException, ' Asset contribution to portfolio VaR table was saved.'];
                end
            end    
        end
        
        if (benchmarkStatisticsExist)
             fetch(conn, benchmarkStatisticSqlQuery);
             if (nargout > 0)
                outStatus = true;
                if (nargout > 1)
                    outException = [outException, ' Benchmark statistics for this portfolio were saved.'];
                end
             end
        end
        
        if (excessStatisticsExist)
             fetch(conn, excessStatisticSqlQuery);
             if (nargout > 0)
                outStatus = true;
                if (nargout > 1)
                    outException = [outException,  ' Excess statistics were saved.'];
                end
             end
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);            
        end      
        
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ME.message];
            end
        end
    
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);            
        end        

    end
    
setdbprefs ('DataReturnFormat','dataset');
end

