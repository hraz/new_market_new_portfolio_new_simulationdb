function [outStatus, outException] = set_allocation(inPortfolioID ,inDate, inAllocationTbl, inTransactionData)
%SET_ALLOCATION initializes and updates allocation, transactions and income-status of portfolio.
%
%	[outStatus, outException] = set_allocation(inPortfolioID ,inDate, inAllocationTbl, inTransactionData)
%
%	Input:
%       inPortfolioID - is a scalar value specifying the given portfolio ID number. It cannot be an empty ([])! 
%
%       inDate	is a scalar value specifying the current date.
%
%       inAllocationTbl - is a dataset value specifying the list of security allocation data with following fields:
%                                'securityID', 'unitNum'. Empty allocation table means closing of all positions.
%                               Where:
%                               securityID - is a scalar value specifying some asset that was purchased or sold at given
%                                                   transaction. 
%
%                               unitNum - is a scalar value specifying the number of given asset�s units, changed during
%                                                   given transaction. (Negative value means Short position.) 
% 
%       inTransactionData - is a dataset value specifying all data about all transactions were done at given transaction
%                              date. The struct must have following fields: �update�, �securityID�, �unitNum�,
%                              �transactionCost�,�actionAmount�.  
%                               Where:
%                               update - is a scalar values specifying the date-time of the operation with given asset.
%
%                               securityID - is a scalar value specifying some asset that was purchased or sold at given
%                                                   transaction. 
%
%                               unitNum - is a scalar value specifying the number of given asset�s units, changed during
%                                                   given transaction (�+� means buy operation, �-�means sale operation). 
%
%                               transactionCost - is a scalar value specifying the tax on the given action in cash of
%                                                   the domestic currency (agorot). 
%
%                               actionAmount - is a scalar value specifying the gross (before tax)money amount (in
%                                                   agorot)  that was used for given operation (�+� means buy operation,
%                                                   �-�means sale operation).  
% 
%	Output:
%       outStatus - is a boolean value specifying the success of the operation.
%
%       outException - is a string value specifying the error message. If operation successed exception will be an empty
%                               string. 
% Sample:
%   inAllocationTbl = dataset([1234567,123;2345678,456;3456789,789], �VarNames�, {�securityID�, unitNum�}); 
%   inTransactionData = dataset([723456, 1234567, 23, 12.3, 147; 723457, 2345678, 56, 45.6, 258; 723458, 3456789, 89,
%   78.9, 369], �VarNames�, {� update�, securityID�, �unitNum�, �transactionCost�, �actionAmount�});  
%   [resStatus, errMessage] = set_transaction(1 ,723400, inAllocationTbl, inTransactionData); 
% Result set: 
%                   resStatus = 1, errMessage = ��;

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	___amit__________, at _aug 2013__________. The sense of update: ___setdbprefs, db return empty dataset which create
%     error in the fetch. changed to setdbprefs('cellarray')______________________________.
% Updated by:	________________, at ___________. The sense of update: _________________________________.
    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dataset.*;
    
    %% Input validation:
    if (nargout > 1)
        outException = '';
    end
    try
        
        error(nargchk(4, 4, nargin));
        if (isempty(inPortfolioID) || isnan(inPortfolioID))
            error('dal_portfolio_set:set_allocation:wrongInput','Portfolio ID cannot be NULL.');
        elseif ~isnumeric(inPortfolioID)
            error('dal_portfolio_set:set_allocation:mismatchInput', 'inPortfolioID must be numeric.');
        else
            inPortfolioID = num2str(inPortfolioID);
        end

        if isempty(inDate)
            error('dal_portfolio_set:set_allocation:mismatchInput', 'inDate cannot be an empty.');
        elseif isnumeric(inDate)
            inDate = ['''' datestr(inDate, 'yyyy-mm-dd HH:MM:SS') ''''];
        else
            error('dal_portfolio_set:set_allocation:mismatchInput', 'inDate must be a numeric value.');
        end

        %% Step #1 (Preparation of allocation table and transaction history):
         if isempty(inAllocationTbl)
             allocationTblStr = 'NULL';
        elseif ~isa(inAllocationTbl, 'dataset')
            error('dal_portfolio_set:set_allocation:wrongInput', 'inAllocationTbl must be a dataset.');
        else
            allocationTblStr = ['''' ds2str4sql(inAllocationTbl,0) ''''];
        end

        if isempty(inTransactionData)
             error('dal_portfolio_set:set_allocation:mismatchInput', 'inTransactionData cannot be an empty.');
        elseif ~isa(inTransactionData, 'dataset')
            error('dal_portfolio_set:set_allocation:mismatchInput', 'inTransactionData must be a dataset.');
        else
            transactionDataStr = ['"', ds2str4sql(inTransactionData, 1), '"'];
        end
           
        %% Step #2 (Build SQL query):
        transactionDataSqlQuery = ['CALL set_transaction(', inPortfolioID, ', ', transactionDataStr, ');'];
        allocationTblSqlQuery = ['CALL set_allocation(', inPortfolioID, ', ', inDate, ', ' allocationTblStr ');'];

        %% Step #1 (Open the connection):
        setdbprefs ('DataReturnFormat','cellarray');
        conn = mysql_conn('portfolio');
        set(conn,'AutoCommit','off');

        %% Step #3(Execution of built SQL sqlquery):
        fetch(conn, transactionDataSqlQuery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                outException = 'Transactions were saved.';
            end
        end
        
        fetch(conn, allocationTblSqlQuery);
        if (nargout > 0)
            outStatus = true;
            if (nargout > 1)
                if isempty(inAllocationTbl)
                    outException = [outException,  ' ', 'All positions in the given allocation table were closed.'];
                else
                    outException = [outException,  ' ', 'Allocation table was updated.'];
                end
            end
        end
        
        %% Step #4.1 (Close the connection):
        if exist('conn', 'var')
            close(conn);            
        end
    
    catch ME
        
        if (nargout > 0)
            outStatus = false;
            if (nargout > 1)
                outException = [outException, ' ', ME.message];
            end
        end
        
        %% Step #4.2 (Close the connection):
        if exist('conn', 'var')
            close(conn);            
        end
        
    end
    
setdbprefs ('DataReturnFormat','dataset');
end

