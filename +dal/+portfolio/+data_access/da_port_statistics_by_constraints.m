function [ port_statistics ] = da_port_statistics_by_constraints(port_id_list, first_date, last_date )
%DA_PORT_STATISTICS_BY_CONSTRAINTS Summary of this function goes here
%   Detailed explanation goes here

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Create portfolio ID list:
%     values = num2sqlbracket(port_id_list);
    values = num2str4sql(port_id_list);
    
     %% Get average of required statistics:
     sqlquery = ['CALL sp_get_port_statistics_by_model_constraints(', values, ', ', first_date,', ', last_date, ')'];
             
     %%  Open a connetion:
     setdbprefs ('DataReturnFormat','dataset');
    
      conn = mysql_conn('portfolio');
      
     %% Execute required commands:
     port_statistics = fetch(conn,sqlquery);
     
     %% Close the connection:
     close(conn);
     
end

