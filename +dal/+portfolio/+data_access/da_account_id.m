function [ account_id ] = da_account_id( account_name )
%GET_ACCOUNT_ID return account id according to account name.
%
%   [ account_id ] = get_account_id( account_name ) 
%   returns the account id according to the account name.
%                    
%   Input:
%           account_name - is a STRING value specifying the account name.
%     
%   Output:
%              account_id - is an INTEGER specifying the account id according to account_name.       
%
% Yaakov Rechtman
%  22/04/2012
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import dal.mysql.data_access.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(1, 1, nargin));   
    if(isempty(account_name))
          error('dal_portfolio_data_access:get_account_id:wrongInput', ...
            'username can not be empty');
    end
    
    %% Step #1 (Execution of sql function):
    setdbprefs ('DataReturnFormat','dataset');                                                     

    conn = mysql_conn('portfolio');
    account_id = get_func(conn,'sf_get_account_by_name',{ account_name }, java.sql.Types.INTEGER  );
       
    %% Step #2 (Close the opened connection):
    close(conn);
    
end





