%% Import external packages:
import dal.portfolio.test.t09_get_income_status.*;

%% Test description:
funName = 'get_income_status';
testDesc = 'Not NULL first date-time and NULL as a second one. -> result set:';

expectedStatus = 1;
expectedError = '';
date = {'2013-05-07 11:09:59.0';'2013-05-08 18:10:31.0'; ...
            '2013-05-09 18:10:54.0';'2013-05-10 11:11:20.0';'2013-05-13 11:11:38.0'};
expectedResult = [dataset(date), dataset({[312,5, 805;310,6,805;312,7,805;313,7,805;315,10,805], ...
                            'holdingValue', 'cashAmount', 'investmentAmount'})];

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = datenum('2013-05-05 12:12:12'); 
 inLastDate = [];
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate );
