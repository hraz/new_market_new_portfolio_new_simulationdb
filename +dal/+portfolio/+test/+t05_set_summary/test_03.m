%% Import external packages:
import dal.portfolio.test.t05_set_summary.*;

%% Test description:
funName = 'set_summary';
testDesc = 'New portfolio creation: NaN as creation date -> expected error:';

expectedStatus = 0;
expectedError = 'Data truncation: Incorrect datetime value: ''NULL'' for column ''in_creation_date'' at row 198';

%% Input definition:
inID = [];
inName = 'New name';
inDesc =  'New';
inCreationDate = NaN;
inMaturityDate = datenum('2012-01-01 12:12:12') + 365;
inPublicFlag = 0;
inVirtualFlag = 1;
inBasedOnFlag = 1;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inName, inDesc, inCreationDate, inMaturityDate, inPublicFlag, inVirtualFlag, inBasedOnFlag);
