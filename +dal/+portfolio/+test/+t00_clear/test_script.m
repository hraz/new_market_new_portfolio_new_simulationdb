function [result] = test_script( funName, testDesc, expectedStatus, expectedError, newSimNum )
%TEST_SCRIPT is a common test operations for the specific tested function.

    %% Import tested directory:
    import dal.portfolio.del.*
    import dal.portfolio.test.*;
    
    %% Test execution:
    [status, errMsg] = clear(newSimNum);

    %% Result analysis:
    if (status == expectedStatus) && (strcmpi(errMsg, expectedError))
        testResult = 'Success';
    else
        testResult = 'Failure';
    end
    result = dataset({{datestr(now()), funName, testDesc, testResult}, 'Time', 'Tested API', 'Test', 'Test Result'});

    %% Save tests' result on hard disk:
    save_test_result(result);

end
