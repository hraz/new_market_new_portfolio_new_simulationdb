%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'Right date-time. -> result set:';

expectedStatus = 1;
expectedError = '';

marketConstraint.staticConstraint = dataset({[10,1,0.1,1;17,1,0.2,0.99], ...
                                                                                'constraintID','constraintValueID','minValue','maxValue'});
marketConstraint.dynamicConstraint = dataset({[6,0.1,1;9,0.2,0.98],'constraintID','minValue','maxValue'});
marketConstraint.sectorConstraint = dataset({[601,0,1], 'sectorID','minValue','maxValue'});
marketConstraint.ratingConstraint = ...
    [dataset({[1,NaN,1,20], 'maalotRatingMinID','maalotRatingMaxID','midroogRatingMinID','midroogRatingMaxID'}), ...
    dataset({{'AND'}, 'logicOperand'})];

specificConstraint.basicConstraint = dataset({[1,0.05;2,0.1], 'constraintID','value'});
specificConstraint.targetConstraint = dataset({[3,0.1;4,0.12;5,0.2], 'constraintID','value'});
specificConstraint.modelConstraint = dataset({[1,1,1], 'modelID','optimizationTypeID','reinvestmentStrategyID'});
specificConstraint.taxConstraint = dataset({[1,0.2], 'taxID','feeValue'});

expectedResult.marketConstraint = marketConstraint;
expectedResult.specificConstraint = specificConstraint;

%% Input definition:
 inPortfolioID = 1;
 inDate = datenum('2012-03-01 12:12:18'); 
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
