%% Import external packages:
import dal.portfolio.test.t14_get_constraints.*;

%% Test description:
funName = 'get_constraints';
testDesc = 'NULL as required date-time. -> error message:';

expectedStatus = 0;
expectedError = 'Required date cannot be an empty.';
expectedResult = dataset();

%% Input definition:
 inPortfolioID = 1;
 inDate = []; 

expectedResult.marketConstraint = struct();
expectedResult.specificConstraint = struct();
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inDate );
