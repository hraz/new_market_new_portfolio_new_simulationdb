%% Import external packages:
import dal.portfolio.test.t04_get_specific_def.*;

%% Test description:
funName = 'get_specific_def';
testDesc = 'Wrong inGroupName value. -> error message:';

expectedStatus = 0;
expectedError = 'Undefined group name! It may be one of: model, optimization, reinvestment or tax.';
expectedResult = [];

%% Input definition:
inGroupName = 'target';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inGroupName);
