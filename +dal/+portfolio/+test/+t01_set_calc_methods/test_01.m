%% Import external packages:
import dal.portfolio.test.t01_set_calc_methods.*;

%% Test description:
funName = 'set_calc_methods';
testDesc = 'Use empty statistic name -> expected error:';

expectedStatus = 0;
expectedError = 'The statistic name cannot be NULL or an empty string.';

%% Input definition:
inID = [];
inStatisticName = '';
inMethod = 'Tmp';
inDesc =  'Tmp';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inStatisticName, inMethod, inDesc);
