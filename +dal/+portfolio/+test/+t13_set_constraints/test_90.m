%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Duplicate input for tax constraint. -> error message:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2012-03-09 12:12:12-1'' for key ''UK_tax_status''';
                           
%% Input definition:
inID = 1;
inDate =  datenum('2012-03-09 12:12:12');
inMarketConstraints = struct('staticConstraint', dataset({[15,1,0.2,0.99], 'constraintID', 'constraintValueID', 'minValue', 'maxValue'}), ...
                                            'dynamicConstraint', dataset({[8,0.2,0.98], 'constraintID', 'minValue', 'maxValue'}), ...
                                            'sectorConstraint', dataset({[603,0.0,1.0], 'sectorID', 'minValue', 'maxValue'}), ...
                                            'ratingConstraint', dataset());
inSpecificConstraints = struct('basicConstraint', dataset({[2,0.20], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset(), ...
                                             'modelConstraint', dataset(), ...
                                             'taxConstraint', dataset({[1,0.2], 'taxID','feeValue'}));

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
