%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Duplicate input for model constraint. -> error message.:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2012-03-09 12:12:12-1-1'' for key ''UK_model_constraints''';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-09 12:12:12');
inMarketConstraints = struct('staticConstraint', dataset({[14,1,0.1,1.0], 'constraintID', 'constraintValueID', 'minValue', 'maxValue'}), ...
                                            'dynamicConstraint', dataset({[7,0.1,1.0], 'constraintID', 'minValue', 'maxValue'}), ...
                                            'sectorConstraint', dataset({[602,0.0,1.0], 'sectorID', 'minValue', 'maxValue'}), ...
                                            'ratingConstraint', dataset());
inSpecificConstraints = struct('basicConstraint', dataset({[1,0.05;2,0.10], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset(), ...
                                             'modelConstraint', dataset({[1,1,1], 'modelID', 'optimizationTypeID', 'reinvestmentStrategyID'}), ...
                                             'taxConstraint', dataset({[1,0.2], 'taxID','feeValue'}));

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
