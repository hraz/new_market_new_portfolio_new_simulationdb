%% Import external packages:
import dal.portfolio.test.t13_set_constraints.*;

%% Test description:
funName = 'set_constraints';
testDesc = 'Duplicate input for basic constraint. -> error message:';

expectedStatus = 0;
expectedError = 'Duplicate entry ''1-2012-03-09 12:12:12-1'' for key ''UK_basic_constraints''';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-09 12:12:12');
inMarketConstraints = struct('staticConstraint', dataset(), ...
                                            'dynamicConstraint', dataset(), ...
                                            'sectorConstraint', dataset(), ...
                                            'ratingConstraint', dataset());
inSpecificConstraints = struct('basicConstraint', dataset({[1,0.05;2,0.10], 'constraintID', 'value'}), ...
                                             'targetConstraint', dataset(), ...
                                             'modelConstraint', dataset(), ...
                                             'taxConstraint', dataset());

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inMarketConstraints, inSpecificConstraints);
