%% Import external packages:
import dal.portfolio.test.t10_set_statistics.*;

%% Test description:
funName = 'set_statistics';
testDesc = 'NULL value as expected_volatility. -> error message:';

expectedStatus = 0;
expectedError = 'Column ''expected_volatility'' cannot be null';

%% Input definition:
inID = 1;
inDate =  datenum('2012-03-01 12:12:12');
inRiskMeasures = dataset({[0.12,NaN,1,1,0.25,0.21], 'expectedReturn', 'expectedVolatility', 'expReturnCalcMethod', ...
                                                         'expVolatilityCalcMethod', 'valueAtRisk', 'cVaR'});
inPerformanceMeasures = dataset({[0.15,0.165,0.131,0.23,0.25,3,4.5,0.101,1.26,0.43,0.75,0.45,0.76,0.43,0.42,0.35], ...
                                                        'holdingReturn', 'moneyWeightedReturn', 'timeWeigthedReturn', 'volatility', ...
                                                         'downsideVolatility', 'skewness', 'kurtosis', 'alpha', 'beta', 'uniqueRisk', 'sharpe', ...
                                                         'jensen', 'omega', 'infRatio', 'rsquareVsBench', 'rmseVsBench'});
inPerBondVaRStatistics = dataset({[1234567, 0.11,0.21; 1234568, 0.12, 0.22; 1234569, 0.13, 0.23; ...
                                                        2345670, 0.14, 0.24; 2345678, 0.15, 0.25; 2345679, 0.16, 0.26; ...
                                                        3456780, 0.17, 0.27; 3456788, 0.18, 0.28; 3456789, 0.19, 0.29], ...
                                                        'securityID', 'marginalVaR', 'componentVaR'});
inBenchmarkStatistics = dataset({[601,0.12,0.23,0.25,0.21,0.15], 'benchmarkID', 'holdingReturn', 'volatility', 'skewness', 'kurtosis', 'sharpe'});
inExcessStatistics = dataset({[0.25,0.21,0.15], 'delta_1', 'delta_2', 'delta_3'});


%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inRiskMeasures, inPerformanceMeasures, inPerBondVaRStatistics, inBenchmarkStatistics, inExcessStatistics)
