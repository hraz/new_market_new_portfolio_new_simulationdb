%% Import external packages:
import dal.portfolio.test.t10_set_statistics.*;

%% Test description:
funName = 'set_statistics';
testDesc = 'Additional data input for testing of last_update in get_summary -> result set:';

expectedStatus = 1;
expectedError = 'Portfolio statistics were saved. Asset contribution to portfolio VaR table was saved. Benchmark statistics for this portfolio were saved. Excess statistics were saved.';

%% Input definition:
inID = 1;
inDate =  datenum('2012-04-02 12:12:12');
inRiskMeasures = dataset({[0.13,0.24,1,1,0.26,0.22], 'expectedReturn', 'expectedVolatility', 'expReturnCalcMethod', ...
                                                         'expVolatilityCalcMethod', 'valueAtRisk', 'cVaR'});
inPerformanceMeasures = dataset({[0.16,0.16,0.13,0.24,0.26,2,4.6,0.10,1.27,0.44,0.76,0.46,0.77,0.44,0.43,0.36], ...
                                                        'holdingReturn', 'moneyWeightedReturn', 'timeWeigthedReturn', 'volatility', ...
                                                         'downsideVolatility', 'skewness', 'kurtosis', 'alpha', 'beta', 'uniqueRisk', 'sharpe', ...
                                                         'jensen', 'omega', 'infRatio', 'rsquareVsBench', 'rmseVsBench'});
inPerBondVaRStatistics = dataset({[1234567, 0.10,0.22; 1234568, 0.13, 0.21; 1234569, 0.11, 0.21; ...
                                                        2345670, 0.13, 0.25; 2345678, 0.16, 0.24; 2345679, 0.15, 0.25; ...
                                                        3456780, 0.16, 0.25; 3456788, 0.19, 0.27; 3456789, 0.18, 0.28], ...
                                                        'securityID', 'marginalVaR', 'componentVaR'});
inBenchmarkStatistics = dataset({[601,0.11,0.24,0.22,0.22,0.16], 'benchmarkID', 'holdingReturn', 'volatility', 'skewness', 'kurtosis', 'sharpe'});
inExcessStatistics = dataset({[0.24,0.22,0.16], 'delta_1', 'delta_2', 'delta_3'});

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inRiskMeasures, inPerformanceMeasures, inPerBondVaRStatistics, inBenchmarkStatistics, inExcessStatistics)
