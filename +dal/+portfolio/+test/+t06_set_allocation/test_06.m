%% Import external packages:
import dal.portfolio.test.t06_set_allocation.*;

%% Test description:
funName = 'set_allocation';
testDesc = 'Check duplicate entry for transaction data: -> error message.';

expectedStatus = 0;
expectedError = ' Duplicate entry ''1-2012-05-01 10:34:20-1234567'' for key ''UK_transactions''';

%% Input definition:
inID = 1;
dates = datenum({'2012-05-01 10:34:20',...
                                        '2012-05-01 10:45:36',...
                                        '2012-05-03 17:36:01',...
                                        '2012-05-03 17:46:25',...
                                        '2012-05-03 17:50:55',...
                                        '2012-05-08 17:37:18',...
                                        '2012-05-09 17:37:38',...
                                        '2012-05-09 17:50:07',...
                                        '2012-05-13 10:38:30'});
securityID = [1234567;2345678;3456789;1234568;2345679;3456780;1234569;2345670;3456788];
unitNum = [2;3;4;5;6;7;8;1;2];
transactionCost = [3;4;5;6;7;8;9;2;4];
actionAmount = [30;40;50;-10;-5;-7;60;20;23];
inDate =  max(dates);

inAllocationTbl = dataset(securityID, unitNum);
inTransactionData = dataset(dates, securityID, unitNum, transactionCost, actionAmount);

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData);
