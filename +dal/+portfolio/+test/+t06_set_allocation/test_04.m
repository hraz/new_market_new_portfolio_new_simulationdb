%% Import external packages:
import dal.portfolio.test.t06_set_allocation.*;

%% Test description:
funName = 'set_allocation';
testDesc = 'NULL as update time for allocation table: -> error message.';

expectedStatus = 0;
expectedError = ' inDate cannot be an empty.';

%% Input definition:
inID = 1;

securityID = [1234567,1534568,7534568]';
unitNum = [12,15,15]';
dates = datenum({'2012-12-14 14:25:37', '2012-12-14 14:26:39', '2012-12-14 18:30:39'});
transactionCost = [0.25, 1.20, 0.20]';
actionAmount = [345, 195,205]';

inDate =  [];

inAllocationTbl = dataset(securityID, unitNum);
inTransactionData = dataset(dates, securityID, unitNum, transactionCost, actionAmount);

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData);
