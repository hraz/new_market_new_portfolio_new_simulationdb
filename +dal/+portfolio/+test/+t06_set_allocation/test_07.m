%% Import external packages:
import dal.portfolio.test.t06_set_allocation.*;

%% Test description:
funName = 'set_allocation';
testDesc = 'Check deleting of the current allocation table (NULL): -> an empty result set.';

expectedStatus = 1;
expectedError = 'Transactions were saved. All positions in the given allocation table were closed.';

%% Input definition:
inID = 1;
dates = datenum({'2013-01-01 10:34:20',...
                             '2013-01-01 10:45:36',...
                             '2013-01-03 17:36:01',...
                             '2013-01-03 17:46:25',...
                             '2013-01-03 17:50:55',...
                             '2013-01-08 17:37:18',...
                             '2013-01-09 17:37:38',...
                             '2013-01-09 17:50:01',...
                             '2013-01-13 10:38:30'});
securityID = [1234567;2345678;3456789;1234568;2345679;3456780;1234569;2345670;3456788];
unitNum = [-2;-3;-4;-5;-6;-7;-8;-1;-2];
transactionCost = [3;4;5;6;7;8;9;2;4];
actionAmount = [30;40;50;60;50;70;60;20;23];
inTransactionData = dataset(dates, securityID, unitNum, transactionCost, actionAmount);

inDate =  max(dates);
securityID = [];
unitNum = [];
inAllocationTbl = dataset(securityID, unitNum);

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID ,inDate, inAllocationTbl, inTransactionData);
