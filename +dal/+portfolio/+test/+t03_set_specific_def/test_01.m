%% Import external packages:
import dal.portfolio.test.t03_set_specific_def.*;

%% Test description:
funName = 'set_specific_def';
testDesc = 'Use empty group name -> expected error:';

expectedStatus = 0;
expectedError = 'The in_group_name''s value cannot be an empty string.';

%% Input definition:
inID = [];
inGroupName = '';
inName = 'Tmp';
inDesc =  'Tmp';

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, inID, inGroupName, inName, inDesc);
