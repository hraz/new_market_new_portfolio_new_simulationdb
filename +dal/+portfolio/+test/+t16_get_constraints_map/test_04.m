%% Import external packages:
import dal.portfolio.test.t16_get_constraints_map.*;

%% Test description:
funName = 'get_constraints_map';
testDesc = 'Right inConstraintGroup value -> result set:';

expectedStatus = 1;
expectedError = '';
expectedResult = cell(4,1);

expectedResult{1}  = [dataset({[1;2], 'id'}), dataset({{'investmentAmount';'investmentHorizon'}, 'dataName'})];
expectedResult{2}  = [dataset({[3;4;5], 'id'}), dataset({{'return';'risk';'optimalSharpe'}, 'dataName'})];
expectedResult{3}  = [dataset({[6;7;8;9], 'id'}), dataset({{'duration';'turnover';'time2Maturity';'yield'}, 'dataName'})];
expectedResult{4}  = [dataset({[10;11;12;13;14;15;16;17], 'id'}), ...
    dataset({{'issuer';'securityType';'couponType';'class';'linkageIndex';...
    'classLinkageCombination';'includedSecurity';'excludedSecurity'}, 'dataName'})];

%% Input definition:
inConstraintGroup = {'basic'; 'target'; 'dynamic'; 'static'};

%% Test execution:
for i = 1:length(inConstraintGroup)
    test_script( funName, [inConstraintGroup{i}, ' :: ', testDesc], expectedStatus, expectedError, expectedResult{i}, inConstraintGroup{i});
end
