%% Import external packages:
import dal.portfolio.test.t11_get_statistics.*;

%% Test description:
funName = 'get_statistics';
testDesc = 'NULL first date-time and not NULL a second one. -> result set:';

expectedStatus = 1;
expectedError = '';
calcDate = {'2012-03-01 12:12:12.0'}; 
ds = dataset({[0.12,0.23,1,1,0.25,0.21,0.15,0.165,0.131,0.23,0.25,3,4.5,0.101,1.26,0.43, 0.75,0.45,0.76,0.43,0.42,0.35], ...        
                       'expectedReturn','expectedVolatility','expReturnCalcMethod',...
                        'expVolatilityCalcMethod','valueAtRisk','cVaR','holdingReturn',...
                        'moneyWeightedReturn','timeWeightedReturn','volatility', ...
                        'downsideVolatility','skewness','kurtosis','alpha','beta','uniqueRisk', ...
                        'sharpe','jensen','omega','infRatio','rsquareVsBench','rmseVsBench'});
expectedResult.portStatistics = [dataset(calcDate), ds];

ds1 = dataset({[1234567;1234568;1234569;2345670;2345678;2345679;
                     3456780;3456788;3456789], 'securityID'});
calcDate = {'2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0'; ...
                   '2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0'; ...
                   '2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0';'2012-03-01 12:12:12.0'};         
ds2 = dataset({[0.11,0.21;0.12,0.22;0.13,0.23;0.14,0.24;0.15,0.25;0.16,0.26;0.17,0.27;...
                         0.18,0.28;0.19,0.29],'marginalVaR', 'componentVaR'});
expectedResult.perBondVaRStatistics = [ds1,dataset(calcDate), ds2];

calcDate = {'2012-03-01 12:12:12.0'};
ds3 = dataset({[601, 0.12 ,0.23 ,0.25 ,0.21 ,0.15], 'benchmarkID', 'holdingReturn','volatility','skewness','kurtosis','sharpe'});
expectedResult.benchStatistics = [dataset(calcDate),ds3];

%% Input definition:
 inPortfolioID = 1;
 inFirstDate = []; 
 inLastDate = datenum('2012-03-02 12:12:12');
inBenchmarkID = 601;

%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioID, inFirstDate, inLastDate, inBenchmarkID );
