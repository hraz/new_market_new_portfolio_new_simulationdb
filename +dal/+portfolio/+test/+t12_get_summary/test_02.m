%% Import external packages:
import dal.portfolio.test.t12_get_summary.*;

%% Test description:
funName = 'get_summary';
testDesc = 'NaN value as a one of portfolio ID values. - error message:';

expectedStatus = 0;
expectedError = 'Portfolio ID list cannot include NaN value.';
expectedResult = dataset();

%% Input definition:
 inPortfolioIDList = [NaN,1];
 
%% Test execution:
test_script( funName, testDesc, expectedStatus, expectedError, expectedResult, inPortfolioIDList );
