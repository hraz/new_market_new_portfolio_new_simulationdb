function [result,err] = set_port_filter_history(account_id, up_date, filter_id)
%SET_PORT_FILTER_HISTORY update filter id used by account on up_date
%
%   [ result,err ] =set_port_filter_history(account_id,up_date,filter_id) - recieves account id up_Date and filter_id
%                                                                                                           and update table
%                      
%   Input:
%       account_id - portfolio accout which use the filter on up_date
%       filter_id - a value specifying the filter to update.
% 
%       up_date - is a DATETIME specifying the date of the obsevation.
% 
% 
%   Output:
%            result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%               
% 02/08/2012
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    
    import utility.dataset.*;    

   %% Input validation:
    error(nargchk(3, 3, nargin));
    
     if(isempty(up_date))
          error('dal_portfolio_crud:set_port_filter_history:wrongInput', ...
            'obs_date can not be empty');
     end
      
     if(isempty(filter_id))
          error('dal_portfolio_crud:set_port_filter_history:wrongInput', ...
            'filter_id can not be empty');
     end
      if ~isnumeric(filter_id)
          error('dal_portfolio_crud:set_port_filter_history:wrongInput', ...
            'wrong input type for filter_id');
      end
      
       if(isempty(account_id))
          error('dal_portfolio_crud:set_port_filter_history:wrongInput', ...
            'account_id can not be empty');
     end
      if ~isnumeric(account_id)
          error('dal_portfolio_crud:set_port_filter_history:wrongInput', ...
            'wrong input type for account_id');
      end

     %% Step #1 (getting column names from the database):
      setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio'); 
    set(conn,'AutoCommit','off');
     
      if isnumeric(up_date)
        up_date = datestr(up_date, 'yyyy-mm-dd');
      end
      account_id = {account_id};
      filter_id = {filter_id};
      up_date = {up_date};
      ds2insert = dataset(account_id,up_date,filter_id);
     
    %% Step #2 (Execution of built SQL sqlquery):
    try
        fastinsert(conn,'port_filter_history', dsnames(ds2insert), ds2insert);
        commit(conn);
        result = true;
        err = '';
    catch ME
        result = false;
        rollback(conn);
        err = ME.message;
    end
    
    %% Step #3 (Close the opened connection):
    close(conn);
        
end


