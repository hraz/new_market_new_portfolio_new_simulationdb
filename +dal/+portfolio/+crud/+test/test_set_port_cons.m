   clear; clc;

import dal.portfolio.crud.*;


 filter_id = 42;
 obs_date = today;
nsin_list = [ 1097385, 1098656, 1101575, 1105543 ];
filter_name = {'ytm'};
min_filter = {0.5};
max_filter = {0.5};

 ds_dyn_cons = dataset( filter_name,  min_filter,max_filter);
 
min_maalot = {'AA'};
max_maalot = {'AAA+'};
min_midroog = {'NR'};
max_midroog = {'Aaa'};
logical_operator = {'OR'};
ds_rating_cons = dataset(min_maalot,max_maalot,min_midroog,max_midroog,logical_operator);

sector = {'Health'};
min_sector = 0.5;
max_sector = 0.5;
 ds_Sector_cons = dataset( sector,  min_sector,max_sector);
 
 type_con = {'Government'};
min_type = 0.5;
max_type = 0.5;
 ds_static_cons = dataset( type_con,  min_type,max_type);
 
 

 
tic
   [result,err] =  set_port_cons(filter_id,nsin_list,ds_dyn_cons,ds_rating_cons,ds_Sector_cons,ds_static_cons,obs_date)
toc

 

