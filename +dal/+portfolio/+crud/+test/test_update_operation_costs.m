clear; clc;

import dal.portfolio.crud.*;
import dml.*;

rdate = '2005-01-02';
account_id = 1;
% tbl_name = 'risk';
% tbl_name = 'account_movements';
 tbl_name = 'operation_costs';



param_account_movements.transaction_cost = 1;
param_account_movements.holding_cost = 'NULL';
param_account_movements.tax_private = 'NULL';
param_account_movements.tax_corporate = 'NULL';

% param_risk.	return;
% param_risk.beta;
% param_risk.non_systematic_risk;
% param_risk.sharpe;
% param_risk.volatility;
% param_risk.kurtosis;
% param_risk.skewness;
% param_risk.alpha;
% param_risk.jensen;
% param_risk.inf_ratio;
% param_risk.omega;
% param_risk.hurst;
% param_risk.var;
% param_risk.cond_var;
% param_risk.margin_var;
% param_risk.coherent_var;
% param_risk.alt_inf_ratio;
% param_risk.debt_statistics;

% param_operation_costs.total_value;
% param_operation_costs.coupon_income;
% param_operation_costs.interest_earned;
% param_operation_costs.deposit;
% param_operation_costs.withdrawal;


tic
update_operation_costs(account_id, tbl_name,param_account_movements,rdate)
toc
