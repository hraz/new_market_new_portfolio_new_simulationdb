function [ result ] = update_portfolio(account_id , manager_nsin, ds2insert)
%UPDATE_PORTFOLIO update portfolio on account_id with ds2insert which
%                                       contains dates, security_id and
%                                       security_id_count
%
%   [ result ] = update_portfolio(account_id , manager_nsin, ds2insert) - update portfolio on account id and dataset containing assets id and asset count and
%                     insert it to the database with today's date
%                      
%   Input:
%       account_id - is an integet specifying the account to update.
%       manger_nsin - is an integer specifying the manager nsin who is in
%                               charge of the account
% 
%       ds2Insert - a NASSETSIDX(MASSET_COUNT)  dataset  specifying the
%       asset and assets count to insert
%       
%   Output:
%             result - success or failed.
%               
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

   %% Input validation:
    error(nargchk(3, 3, nargin));
    
      
     if(isempty(manager_nsin))
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'manager_nsin can not be empty');
     end
     if(isempty(account_id))
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'account_id can not be empty');
     end
       if ~isnumeric(manager_nsin)
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'wrong input type for manager nsin');
     end
      if ~isnumeric(account_id)
          error('dal_portfolio_crud:update_portfolio:wrongInput', ...
            'wrong input type for owner_nsin nsin');
     end

     %% Step #1 (Adding account_id to the dataset):
    account_id = account_id(ones(size(ds2insert.security_id,1),1));
    manager_nsin = manager_nsin(ones(size(ds2insert.security_id,1),1));
    security_id = ds2insert.security_id;
    update_date = ds2insert.update_date;
    security_id_count = ds2insert.security_id_count;
    ds2insert = dataset(account_id,manager_nsin, security_id,  security_id_count, update_date);
      
    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');                                                     

     conn = mysql_conn('portfolio');
     fastinsert(conn,'port_history' , {'account_id';'manager_nsin';'security_id';'security_id_count';'update_date'}, ds2insert)
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    
end


