function [result,err] = set_sim_err( obs_date, sim_id , ds_sim_err )
%SET_SIM_ERR insert simulation error to the database
%
%   [result,err]  = set_sim_err( obs_date, account_id , ds_sim_err ) - recives error messages and insert them to the
%                                                                                                               according to obs_date and
%                                                                                                               account id
%
%   Input:
%       obs_date - is a DATETIME specifying the date of the obsevation.
%
%       account_id - a value specifying the account to update.
%
%      ds_sim_err -is a dataset containing error messages
%
%   Output:
%            result - is a boolean value specifying if the insert was
%                           successful
%               err - is a STRING value specifying error message if the
%                        insert was unseccessful, null otherwise.
%
% 06/08/2012
% Yaakov Rechtman
% Copyright 2012

%% Import external packages:
import dal.mysql.connection.*;
import utility.dal.*;
import utility.dataset.*;

%% Input validation:
error(nargchk(3, 3, nargin));

if(isempty(obs_date))
    error('dal_portfolio_crud:set_sim_err:wrongInput', ...
        'obs_date can not be empty');
end

if isempty(ds_sim_err)
    error('dal_portfolio_crud:set_sim_err:wrongInput', ...
        'dyn_param can not be empty');
end

if(isempty(sim_id))
    error('dal_portfolio_crud:set_sim_err:wrongInput', ...
        'account_id can not be empty');
end
if ~isnumeric(sim_id)
    error('dal_portfolio_crud:set_sim_err:wrongInput', ...
        'wrong input type for owner_nsin nsin');
end

if isnumeric(obs_date)
    obs_date = {datestr(obs_date, 'yyyy-mm-dd')};
else
    obs_date = {obs_date};
end

% %       connecting to database
 setdbprefs ('DataReturnFormat','dataset');                                                     
   
conn = mysql_conn('errorDB');
set(conn,'AutoCommit','off');
try
    
    obs_date = obs_date(ones(size(ds_sim_err,1),1));
    sim_id = sim_id(ones(size(ds_sim_err,1),1));
    additional_cols = dataset(sim_id,obs_date);
    ds_sim_err =  [additional_cols , ds_sim_err];
    fastinsert(conn,'sim_errors', dsnames( ds_sim_err),  ds_sim_err);
    commit(conn);
    result = true;
    err = '';
catch ME
    result = false;
    rollback(conn);
    err = ME.message;
end

%% Step #3 (Close the opened connection):
close(conn);

end
