function state = update_port_status( account_id,status_name )
%UPDATE_PORT_STATUS  - updates the account status to the status name
%
%   [ state ] =  update_port_status( account_id,status_name )- recieves an
%   account id and a status name and update the acount to this status.
%                    
%   Input:
%       account_id -is an Integer specifying on which acount to update the
%                           status.
%        status_name - is a string value specifying the status to update.
%                                 value can be:
%                                 Opened,Active,Freezed,Closed.
%   Output:
%              state - success - if the update was successful.
%                          failed - if the update failed.
%                           Error: same status - if the update is for the
%                           same status.
%               
%
% Yaakov Rechtman
% Copyright 2012

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;    

    %% Input validation:
    error(nargchk(2, 2, nargin));   
    
   if(isempty(account_id) || isempty(status_name))
          error('dal_portfolio_IUD:update_port_status:wrongInput', ...
            'account_id or status_name can not be empty');
    end
    if ~ismember(status_name, {'Opened','Closed','Freezed','Active'})
         error('dal_portfolio_IUD:update_port_status:wrongInput', ...
            'wrong input for status_name.');
    end
    if ~isnumeric(account_id)
         error('dal_portfolio_IUD:update_port_status:wrongInput', ...
            'wrong input for account_id.');
    end
    
   %% Step #1 (SQL sqlquery for this filter):
    sqlquery = ['CALL sp_port_update_status( ' num2str(account_id) ',''' status_name ''');'];

    %% Step #2 (Execution of built SQL sqlquery):
    setdbprefs ('DataReturnFormat','dataset');                                                     
   
    conn = mysql_conn('portfolio');
    state = fetch(conn, sqlquery);
    
    %% Step #3 (Close the opened connection):
    close(conn);
    
    
end



