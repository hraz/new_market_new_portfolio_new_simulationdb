function [outPortfolioSummary] = get_summary(inPortfolioIDList)
%GET_SUMMARY returns portfolio summary by given portfolio ID list.
%
%	[outPortfolioSummary] = get_ summary(inPortfolioIDList)
%
% 	Input:
%       inPortfolioIDList - is a column numerical vector specifying the required portfolio ID numbers. 
%                                   Cannot be an empty! 
%
% 	Output:
%       outPortfolioSummary - is a dataset with the following fields :
%           - portfolioID - is a scalar value specifying the given portfolio ID number.
% 
%           - name - is a string value specifying the given portfolio name.
%
%           - description - is a string value specifying some portfolio description.
%
%           - creationDate - is a scalar value specifying given portfolio creation date.
%
%           - maturityDate - is a scalar value specifying given portfolio maturity date.
%
%           - publicFlag - is a boolean value specifying if given portfolio is public, default value is FALSE.
%
%           - virtualFlag - is a boolean value specifying if given portfolio is virtual, default value is FALSE.
%
%           - basedOn - is a scalar value specifying the type of underground portfolio that the current based on.
%
%           - lastUpdate - is  a scalar value specifying just last statistics update.
% 
% Sample:	
%	[outPortfolioSummary] = get_summary([15378; 12154]);
% Result set is:
%                       portfolioID | name | description | creationDate | maturityDate | publicFlag | virtualFlag | basedOn | lastUpdate
%                           15378 | Cohen | Long term low risk investment portfolio. | 756132 | 757543 | 0 | 1 | 2 | 756142
%                           12154 | Mizrachi | Middle term low risk investment portfolio. | 756140 | 757243 | 0 | 0 | 1 | 756142

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    import utility.dal.*;
    
    %% Input validation:
    error(nargchk(1,1,nargin));
    
    if isempty(inPortfolioIDList)
        error('dal_portfolio_get:get_summary:mismatchInput', 'Portfolio ID list cannot be an empty.');
    elseif ~isnumeric(inPortfolioIDList)
        error('dal_portfolio_get:get_summary:mismatchInput', 'Portfolio ID list must be a numeric vector.');
    elseif any(isnan(inPortfolioIDList))
        error('dal_portfolio_get:get_summary:mismatchInput', 'Portfolio ID list cannot include NaN value.');
    elseif any(inPortfolioIDList <0)
        error('dal_portfolio_get:get_summary:mismatchInput', 'Portfolio ID list must include just positive numbers.');
    else
        inPortfolioIDList = num2str4sql(inPortfolioIDList);
    end

    %% Create SQL query:
    sqlquery = ['CALL get_summary(', inPortfolioIDList, ');'];
    
    %% Create database connection:
    setdbprefs ('DataReturnFormat','dataset')
    conn = mysql_conn('portfolio');
    
    %% Get required data:
    try
        outPortfolioSummary = fetch(conn, sqlquery);
        if ~isempty(outPortfolioSummary)
            outPortfolioSummary = set(outPortfolioSummary, 'VarNames', {'portfolioID', 'name', 'description', 'creationDate', 'maturityDate', 'publicFlag', 'virtualFlag', 'basedOn', 'lastUpdate'});
        else
            outPortfolioSummary = dataset({cell(1,9), 'portfolioID', 'name', 'description', 'creationDate', 'maturityDate', 'publicFlag', 'virtualFlag', 'basedOn', 'lastUpdate'});
        end
    catch ME
        error('dal_portfolio_get:get_summary:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);
    
    %% Update logical values
end
