function [outDefinitionList] = get_calc_methods(inStatisticName)
%GET_CALC_METHODS returns a list of calculation methods.
%
%   [outDefinitionList] = get_calc_methods(inStatisticName)
%
%	Input:
%       inStatisticName - is a string value specifying some expected statistic like expected return or expected volatility.
%
%	Output:
%       outDefinitionList - is a result dataset that contains follow fields:
%           id - is a column vector specifying the property ID number.
%
%           methodName - is a column vector of strings describes the property name.
%
%           desc - is a column vector of strings specifying the some property description.
% 
%	Sample:
%           [definitionList] = get_calc_methods('exp_return')
%	Result set is:	
%           id | methodName | desc
%           1 | �Simple� | �Simple average�

% Created by Yigal Ben Tal.
% Date:		
% Copyright 2013, BondIT Ltd.	
% 
% Updated by:	________________, at ___________. The sense of update: _________________________________.

    %% Import external packages:
    import dal.mysql.connection.*;
    
    %% Input validation:
    try
        error(nargchk(1,1,nargin));

        if isempty(inStatisticName)
            error('dal_portfolio_get:get_calc_methods:mismatchInput', 'Required input cannot be NULL or an empty string.');
        elseif ~ischar(inStatisticName)
            error('dal_portfolio_get:get_calc_methods:wrongInput', 'Input value must be a string.');
        end

        %% Create SQL query:
        sqlquery = ['CALL get_calc_methods(''' inStatisticName ''');'];

        %% Create database connection:
        setdbprefs ('DataReturnFormat','dataset')
        conn = mysql_conn('portfolio');

        %% Get required data:
        outDefinitionList = fetch(conn, sqlquery);
        if ~isempty(outDefinitionList)
            outDefinitionList = set(outDefinitionList, 'VarNames', {'id','methodName','desc'});
        else
            outDefinitionList = dataset({cell(1,3), 'id', 'methodName', 'desc'});
        end
    catch ME
        outDefinitionList = dataset(); % THIS ROW POSSIBLE WILL REMOVED DURING A TESTING PHASE!
        error('dal_portfolio_get:get_calc_methods:wrongInput', ME.message);
    end
    
    %% Close opened connection:
    close(conn);

end

