classdef perf_ratio < hgsetget
    %PERF_RATIO determine Performance Measures class.
    % This class inherited from hgsetget class.
    %
    % Each object of rrm class has following properties (Default values are NaN):
    % --------------------------------------------------------------------------
    %     capm
    %     fin
    % --------------------------------------------------------------------------
    %
    %   The class includes following public methods:
    % --------------------------------------------------------------------------
    %   [ ratio ] = get_ratio(obj, ratio_name)
    % --------------------------------------------------------------------------
    %
    % Yigal Ben Tal
    % Copyright 2012 BondIT Ltd.
    
    properties (SetAccess = private, GetAccess = public)
        total_value
        capm
        fin
        rsquare
    end
    
    methods
        function [ obj ] = perf_ratio(varargin)
        %PERF_RATIO determine the asset or portfolio performance measures class constructor.
        %
        %   [ obj ] = perf_ratio(capm, fin) receives capm parameters and financial ratio values
        %   and assign it to the performance class properties.
        %
        %   Input:
        %       capm - is an 1x3 dataset specifying the CAPM model parameters of some asset or portfolio.
        %
        %       fin - is an 1x4 dataset specifying financial ratios like: Sharpe, Jensen,
        %                   Information ratio, Omega for some asset or portfolio.
        %
        %   Output:
        %       obj - it is an object of the portfolio class.
        %
        % Yigal Ben Tal
        % Copyright 2012 BondIT Ltd.

            %% Pre Initialization %%
            error(nargchk(0,8,nargin));

            % Input parsing:
            p = inputParser;

            p.addParamValue('total_value', NaN, @(x)(isnumeric(x)));
            p.addParamValue('capm', dataset(NaN, NaN, NaN, 'VarNames', {'alpha', 'beta', 'unique_risk'}), @(x)(isa(x, 'dataset')));
            p.addParamValue('fin', dataset(NaN, NaN, NaN, NaN, 'VarNames', {'sharpe', 'jensen', 'omega', 'inf_ratio'}), @(x)(isa(x, 'dataset')));
            p.addParamValue('rsquare', dataset(NaN, NaN, NaN, NaN, 'VarNames', {'rsquare_vs_expret', 'rmse_vs_expret', 'rsquare_vs_bench', 'rmse_vs_bench'}), @(x)(isa(x, 'dataset')));
        
            % Parse method to parse and validate the inputs:
%             p.StructExpand = true; % for input as a structure
            p.KeepUnmatched = true;

            try
                p.parse(varargin{:});
            catch ME
                newME = MException('dml:perf_ratio:optionalInputError', 'Error in input arguments');
                newME = addCause(newME,ME);
                throw(newME)
            end
            
            %% Initialization %%
            obj.total_value = p.Results.total_value;
            obj.capm = p.Results.capm;
            obj.fin = p.Results.fin;
            obj.rsquare = p.Results.rsquare;
            
            %% Post Initialization %%
            
        end
        function [ ratio ] = get_ratio(obj, ratio_name)
        %PERF_RATIO\GET_RATIO returns all performance values of the given performance object.
        %
        %   [ ratio ] = get_ratio(obj, ratio_name)
        %   receives performance object and returns all its ratio values.
        %
        %   Input:
        %       obj - is a dml.performance object specifying all groups of ratio measures.
        %
        %       ratio_name - is a 1xNRATIOS cell of strings array specifying the required ratio
        %                           names.
        %
        %   Output:
        %       ratio - is a 1xNRATIOS dataset specifying the values of the given list of
        %       performance ratios. 
        %
        % Yigal Ben Tal
        % Copyright 2012 BondIT Ltd..
            
            %% Import external packages:
            import dal.portfolio.crud.*;
            import utility.dataset.*;
            
            %% Input validation:
            error(nargchk(1,2,nargin));
            if (isempty(obj))
                error('perf_ratio:get_ratio:mismatchInput', 'An empty input.');
            elseif (~isa(obj, 'dml.perf_ratio'))
                error('perf_ratio:get_ratio:wrongInput', 'Wrong object type.');
            end
            
            if (nargin < 2) || (isempty(ratio_name))
                ratio_name = 'all';
            end
                
            %% Step #1 (Concatenate all ratios together):
            ratio = [dataset(obj.total_value, 'VarNames', {'total_value'}), obj.capm, obj.fin, obj.rsquare];
            
            %% Step #2 (Get part of ratios according to its given name list):
            if (~strcmpi( 'all', ratio_name ))
                req_ratio = ismember(dsnames(ratio), ratio_name);
                ratio = ratio(:, req_ratio);
            end
            
        end
    end
    
end

