clear; clc;

%% Import external packages
import dml.*;

%% Definition of portfolio creation information:
account_name = 'test010120082';

start_date = '2006-01-02';
end_date = '2012-01-02';

%% Get simulation performace and historical risk measures from database:
performance_measure = portfolio.get_history(account_name, 'performance', start_date, end_date)
risk_measure =  portfolio.get_history(account_name, 'risk', start_date, end_date)

% cost_measure = portfolio.get_history(account_name, 'cost', start_date, end_date);
% money_breakdown =  portfolio.get_history(account_name, 'money_breakdown', start_date, end_date);