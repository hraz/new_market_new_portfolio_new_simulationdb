function [ history_data ] = get_history(account, dyn_group_name, start_date, end_date)
%GET_HISTORY returns historical values of an account given time period.
%
%   [ history_data ] = get_history(account_name, dyn_group_name, start_date, end_date)
%   receives account name for identification, required historical property group name and the bounds of
%   required time period, as a result the function returns the struct of fts, in which, each field
%   specifying some dynamic property.
%
%   Input:
%       account - is a string value specifying the required account name or a scalar value specifying the account_id.
%
%       dyn_group_name - is a string value specifying the group name of
%                                   some portfolio dynamic characteristics. Possible group names are: 
%                                   - allocation
%                                   - cost
%                                   - risk
%                                   - performance
%                                   - port_deltas
%                                   - money_breakdown
%
%       start_date - is a date value specifying the lower bount of the required time period.
%
%       end_date - is a date value specifying the upper bound of the required time period.
%
%   Output:
%       history_data - is a struct of fts specifying the asked groups of the dynamic portfolio
%                                   characteristics.

% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Import external packages:
    import dal.portfolio.data_access.*;
    
    %% Input validation:
    error(nargchk(3,4,nargin));
    
    if (isempty(account) )
        error('portfolio:get_history:mismatchInput', ...
            'Account identificator is empty.');
    elseif isnumeric(account)
        account_id = account;
    elseif ischar(account)
        account_id = da_account_id(account);
        if isempty(account_id)
            error('portfolio:get_history:mismatchInput', ...
                'There is no such account.');
        end   
    end
    
    if isempty(start_date)
        error('portfolio:get_history:wrongInput', ...
            'The start_date is empty.');
    elseif ~ischar(start_date)
        start_date = datestr(start_date, 'yyyy-mm-dd'); % for da_multy_hist instead of between I create there <=x<=
    end
    
    if isempty(end_date)
        end_date = datestr(today, 'yyyy-mm-dd');
    elseif ~ischar(end_date)
        end_date = datestr(end_date, 'yyyy-mm-dd');
    end
    
    if ~ischar(dyn_group_name)
        error('portfolio:get_history:wrongInput', ...
            'The dyn_group_name data type is wrong.');
    elseif sum(ismember(lower(dyn_group_name), {'allocation', 'model', 'risk', 'performance', 'port_deltas', 'money_breakdown', 'cost'})) ~= 1
        error('portfolio:get_history:wrongInput', ...
            'You can get just one property by time.');
    end
    
    %% Step #1 (Return historic data between given dates):
    if ischar(dyn_group_name)
        dyn_group_name = {dyn_group_name};
    end
    [ history_data  ] = da_multi_hist(account_id, start_date, end_date, dyn_group_name);
    history_data = history_data.(dyn_group_name{:});
    
end

