function [port_duration_min port_duration_max warning_id] = set_port_dur_min_max(portfolio_duration)

durationTable=[1,5,8,13,17];
diffdurationTable=diff(durationTable)/2;
diffdurationTable=[diffdurationTable,diffdurationTable(end)];

if portfolio_duration<durationTable(1)    
    port_duration_min=durationTable(1)/2;
    port_duration_max=durationTable(1);
    warning_id=121;
elseif portfolio_duration>durationTable(end)    
    port_duration_min=durationTable(end-1);
    port_duration_max=2*durationTable(end)+durationTable(end-1);
    warning_id=121;
else
    portDurationIndx=find(durationTable<=portfolio_duration,1,'last');
    portDurationDelta=diffdurationTable(portDurationIndx);
    port_duration_min=max(portfolio_duration-portDurationDelta,0);
    port_duration_max=portfolio_duration+portDurationDelta;    
    warning_id=0;
end