function [ num_id ] = strid2numid( str_id )
%NUMID2STRID redefine id from number to string value for upload data from mat file.
%
%   [ str_id ] = numid2strid( num_id )
%   receives the vector of numeric id values and returns cell-vector of
%   string id values.
%
%   Inputs:
%       str_id - is an NASSETSx1 cell-vector specifying the asset id
%                    numbers in string format corresponding to db structure.
%
%   Outputs:
%       num_id - is an NASSETSx1 vector specifying the asset id numbers.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(1, 1, nargin));
    
     if (isempty(str_id)) 
        error('utility_db:strid2numid:mismatchInput', ...
            'ID value can''t be an empty.');
     elseif (~iscell(str_id)) && (~ischar(str_id))
        error('utility_db:numid2strid:mismatchInput', ...
            'ID value must be string.');
     end
    
    %% Step #1 (Creation needed id):
    if (ischar(str_id))
        str_id = {str_id};
    end
    
    id_l = length(str_id);
    num_id = zeros(id_l, 1);
    for i = 1:id_l
        num_id(i) = str2double( strrep( str_id{i}, 'id', '' ) );
    end
    
end
