function [ alpha, beta, unique_risk, bench_expret ] = nanab( asset_ret, bench_ret )
%NANAB receives asset return and benchmark return vectors that
%   both of them have the same min_leng and returns a alpha and beta risk measures
%   for given asset.
%
%   Note: If both given vectors have different min_leng, the function will
%                cuts it until minimum min_leng if it not less then 30, othewise it creates an error.
%   
%   [ alpha, beta, unique_risk, bench_expret ] = nanab( asset_ret, bench_ret ) returns alpha and
%               beta risk measures of the given asset against to given benchmark, like index 
%               or portfolio.
%   
%   Inputs:
%       asset_ret - is an NOBSERVATIONSx1 column vector specifying the portfolio returns.
%       
%       bench_ret - is an NOBSERVATIONSx1 column vector specifying the benchmark' return during the
%                           same period. 
%
%	Outputs:
%       alpha - is a scalar specifying the alpha value of the given asset against the
%                           given benchmark.
%
%       beta - is a scalar specifying the beta value of the given asset against the 
%                           given benchmark.
%
%       unique_risk - is a sclar specifying the non-systematic risk of the given asset.
%
%       bench_expret - is a scalar specifying the benchmark expected return.
%
%   Notes:
%     - The beta of an asset is a number describing the relation of its returns
%       with that of the financial market as a whole.
%       
%     - An asset with a beta of zero means that its returns change independently 
%       of changes in the market's returns.
%       
%     - A positive beta means that the asset's returns generally follow the
%       market's returns, in the sense that they both tend to be above their
%       respective averages together, or both tend to be below their
%       respective averages together. 
%
%     - A negative beta means that the asset's returns generally move opposite 
%       the market's returns: one will tend to be above its average when the other 
%       is below its average.
%
%     - The beta coefficient is a key parameter in the Capital Asset Pricing Model
%       (CAPM). It measures the part of the asset's statistical variance  that cannot
%       be mitigated by the diversification provided by the portfolio of many risky
%       assets, because of the correlation of its returns with the returns of the 
%       other assets that are in the portfolio. 
%
%     - beta can be estimated for individual companies using regression analysis 
%       against a stock market index.

% Yigal Ben Tal
% Copyright 2010-2012.

    %% Input validation:
    error(nargchk(2,2, nargin))
    if (~iscolumn(asset_ret))
        error('utility:nanab:wrongInput', 'Wrong asset_ret data type, it must be a column of numbers.');
    end
    
    if (~iscolumn(bench_ret))
        error('utility:nanab:wrongInput', 'Wrong bench_ret data type, it must be a column of numbers.');
    end
    
    %% Step #1 (There is a cut of given arrays' min_lengs to the minimum of it):
    asset_hleng = find( isfinite(asset_ret) , 1,'last');
    bench_hleng = find( isfinite( bench_ret ),1, 'last');
    if ( asset_hleng ~= bench_hleng )
        min_leng = min( asset_hleng, bench_hleng );
    else
        min_leng = bench_hleng;
    end
    asset_ret = asset_ret( (end - min_leng + 1):end);
    bench_ret = bench_ret( (end - min_leng + 1):end);
    
    %% Step #2 (Expected return and expected covariance computing):
    asset_expret = nanmean(asset_ret);
    bench_expret = nanmean(bench_ret);
    cov_mtx = nancov(asset_ret, bench_ret);
    
    %%  Step #4 (There is beta calculation. beta = cov_mtx(r,bench)/Var(Bench)):
    beta = cov_mtx(1,2) ./ cov_mtx(2,2);
      
    %% Step #5 (There is alpha calculation. alpha = E[r] - beta*E[Bench] ):
    alpha = nanmean(asset_expret - beta .* bench_expret);

    %% Step #6 (unique_risk calculation. NonSysRisk = Var[r - (alpha + beta*E[Bench])^2]):
    unique_risk = nanmean((asset_ret - (alpha + beta .* bench_ret)));
end
