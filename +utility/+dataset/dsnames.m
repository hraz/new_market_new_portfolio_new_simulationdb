function [ data_names ] = dsnames( ds_obj )
%DSNAMES returns dataset field names.
%
%   [ data_names ] = dsnames( ds_obj ) receives the dataset object and returns its field names.
%
%   Input:
%       ds_object - is a scalar dataset objet.
%
%   Output:
%       data_names - is an 1xNFIELDS cell of strings array specifying the names of the given
%                           dataset.

% Yigal Ben Tal
% Copyright 2012.

    %% Input validation:
    error(nargchk(1, 1, nargin));
    if (~isa(ds_obj, 'dataset'))
        error('utility:dsnames:mismatchInput', 'Wrong data type.');
    end
    
    %% Initialization of the output variable:
    data_names = get(ds_obj, 'VarNames');
    
end

