function [ res_ds ] = addds( old_ds, new_ds )
%ADDDS adds new dataset to oldest one by isin number.
%
%   [ res_ds ] = addds( old_ds, new_ds )
%   receives old and new datasets and return sorted by isin union dataset.
%   
%   Input:
%       old_ds - is a scalar dataset specifying the old dataset.
%
%       new_ds - is a scalar dataset specifying the new dataset.
%
%   Output:
%       res_ds - is a scalar dataset specifying sorted by unique values in isin
%                    field merged total dataset.
%
% Yigal Ben Tal
% Copyright 2011.

    %% Input validation:
    error(nargchk(2, 2, nargin));
    if (~isa(old_ds, 'dataset') || ~isa(new_ds, 'dataset'))
        error('utility:addds:wrongInput', 'Wrong parameter type.');
    end
    if (isempty(new_ds)) && (isempty(old_ds))
        res_ds = fints();
        return;
    elseif (isempty(new_ds))
        res_ds = old_ds;
        return;
    elseif (isempty(old_ds))
        res_ds = new_ds;
        return;
    end
    
    %% Step #1 (Adding new data to old one):
    res_ds = vertcat(old_ds, new_ds);
    
    %% Step #2 (Select just unique data by isin field):
    res_ds = unique(res_ds, 'nsin');

end

