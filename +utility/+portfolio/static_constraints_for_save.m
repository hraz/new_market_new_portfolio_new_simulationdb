

function outDS = static_constraints_for_save(static_constraints,benchmark_id_constraints)

class_linkage_combination_ID= 15; 
 
constraintValueID=(1:length(static_constraints.minWeight))';
constraintID = class_linkage_combination_ID*ones(size(constraintValueID));
minValue=static_constraints.minWeight;
maxValue=static_constraints.maxWeight;

outDS = dataset(constraintID,constraintValueID, minValue,maxValue);

if ~isempty(benchmark_id_constraints)
    staticBenchmarkID = 18;
    tempDS = dataset(staticBenchmarkID, benchmark_id_constraints,1,1,...
        'VarNames',{'constraintID','constraintValueID', 'minValue','maxValue'});
end

outDS=[outDS;tempDS];
