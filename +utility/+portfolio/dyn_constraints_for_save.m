function outDS = dyn_constraints_for_save(dyn_constraints)

name={'history_length';'ttm';'yield';'turnover';'duration';'modified_duration'};
outDS=dataset();
for iName=1:length(name)
    if isfield(dyn_constraints,name{iName})
        constraintID=iName;
        minValue=dyn_constraints.(name{iName}){1};
        maxValue=dyn_constraints.(name{iName}){2};
        datasetTemp=dataset(constraintID,minValue,maxValue);
        outDS=[outDS;datasetTemp];
    end
end

