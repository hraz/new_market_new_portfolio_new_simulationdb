function outDS = rating_constraints_for_save(rating_constraints)
%% mapping rating strings to Id's
% global maalotTable;
% global midroogTable;

% import dal.market.get.base_data.*;
% maalotTable=get_bond_base_static('maalot');
% midroogTable=get_bond_base_static('midroog');
% 
rating_maalot={'AAA'
    'AA+'
    'AA'
    'AA-'
    'A+'
    'A'
    'A-'
    'BBB+'
    'BBB'
    'BBB-'
    'BB+'
    'BB'
    'BB-'
    'B+'
    'B'
    'B-'
    'CCC+'
    'CCC'
    'CCC-'
    'CC'
    'C'
    'SD'
    'D'
    'NR'
    'NVR'};
place_on_scale=(24:-1:0)';
maalotTable=dataset(place_on_scale,rating_maalot);

rating_midroog={'Aaa'
    'Aa1'
    'Aa2'
    'Aa3'
    'A1'
    'A2'
    'A3'
    'Baa1'
    'Baa2'
    'Baa3'
    'Ba1'
    'Ba2'
    'Ba3'
    'B1'
    'B2'
    'B3'
    'Caa1'
    'Caa2'
    'Caa3'
    'Ca'
    'C'
    'NR'
    'NVR'};
place_on_scale=(22:-1:0)';
midroogTable=dataset(place_on_scale,rating_midroog);

%% main body - building the dataset
minMaalotIndx=find(strcmp(maalotTable.rating_maalot,rating_constraints.min_maalot));
maalotMinID=maalotTable.place_on_scale(minMaalotIndx);
maxMaalotIndx=find(strcmp(maalotTable.rating_maalot,rating_constraints.max_maalot));
maalotMaxID=maalotTable.place_on_scale(maxMaalotIndx);

minMidroogIndx=find(strcmp(midroogTable.rating_midroog,rating_constraints.min_midroog));
midroogMinID=midroogTable.place_on_scale(minMidroogIndx);
maxMidroogIndx=find(strcmp(midroogTable.rating_midroog,rating_constraints.max_midroog));
midroogMaxID=midroogTable.place_on_scale(maxMidroogIndx);

logicOperand=rating_constraints.logical_operator;

outDS=[dataset(maalotMinID,maalotMaxID,midroogMinID,midroogMaxID),...
    dataset({logicOperand},'VarNames','logicOperand')];
