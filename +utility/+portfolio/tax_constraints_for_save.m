function outDS = tax_constraints_for_save(tax_constraint)

taxID=1; % global transaction cost
feeValue = tax_constraint;

outDS = dataset(taxID,feeValue);

