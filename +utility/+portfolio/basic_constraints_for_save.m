function outDS = basic_constraints_for_save(basic_constraints)

% 1 - invested amount
% 2 - horizon (=portfolio duration sought)
% 3 - history length for calculation of perforamnces
% 4 - maximal weight for asset
% 5 - percentile used for VaR calculations

 
constraintID=(1:5)';
value=[basic_constraints.investedAmount; basic_constraints.port_duration_sought; ...
    basic_constraints.histLen; basic_constraints.maxWeight;basic_constraints.VaRPercentile];

outDS=dataset(constraintID,value);

% 
% function outDS = basic_constraints_for_save(basicConstraint)
% 
% outDS=dataset();
% basicConstraintMap=get_constraints_map('basic');
% for iConstraint=1:size(basicConstraintMap,1)
%     constraintName=basicConstraintMap.name(iConstraint);
%     constraintIndx=find(ismember(fieldnames(basicConstraint),constraintName));
%     if ~isempty(constraintIndx)
%         constraintID=basicConstraintMap.id(iConstraint);
%         value=basicConstraint.(constraintName);
%         tempDS=dataset(constraintID,value);
%     end
%     outDS=[outDS;tempDS];
% end
% 
% function outDS = find_dataset_in_mapping(specificDS,mappingDS,varName)
% 
% if nargin==2
%     varName='constraintID';
% end
% 
% outDS=dataset();
% for iRow=1:size(mappingDS,1)
%     rowName=mappingDS.name(iRow);
%     indxInSpecificDS=find(ismember(specificDS.Properties.VarNames,rowName));
%     if ~isempty(indxInSpecificDS)
%         
%         value=specificDS.(rowName);
%         tempDS=
%     end
% end
