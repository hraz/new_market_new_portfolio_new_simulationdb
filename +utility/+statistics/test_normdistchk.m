clc; clear;

import utility.statistics.*

x_norm = normrnd(0,1,[10000,1]);

% Tests that check the error handling of bad inputs. All of the following
% should result in exiting from the function.
%normdistchk()
%normdistchk(x_norm,2)
%normdistchk([]), return
%normdistchk('test')

% Tests that check whether the statistical test results make sense for
% different distributions.
disp('==========norm==========')

[chit_res, kst_res, llt_res, propt_res, zt_res] = normdistchk(x_norm)
figure(1) 
hist(x_norm,100)
title('norm')

disp('==========rand==========')
x_rand = rand([10000,1])-0.5;
[chit_res, kst_res, llt_res, propt_res, zt_res] = normdistchk(x_rand)
figure(2) 
hist(x_rand,100)
title('rand')

disp('==========peaked but not normal==========')
x_real = x_norm+6*x_rand+0.5;
[chit_res, kst_res, llt_res, propt_res, zt_res] = normdistchk(x_real)
figure(3) 
hist(x_real,100)
title('peaked but not normal')