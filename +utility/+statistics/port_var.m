function [ port_variance ] = port_var( covar, weight )
%PORT_VAR calculates portfolio variance given covariance matrix and asset weights.
%
%   [ port_variance ] = port_var( covar, weight )
%   receives assets' coavriance matrix, asset weights and returns portfolio variance.
%
%   Input:
%       covar -
%
%       weight - is a NASSETSx1 vector specifying the given asset allocation weights.
%
%   Output:
%       port_variance - is a scalar value specifying the total portfolio variance.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Input validation:
    error(nargchk(2,2,nargin));
    
    if (isempty(covar) || isempty(weight))
        error('utility_rm_statistics:port_risk:mismatchInput', 'The one of the input parameters is empty.');
    end
    if (size(covar,1) ~= length(weight)) || (size(covar,2) ~= length(weight))
        error('utility_rm_statistics:port_risk:wrongInput', 'The length o the second parameter must be equal to num of rows and columns of covariance matrix.');
    end
    
    %%  Portfolio variance calculation:
    port_variance = weight' * covar * weight;
    
end

