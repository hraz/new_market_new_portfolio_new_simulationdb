function [ opt_data, opt_weight ] = hdataoptimizer( data, weight, sector )
%HDATAOPTIMAZER 
%
%   [ opt_weight, opt_data ] = hdataoptimizer( data,weight,sector )
%   receives history price datass of all assets from given portfolio, its weights and sector
%   relations and returns rebalanced weights of assets and its optimized
%   price datas with the same acceptable minimal history length.
%
% Inputs:
%       data - is an 1xNASSETS cell-vector specifying the history prices
%                                   of all assets from given portfolio.
%
%       weight - is an 1xNASSETS vector specifying the weights of assets
%                                   in given portfolio.
%
%       sector - is an 1xNASSETS vector specifying the sector names of
%                                   assets in given portfolio.
%
%   Outputs:
%       opt_weight - is an 1xMASSETS vector specifying the rebalanced
%                                   weights of assets in given portfolio that meet the
%                                   requirements of the Central Limit Theorem.
%
%       opt_data - is an MINNUMOBSERVATIONSxMASSETS matrix specifying the optimized history
%                                    prices of each asset from given portfolio.
%
% Yigal Ben Tal
% Copyright 2010 - 2011.

    %% Import external packages and global variables declaration:
    import utility.rm.*;
    
    %% Input validation:
    error(nargchk(3, 3, nargin));
     
    %% Step #1 (Looking for acceptable minimum history length (30<AccMin)):
    [ MinAcceptedL, ShortLInd ] = acceptable_min(data, 30 );
        
    %% Step #2 (Temporary rebalancing of the portfolio for risk measures calculus): 
    nassets = size(data, 2);
    ShortLIndL = length(ShortLInd);    
    for i = 1:ShortLIndL
        % Looking for indexes of assets from the same sector:
        SectorInd = find( strcmp(sector{ShortLInd(i)}, sector) );
        SecIndL = length(SectorInd);
        IntersectL = length( intersect(SectorInd, ShortLInd));
        
        % Rebalancing into all portfolio:
        if ( isempty(SectorInd) ) || all(SectorInd == 0) || (IntersectL == SecIndL)
            % The problematic asset weight divided by all assets number
            if (IntersectL == SecIndL)
                del_weight = weight(ShortLInd(i)) / (nassets - SecIndL);
            else
                del_weight = weight(ShortLInd(i)) / nassets;
            end
            for j = 1:nassets
                weight(j) = (weight(j) + del_weight) * (all(j ~= ShortLInd(i)));
            end %end for
            
        % Rebalancing into the same sector:
        else
            % The problematic asset weight divided by sector assets number:
            del_weight = weight(ShortLInd(i)) / (SecIndL - IntersectL);    
            for j = 1:SecIndL
                    weight(SectorInd(j)) = (weight(SectorInd(j)) + del_weight) * (all( SectorInd(j) ~= ShortLInd ));
            end %end for
            
        end %endif
        
    end %end for

    %% Step #3 (Optimized assets' returns and weights creation):
    OptAssInd = 0;
    opt_data = zeros(MinAcceptedL, nassets - ShortLIndL);
    opt_weight = zeros(1, nassets - ShortLIndL);
    for i = 1:nassets
        if ~(any( i == ShortLInd ))
            OptAssInd = OptAssInd+1;
            opt_data(:,OptAssInd) = data( (length(data(: ,i)) - MinAcceptedL + 1) : length(data(: ,i)), i);
            opt_weight(OptAssInd) = weight(i);
        end % end if
    end % end for
        
end %end function

