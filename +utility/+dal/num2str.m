function [ strArr ] = num2str( numArr )
%NUM2STR Convert an ND numeric array to a string in MySQL syntax.
%
%   [ strArr ] = num2str(numArr) converts the ND numerical matrix to a string value in MySQl syntax
%   Works as corresponding NUM2STR but the result is a string suitable to using in MySQL.
%
%   Example
%       numArr = [1 2 3; 4 5 6]';
%       num2str(numArr) produces the string '1,2,3,4,5,6'.
%
%   See also NUM2STR, STRREP

% Author: Yigal Ben Tal
% Copyright 2011-2013, BondIT Ltd.
% Updated by: _________________
% Reason: ____________________

    %% Input validation:
    error(nargchk(nargin,1,1));
    
    if ~isnumeric(numArr)
        error('utility_dal:num2str:wrongDataType','Input argument must be numeric matrix.');
    end
    
    %% Reformat given data:
    strArr = strrep(strrep(num2str(numArr(:)'), '  ', ','), ',,', ',');

end

