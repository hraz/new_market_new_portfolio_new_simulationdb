function [ string ] = num2str4sqlbracket(numarr)
%num2str4sqlbracketBRACKET Convert a 1-D numeric array to a string in MySQL syntax.
%
%   [ string ] = num2str4sqlbracketbracket(numarr) converts the 1-D matrix numarr to a string value in MySQl syntax
%   Works as corresponding MAT2STR but the result is a string suitable to using in MySQL.
%
%   Example
%       numarr = [234; 230];
%       num2str4sqlbracket(numarr) produces the string ''('234'), ('230')''; its equal to
%                               ('234'), ('230') as result list for MySQL.
%
%   See also MAT2STR, STRREP, num2str4sqlbracket
%
% Created by Yigal Ben Tal
% Update by Yigal Ben Tal 
% At 26.08.2013
% The reason is: The our update in mat2str.
% Copyright 2012 - 2013

    %% Import external packages
    import utility.dal.mat2str;
    
    %% Input validation:
    if nargin~=1
        error('utility_dal:num2str4sqlbracket:Nargin','Takes 1 input argument.');
    end
    
    if ~isnumeric(numarr)
        error('utility_dal:num2str4sqlbracket:Class','Input argument must be numeric matrix.');
    end
    if ~isvector(numarr)
        error('utility_dal:num2str4sqlbracket:OneDInput','Input matrix must be 1-D.');
    end

    %% Step #1 (Matrix rebuild to one string):
    strnumarr = mat2str(numarr, 0);
    [n, m] = size(numarr);
    if (n == 1 && m == 1)
        string = ['''(''''', strnumarr, ''''')'''];
    else
        if (n > m)
            string = strrep(strnumarr, ';', '''''), (''''');
        else
            string = strrep(strnumarr, ' ', '''''), (''''');
        end
        string = strrep(strrep(string, '[', '''('''''), ']', ''''')''');    
    end
    
end
