function [ fts ] = dataset2fts( ds, data_name )
%DATASET2FTS transformate dataset to fts.
%
%	[ fts ] = dataset2fts( ds, data_name ) receives dataset with its own
%	name and returns fts.
%
%   Input:
%       ds - is an NOBSERVATIONSxMASSETS+1 dataset specifying some
%               dynamic data of assets;
%
%       data_name - is a string value specifying the needed asset property.
%                           In the case of ex_date the default name of the data_name is ex_day.
%
%   Output:
%       fts - is an NOBSERVATIONSx(MASSETS+1) financial time series
%                       specifying the daily data about required.
%
% Yigal Ben Tal
% Copyright 2011

    %% Import external packages:
    import utility.dal.*;
    import utility.dataset.*;
    
    %% Input validation:
    error(nargchk(1, 2, nargin));
    if (nargin < 2) || (isempty(data_name))
        data_name = 'ex_day';
    end
   
    %% Step #1 (Export dates):
    [ udate ] = ds.date;
    
    %% Step #2 (Export data names):
    [ uid ] = dsnames(ds);
    uid = uid(2:end);
            
    %% Step #3 (Creation needed data):
    if (any(strcmpi(data_name, 'ex_day')))
        data = cellstr(ds, uid);
        data(strcmpi(data, 'null')) = {NaN};
        data = cellfun(@(x) datenum(x),data);
    else
        data = double(ds, uid);
    end
    
    %% Step #4 (Financial time series creation):
    fts = fints(udate, data, uid, 'd');
    fts.desc = data_name;
    
end

