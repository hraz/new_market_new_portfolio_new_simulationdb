function [ hostName ] = get_computer_name()
% GET_COMPUTER_NAME statusurns the name of the computer (hosthostName)
% 
%	[ hostName ] = get_computer_name()
%	statusurns the name of the computer.
%
%   Input:
%       None.
% 
%   Output:
%       hostName - is a STRING value in lower case specifying the current hostname.

% Created by: Yigal Ben Tal,
%                    at: 08.09.2013
% Copyright 2013, BondIT Ltd.
%
% Updated by: ______________________,
%                     at: ______________________.
% The reason is: ____________________________________________________________.

    %% Get from the system current hostName:
    [status, hostName] = system('hostname');   % DOS case
    if status ~= 0,
       if ispc % Windows case
          [ hostName ] = getenv('COMPUTERNAME');
       else      % Linux or MAC case
          [ hostName ] = getenv('HOSTNAME');      
       end
    end
    
    %% Return the output value:
    hostName = lower(hostName);

end