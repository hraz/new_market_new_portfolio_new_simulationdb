function [ appropriate_rf_period ] = approp_rf_prd( given_time_period )
%APPROP_RF_PRD returns appropriate risk free time period for given times.
%
%   [ appropriate_rf_period ] = approp_rf_prd( given_time_period )
%   receives time periods and returns appropiate for each one risk free period.
%
%   Input:
%       given_time_period - is an 1xNTIMES numerical vector specifying the user defined time period.
%
%   Output:
%       appropriate_rf_period - is an 1xNTIMES numeric vector specifying risk free rate period
%                                           appropiates to the given ones.
%
% Yigal Ben Tal
% Copyright 2012 BondIT Ltd.

    %% Input validation:
    error(nargchk(1,1,nargin));
    if isempty(given_time_period)
        error('utility_dal:approp_rf_prd:mismatchInput',...
            'The input cann''t be an empty.');
    elseif ~isnumeric(given_time_period)
        error('utility_dal:approp_rf_prd:wrongInput',...
            'The input must be numerical.');
    elseif ~isrow(given_time_period)
        error('utility_dal:approp_rf_prd:wrongInput',...
            'The input must be a row vectorl.');
    end
    
    %% Step #1 (Declare all possible risk free time periods): 
    interest_period = round([(1:1:11)/12, 1, 2, 3, 5, 7, 10]' * 10000) / 10000;
    
    %% Step #2 (Reformat given times to appropriate values): 
    given_time_period = round(given_time_period *10000)/10000;
    nperiod = length(given_time_period);
    
    %% Step #3 (Reshape based in user defined time vectors);
    given_time_period_mtx = given_time_period(ones(length(interest_period),1),:);
    interest_period_mtx = interest_period(:,ones(length(given_time_period),1));

    %% Step #4 (Look for a cover risk free period index for each given one):
    cover = given_time_period_mtx <= interest_period_mtx;
    
    %% Step #5 (Find a cover risk free period for each given one):
    yy = zeros(1,nperiod);
    for i = 1: nperiod
        yy(i) = find(cover(:,i),1,'first');
    end
    appropriate_rf_period = interest_period(yy)';

end

