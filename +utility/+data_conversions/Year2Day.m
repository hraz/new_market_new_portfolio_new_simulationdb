function daily_interest=Year2Day(annual_interest)
% translates annual interest to daily interest. Bote interest rates
% are in decimal format, e.g., 10 percent annually will be denoted as 0.1, which 
% is equivalent to 0.0261 percent daily, which will be denoted as 0.000261
daily_interest=(1+annual_interest).^(1/252)-1;       